angular.module('${menuCode}')
.filter('to_trusted', ['$sce', function ($sce) {
	return function (text) {
	    return $sce.trustAsHtml(text);
	};
}])
.controller("${widgetCode}Ctrl",function($scope,AppKit){
	$scope.setActiveWeekTab = function (activeWeekTab) {     
		　　$scope.activeTab = activeWeekTab; 
	};
	
	$scope.initWeekTabs = function(tabs){
		$scope.tabs = tabs;	
		$scope.activeTab = tabs[1];
	}
	
	$scope.findProWeekWorkListInfos = function(){
		var url = '/aeaiwm/services/WeekMgmt/rest/find-proweeklist-infos/'+$scope.weekTimeId+'/'+$scope.grpId+'/'+$scope.userId;
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			if("error" == rspJson.startTime){
				AppKit.successPopup({"title":"这是第一周周报记录!"});	
			}else if("0" == rspJson.wwDay){
				AppKit.successPopup({"title":"这是第一周周报记录!"});	
			}else{
				$scope.startTime = rspJson.startTime;
				$scope.time = rspJson.time;
				$scope.endTime = rspJson.endTime;
				$scope.wwDay = rspJson.wwDay;
				$scope.plan = rspJson.plan;
				$scope.follow = rspJson.follow;
				$scope.weekTimeId = rspJson.weekTimeId
				$scope.stateName = rspJson.stateName;
			}
		});
	}
	
	$scope.findThisWeekWorkListInfos = function(){
		AppKit.isLogin().success(function(data, status, headers, config){
			if (data.result=='true'){
				$scope.userLogin = "isLogin";
				AppKit.secuityOperation("aeaiwm",{"backURL":"/map/repository/genassets/${navCode}/index.cv#/tab/home",
					"success":function(){
						var url = '/aeaiwm/services/WeekMgmt/rest/find-thisweekwork-infos/'+$scope.grpId+'/'+$scope.userId;
						var promise = AppKit.getJsonApi(url);
						promise.success(function(rspJson){
							$scope.startTime = rspJson.startTime;
							$scope.endTime = rspJson.endTime;
							$scope.time = rspJson.time;
							$scope.wwDay = rspJson.wwDay;
							$scope.plan = rspJson.plan;
							$scope.follow = rspJson.follow;
							$scope.weekTimeId = rspJson.weekTimeId
							$scope.activeTab = "currentWeek";
							$scope.stateName = rspJson.stateName;
						});
					}	
				})
			}
		});
	}
	$scope.findThisWeekWorkListInfos();
	
	$scope.findLastWeekWorkListInfos = function(){
		var url = '/aeaiwm/services/WeekMgmt/rest/find-lastweekworklist-infos/'+$scope.weekTimeId+'/'+$scope.grpId+'/'+$scope.userId;
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			if("0" == rspJson.wwDay){
				AppKit.successPopup({"title":"这是最后一周周报记录!"});	
			}else if("error" == rspJson.startTime){
				AppKit.successPopup({"title":"这是最后一周周报记录!"});	
			}else{
				$scope.startTime = rspJson.startTime;
				$scope.time = rspJson.time;
				$scope.endTime = rspJson.endTime;
				$scope.wwDay = rspJson.wwDay;
				$scope.plan = rspJson.plan;
				$scope.follow = rspJson.follow;
				$scope.weekTimeId = rspJson.weekTimeId
				$scope.stateName = rspJson.stateName;
			}
			
		});
	}
});