angular.module('${menuCode}')
.controller("${widgetCode}Ctrl",function($scope,AppKit,$state,$ionicActionSheet,$timeout){
	
	$scope.loadDayMgmtCardInfos = function(){
		AppKit.isLogin().success(function(data, status, headers, config){
			if (data.result=='true'){
				$scope.userLogin = "isLogin";
				AppKit.secuityOperation("aeaiwm",{"backURL":"/map/repository/genassets/${navCode}/index.cv#/tab/home",
					"success":function(){
						var url = '/aeaiwm/services/DayMgmt/rest/get-daywork-info';
						var promise = AppKit.getJsonApi(url);
						promise.success(function(rspJson){
							$scope.today = rspJson.today;
						});
					}	
				})
			}
		});
	}
	$scope.loadDayMgmtCardInfos();

});