angular.module('${menuCode}')
.controller("${widgetCode}Ctrl",function($scope,AppKit){

	$scope.weekTimeId = null;
	$scope.limitSize = 5
	$scope.showionic = null;
	$scope.data = {grpId:''};
	
	
	
	$scope.setActiveTab = function (activeTab) {     
		　　$scope.activeTab = activeTab; 
	};
	
	$scope.isExistLeftHide = function(){
		if ($scope.curentIndex > $scope.limitSize/2){
			return true;
		}else{
			return false;
		}	
	};
	
	$scope.isExistRightHide = function(){
		if (($scope.totalSize - $scope.curentIndex) < ($scope.limitSize/2 + 1)){
			return false;
		}else{
			return true;
		}				
	};
	
	$scope.showIonic= function(activeTab){
		if(activeTab == $scope.activeWeekTab){
			return true;
		}else{
			return false;
		}
	};
	
	$scope.showPolicy = function(itemIndex){
		var rangeDiff = $scope.limitSize/2 + 1;
		var curentIndex = $scope.curentIndex;
		if($scope.totalSize <= $scope.limitSize){
			//不显示箭头图片
			$scope.showLeftionic = false;
			$scope.showRightionic = false;
			return true;
		}else{
			if (curentIndex >=rangeDiff){
				//显示向左箭头
				$scope.showLeftionic = true;
				$scope.showRightionic = false;
				
				if($scope.totalSize - curentIndex <= $scope.limitSize/2){
					if(itemIndex >= $scope.totalSize - $scope.limitSize){
						return true;
					}else{
						return false;
					}
				}else{
					//显示向左、右的箭头
					$scope.showLeftionic = true;
					$scope.showRightionic = true;
					if (itemIndex <= curentIndex + $scope.limitSize/2 && itemIndex > curentIndex - $scope.limitSize/2){
						return true;
					}else{
						return false;
					}
				}
			}else{
				//显示向右箭头
				$scope.showLeftionic = false;
				$scope.showRightionic = true;
				
				if(itemIndex <$scope.limitSize){
					return true;
				}else{
					return false;
				}
			}
		}
	}
	
	$scope.onSwipeLeft = function(){
		for(var i=0;i< $scope.tabs.length;i++){
			var tempTab = $scope.tabs[i];
			if (tempTab == $scope.activeTab){
				if (i == ($scope.tabs.length-1))continue;
				$scope.activeTab = $scope.tabs[i+1];
				$scope.changeClass($scope.activeTab);
				$scope.findActiveUserId($scope.activeTab);
				$scope.setActiveWeekTab($scope.activeWeekTab);
				
				$scope.curentIndex = i+1;
				break;
			}
		}
	};

	$scope.onSwipeRight = function(){
		for(var i=0;i< $scope.tabs.length;i++){
			var tempTab = $scope.tabs[i];
			if (tempTab == $scope.activeTab){
				if (i == 0)continue;
				$scope.activeTab = $scope.tabs[i-1];
				$scope.changeClass($scope.activeTab);
				$scope.findActiveUserId($scope.activeTab);
				$scope.setActiveWeekTab($scope.activeWeekTab);
				
				$scope.curentIndex = i-1;
				break;
			}
		}
	};
	
	$scope.setActiveWeekTab = function (activeWeekTab) {     
		　　$scope.activeWeekTab = activeWeekTab; 
	};
	
	$scope.initWeekTabs = function(weekTabs){
		$scope.weekTabs = weekTabs;	
		$scope.activeWeekTab = weekTabs[1];
	}
	
	$scope.initWeekExaminationUserInfos = function() {
		AppKit.isLogin().success(function(data, status, headers, config){
			if (data.result=='true'){
				$scope.userLogin = "isLogin";
				AppKit.secuityOperation("aeaiwm",{"backURL":"/map/repository/genassets/${navCode}/index.cv#/tab/home",
					"success":function(){
						var url = '/aeaiwm/services/WeekWorkExam/rest/retrieve-weekexam-user-infos/'+$scope.grpId;
						var promise = AppKit.getJsonApi(url);
						promise.success(function(rspJson){
							$scope.userInfos = rspJson.userInfos;
							$scope.userCodes = rspJson.userCodes;
							$scope.activeUserCode = $scope.userInfos[0].userCode;
							$scope.activeUserId = $scope.userInfos[0].userId;
							$scope.userId = $scope.userInfos[0].userId;
							$scope.grpId = rspJson.grpId;
							$scope.initThisWeekInfo();
							$scope.tabs = $scope.userCodes;
							$scope.activeTab = $scope.tabs[0];
							
							$scope.totalSize = $scope.userCodes.length;
							$scope.curentIndex = 0;
							$scope.showSubmitBtn($scope.activeUserCode);
						});
					}	
				})
			}
		});

	}
	$scope.initWeekExaminationUserInfos();
	
	$scope.initThisWeekInfo = function() {
		AppKit.isLogin().success(function(data, status, headers, config){
			if (data.result=='true'){
				$scope.userLogin = "isLogin";
				AppKit.secuityOperation("aeaiwm",{"backURL":"/map/repository/genassets/${navCode}/index.cv#/tab/home",
					"success":function(){
						var url = '/aeaiwm/services/WeekMgmt/rest/find-thisweekwork-infos/'+$scope.grpId+'/'+$scope.activeUserId;
						var promise = AppKit.getJsonApi(url);
						promise.success(function(rspJson){
							$scope.startTime = rspJson.startTime;
							$scope.endTime = rspJson.endTime;
							$scope.time = rspJson.time;
							$scope.wwDay = rspJson.wwDay;
							$scope.plan = rspJson.plan;
							$scope.follow = rspJson.follow;
							$scope.setActiveWeekTab('currentWeek');
							$scope.weekTimeId = rspJson.weekTimeId;
							$scope.completion = rspJson.completion;
							
							$scope.weekWorkId = rspJson.weekWorkId;
							$scope.state = rspJson.state;
							$scope.stateName = rspJson.stateName;
							
							$scope.grpName = rspJson.grpName;
						});
					}
				})
			}
		});

	}
	
	$scope.findProWeekInfos = function(){
		var url = '/aeaiwm/services/WeekMgmt/rest/find-proweeklist-infos/'+$scope.weekTimeId+'/'+$scope.grpId+'/'+$scope.activeUserId;
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.weekTimeId = rspJson.weekTimeId;
			
			if("error" == rspJson.startTime){
				AppKit.successPopup({"title":"这是第一周周报记录!"});	
			}else{
				$scope.startTime = rspJson.startTime;
				$scope.time = rspJson.time;
				$scope.endTime = rspJson.endTime;
				$scope.wwDay = rspJson.wwDay;
				$scope.plan = rspJson.plan;
				$scope.follow = rspJson.follow;
				$scope.completion = rspJson.completion;
				
				$scope.weekWorkId = rspJson.weekWorkId;
				$scope.state = rspJson.state;
				$scope.stateName = rspJson.stateName;
				$scope.grpName = rspJson.grpName;
			}
		});
	}
	
	$scope.findLastWeekInfos = function(){
		AppKit.isLogin().success(function(data, status, headers, config){
			if (data.result=='true'){
				$scope.userLogin = "isLogin";
				AppKit.secuityOperation("aeaiwm",{"backURL":"/map/repository/genassets/${navCode}/index.cv#/tab/home",
					"success":function(){
						var url = '/aeaiwm/services/WeekMgmt/rest/find-lastweekworklist-infos/'+$scope.weekTimeId+'/'+$scope.grpId+'/'+$scope.activeUserId;
						var promise = AppKit.getJsonApi(url);
						promise.success(function(rspJson){
							$scope.weekTimeId = rspJson.weekTimeId;
							
							if("error" == rspJson.startTime){
								AppKit.successPopup({"title":"这是最后一周周报记录!"});	
							}else{
								$scope.startTime = rspJson.startTime;
								$scope.time = rspJson.time;
								$scope.endTime = rspJson.endTime;
								$scope.wwDay = rspJson.wwDay;
								$scope.plan = rspJson.plan;
								$scope.follow = rspJson.follow;
								$scope.completion = rspJson.completion;
								$scope.weekWorkId = rspJson.weekWorkId;
								$scope.state = rspJson.state;
								$scope.stateName = rspJson.stateName;
								$scope.grpName = rspJson.grpName;
							}
							
						});
					}	
				})
			}
		});

	}
	
	$scope.initCurrentSelectWeekInfo = function(weekWorkId) {
		var url = '/aeaiwm/services/WeekWorkExam/rest/retrieve-weekwork-infos/'+weekWorkId+'/'+$scope.activeUserId+'/'+$scope.grpId;
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.startTime = rspJson.startTime;
			$scope.endTime = rspJson.endTime;
			$scope.time = rspJson.time;
			$scope.wwDay = rspJson.wwDay;
			$scope.plan = rspJson.plan;
			$scope.follow = rspJson.follow;
			$scope.setActiveWeekTab($scope.activeWeekTab);
			$scope.weekTimeId = rspJson.weekTimeId;
			$scope.completion = rspJson.completion;
			
			$scope.weekWorkId = rspJson.weekWorkId;
			$scope.state = rspJson.state;
			$scope.stateName = rspJson.stateName;
			$scope.grpName = rspJson.grpName;
		});
	}
	
	
	$scope.initCurSelectTimeWeekInfo = function() {
		AppKit.isLogin().success(function(data, status, headers, config){
			if (data.result=='true'){
				$scope.userLogin = "isLogin";
				AppKit.secuityOperation("aeaiwm",{"backURL":"/map/repository/genassets/wm/index.cv#/tab/home",
					"success":function(){
						var url = '/aeaiwm/services/WeekWorkExam/rest/retrieve-cursele-weekinfos/'+$scope.weekTimeId+'/'+$scope.activeUserId+'/'+$scope.grpId;
						var promise = AppKit.getJsonApi(url);
						promise.success(function(rspJson){
							$scope.startTime = rspJson.startTime;
							$scope.endTime = rspJson.endTime;
							$scope.time = rspJson.time;
							$scope.wwDay = rspJson.wwDay;
							$scope.plan = rspJson.plan;
							$scope.follow = rspJson.follow;
							$scope.setActiveWeekTab($scope.activeWeekTab);
							$scope.weekTimeId = rspJson.weekTimeId;
							$scope.completion = rspJson.completion;
							
							$scope.weekWorkId = rspJson.weekWorkId;
							$scope.state = rspJson.state;
							$scope.stateName = rspJson.stateName;
							$scope.grpName = rspJson.grpName;
						});
					}	
				});
			}
		});
	}
	
	$scope.changeClass = function(userCode) {
		$scope.activeUserCode = userCode;
	}
	
	$scope.setActiveUserId = function(userId) {
		$scope.activeUserId = userId;
	}

	$scope.isDisBtn = function() {
		if($scope.state != "INITIALISE"){
			return true;
		}else{
			return false;
		}
	}

	$scope.showSubmitBtn = function(userCode) {
		AppKit.isLogin().success(function(data, status, headers, config){
			if (data.result=='true'){
				$scope.userLogin = "isLogin";
				AppKit.secuityOperation("aeaiwm",{"backURL":"/map/repository/genassets/wm/index.cv#/tab/home",
					"success":function(){
						var url = '/aeaiwm/services/WeekWorkExam/rest/show-submit-btn/'+$scope.grpId+'/'+userCode;
						var promise = AppKit.getJsonApi(url);
						promise.success(function(rspJson){
							$scope.showSubBtn = rspJson.showSubmitBtn;
						});
					}
				});
			}
		})

	}
	
	$scope.findActiveUserId = function(userCode) {
		var url = '/aeaiwm/services/DayWorkExam/rest/get-active-userId/'+userCode;
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.activeUserId = rspJson.userId;
			$scope.showSubmitBtn(userCode);
			$scope.initCurSelectTimeWeekInfo();
		});
	}
	
	$scope.doSubmitWeekWork = function() {
		var url = '/aeaiwm/services/WeekWorkExam/rest/submit-curweek/'+$scope.weekWorkId;
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			if ("success" == rspJson){
				AppKit.successPopup({"title":"提交成功!"});
				$scope.initCurrentSelectWeekInfo($scope.weekWorkId);
			}else{
				AppKit.errorPopup();
			}
		});
	}
	
	$scope.createProjectModal = function(){
		AppKit.createModal("${menuCode}","WeekProjectInfosModal",$scope);
	}
	
	$scope.findProjectInfos = function(){
		AppKit.isLogin().success(function(data, status, headers, config){
			if (data.result=='true'){
				$scope.userLogin = "isLogin";
				AppKit.secuityOperation("aeaiwm",{"backURL":"/map/repository/genassets/wm/index.cv#/tab/home",
					"success":function(){
						var url = '/aeaiwm/services/WeekWorkExam/rest/find-project-infos';
						var promise = AppKit.getJsonApi(url);
						promise.success(function(rspJson){
							$scope.groupInfos = rspJson.projectInfos;
							$scope.grpId = rspJson.projectId;
							$scope.data.grpId = rspJson.projectId;
						});
					}	
				})
			}
		})

	}
	$scope.findProjectInfos();
	$scope.changeProject = function(grpId){
		AppKit.isLogin().success(function(data, status, headers, config){
			if (data.result=='true'){
				$scope.userLogin = "isLogin";
				AppKit.secuityOperation("aeaiwm",{"backURL":"/map/repository/genassets/wm/index.cv#/tab/home",
					"success":function(){
						$scope.grpId = grpId;
						var url = '/aeaiwm/services/WeekWorkExam/rest/sync-user-projectId/'+grpId;
						var promise = AppKit.getJsonApi(url);
						promise.success(function(rspJson){
							if ("success" == rspJson){
								$scope.initWeekExaminationUserInfos();
							}else{
								AppKit.errorPopup();
							}
						});
					}
				})
			}
		});

	}
	
});