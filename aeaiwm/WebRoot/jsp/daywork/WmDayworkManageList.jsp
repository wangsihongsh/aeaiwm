<!DOCTYPE html>
<%@page import="com.agileai.domain.DataRow"%>
<%@page import="java.sql.Timestamp"%>
<%@page import="com.agileai.util.DateUtil"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page language="java" import="java.util.*"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>日报记录</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script type="text/javascript" src="aeditors/xheditor/xheditor-1.2.2.min.js"></script>
<script type="text/javascript" src="aeditors/xheditor/xheditor_lang/zh-cn.js"></script>
<script type="text/javascript" src="js/jquery.hotkeys.js"></script>
<script>
var dayworkMap = new Map();
var daynoteMap = new Map();
</script>
<style type="text/css">
<!--
body {
	font: 100%/1.4 Verdana, Arial, Helvetica, sans-serif;
	background-color: #FFFFFF;
	margin: 0;
	padding: 0;
	color: #000;
}

/* ~~ 元素/标签选择器 ~~ */
ul, ol, dl { /* 由于浏览器之间的差异，最佳做法是在列表中将填充和边距都设置为零。为了保持一致，您可以在此处指定需要的数值，也可以在列表所包含的列表项（LI、DT 和 DD）中指定需要的数值。请注意，除非编写一个更为具体的选择器，否则您在此处进行的设置将会层叠到 .nav 列表。 */
	padding: 0;
	margin: 0;
}
h1, h2, h3, h4, h5, h6, p {
	margin-top: 0;	 /* 删除上边距可以解决边距会超出其包含的 div 的问题。剩余的下边距可以使 div 与后面的任何元素保持一定距离。 */
	padding-right: 15px;
	padding-left: 15px; /* 向 div 内的元素侧边（而不是 div 自身）添加填充可避免使用任何方框模型数学。此外，也可将具有侧边填充的嵌套 div 用作替代方法。 */
}
a img { /* 此选择器将删除某些浏览器中显示在图像周围的默认蓝色边框（当该图像包含在链接中时） */
	border: none;
}

/* ~~ 站点链接的样式必须保持此顺序，包括用于创建悬停效果的选择器组在内。 ~~ */
a:link {
	color: #42413C;
	text-decoration: underline; /* 除非将链接设置成极为独特的外观样式，否则最好提供下划线，以便可从视觉上快速识别 */
}
a:visited {
	color: #6E6C64;
	text-decoration: underline;
}
a:hover, a:active, a:focus { /* 此组选择器将为键盘导航者提供与鼠标使用者相同的悬停体验。 */
	text-decoration: none;
}

/* ~~ 此固定宽度容器包含所有其它 div ~~ */
.container {
	width: 100%;
	background-color: #FFF;
	overflow: hidden; /* 此声明可使 .container 了解其内部浮动列的结束位置以及包含列的位置 */
}

/* ~~ 以下是此布局的列。 ~~ 

1) 填充只会放置于 div 的顶部和/或底部。此 div 中的元素侧边会有填充。这样，您可以避免使用任何“方框模型数学”。请注意，如果向 div 自身添加任何侧边填充或边框，这些侧边填充或边框将与您定义的宽度相加，得出 *总计* 宽度。您也可以选择删除 div 中的元素的填充，并在该元素中另外放置一个没有任何宽度但具有设计所需填充的 div。

2) 由于这些列均为浮动列，因此未对其指定边距。如果必须添加边距，请避免在浮动方向一侧放置边距（例如：div 中的右边距设置为向右浮动）。在很多情况下，都可以改用填充。对于必须打破此规则的 div，应向该 div 的规则中添加“display:inline”声明，以控制某些版本的 Internet Explorer 会使边距翻倍的错误。

3) 由于可以在一个文档中多次使用类（并且一个元素可以应用多个类），因此已向这些列分配类名，而不是 ID。例如，必要时可堆叠两个侧栏 div。您可以根据个人偏好将这些名称轻松地改为 ID，前提是仅对每个文档使用一次。

4) 如果您更喜欢在右侧（而不是左侧）进行导航，只需使这些列向相反方向浮动（全部向右，而非全部向左），它们将按相反顺序显示。您无需在 HTML 源文件中移动 div。

*/
.sidebar1 {
	float: left;
	width: 16%;
	background-color: #FFF;
}
.content {
	padding:0;
	width: 84%;
	float: left;
}

/* ~~ 此分组的选择器为 .content 区域中的列表提供了空间 ~~ */


/* ~~ 导航列表样式（如果选择使用预先创建的 Spry 等弹出菜单，则可以删除此样式） ~~ */
ul.nav {
	list-style: none; /* 这将删除列表标记 */
	border-top: 1px solid #666; /* 这将为链接创建上边框 – 使用下边框将所有其它项放置在 LI 中 */
}
ul.nav li {
	border-bottom: 1px solid #666; /* 这将创建按钮间隔 */
}
ul.nav a, ul.nav a:visited { /* 对这些选择器进行分组可确保链接即使在访问之后也能保持其按钮外观 */
	padding: 5px 5px 5px 15px;
	display: block; /* 这将为链接赋予块属性，使其填满包含它的整个 LI。这样，整个区域都可以响应鼠标单击操作。 */
	text-decoration: none;
	background-color: #FFF;
	color:gray;
}
ul.nav a:hover, ul.nav a:active, ul.nav a:focus { /* 这将更改鼠标和键盘导航的背景和文本颜色 */
	background-color: #579EFF;
	color: #FFF;
}
-->
</style>
<script type="text/javascript">
	function exportWordFile(){
		doSubmit({actionType:'exportWordFile'});
		hideSplash();
	}
	function exportPdfFile(){
		doSubmit({actionType:'exportPdfFile'});
		hideSplash();
	}
	function saveDisplayCount(DISPLAY_COUNT){
		$("#DISPLAY_COUNT").val(DISPLAY_COUNT); 
		doSubmit({actionType:'saveDisplayCount'});
	}
	function queryDayWorks(startTime,endTime,content){
		$("#startTime").val(startTime); 
		$("#endTime").val(endTime); 
		$("#content").val(content); 
		doSubmit({actionType:'queryDayWork'});	
	}
	var insertBox;
	function showInsertBox() {
		if (!insertBox) {
			insertBox = new PopupBox('insertBox', '新增信息查看', {
				size : 'big',
				height : '300px',
				width : '450px',
				top : '30px'
			});
		}
		var url = "index?WmDayworkManageEdit&actionType=&operaType=insert";
		insertBox.sendRequest(url);
	}
	var queryBox;
	function showQueryBox() {
		if (!queryBox) {
			queryBox = new PopupBox('queryBox', '日报过滤查询', {
				size : 'big',
				height : '290px',
				width : '500px',
				top : '30px'
			});
		}
		var url = "index?WmDayworkManageQuery&actionType=";
		queryBox.sendRequest(url);
	}
	
	var displayCountBox;
	function setDisplayCountBox() {
		if (!displayCountBox) {
			displayCountBox = new PopupBox('displayCountBox', '设置展示条数', {
				size : 'big',
				height : '150px',
				width : '400px',
				top : '30px'
			});
		}
		var url = "index?WmDayworkManageSetDisplayCount&actionType=&operaType=query";
		displayCountBox.sendRequest(url);
	}
	
	function doSaveDayWork(index,useSplash){
		actionTypeTagId = "dayworkActionType"+index;
		if (useSplash){
			showSplash();			
		}
		postRequest('dayworkForm'+index,{actionType:'saveDayWork',showSplash:true,onComplete:function(responseText){
			hideSplash();
			if ("fail" != responseText){
				if (useSplash){
					hideSplash();					
				}
			}else{
				if (useSplash){
					hideSplash();					
				}
				jAlert('保存操作出错啦！');
			}
		}});
	}
	
	function doSaveNote(index,useSplash){
		actionTypeTagId = "noteActionType"+index;
		if (useSplash){
			showSplash();			
		}
		postRequest('noteForm'+index,{actionType:'saveNote',showSplash:true,onComplete:function(responseText){
			hideSplash();
			if ("fail" != responseText){
				if (useSplash){
					hideSplash();					
				}
			}else{
				if (useSplash){
					hideSplash();					
				}
				jAlert("保存操作出错啦！");
			}
		}});
		var currentNoteTitle =$("#"+"NOTE_TITLE"+index).val();
		if(currentNoteTitle != ''){
			$("#"+"current"+index).html(currentNoteTitle);
		}
		$("#index").val(index);
		formTagId = "noteForm"+index;
		triggerContent($("#"+"current"+index),formTagId);
	}
	
	function doDeleteDayWork(index){
		jConfirm('确认要删除该条记录吗？',function(r){
			if (r){
				actionTypeTagId = "dayworkActionType"+index;
				formTagId = "dayworkForm"+index;
				doSubmit({actionType:'deleteDayWork'});
			}
		});
	}
	
	function doDeleteNote(index){
		jConfirm('确认要删除该条记录吗？',function(r){
			if (r){
				actionTypeTagId = "noteActionType"+index;
				formTagId = "noteForm"+index;
				doSubmit({actionType:'deleteNote'});
			}
		});
	}	
	function doMoveUp(index){
		actionTypeTagId = "noteActionType"+index;
		formTagId = "noteForm"+index;
		doSubmit({actionType:'moveUp'});
		$("#rowIndex").val(index);
		$("#"+"current"+index).click();
		triggerContent($("#"+"current"+index),formTagId);
	}
	function doMoveDown(index){
		actionTypeTagId = "noteActionType"+index;
		formTagId = "noteForm"+index;
		doSubmit({actionType:'moveDown'});
		$("#rowIndex").val(index);
		$("#"+"current"+index).click();
		triggerContent($("#"+"current"+index),formTagId);
	}
	function triggerContent(currentA,formId,index){
		$("#index").val(index);
		$("#formSource").val(formId);
		$(".header").css("background-color","white");
		$("#"+formId+" .header").css("background-color","#D9E9Ff");
		$("ul.nav a").css({"background-color":"#FFF","color":"gray"});
		$(currentA).css({"background-color":"#579EFF","color":"white"});
	}
	
	function onMouseOut(){
		var index = $("#index").val();
		var dayworkFormId = "dayworkForm"+index;
		var formSource = $("#formSource").val();
		$("ul.nav a").css({"background-color":"#FFF","color":"gray"});
		if(dayworkFormId == formSource){
			$("ul.nav a:eq("+$("#index").val()+")").css({"background-color":"#579EFF","color":"white"});
		}else{
			$("#"+"current"+index).css({"background-color":"#579EFF","color":"white"});
		}
	}
	
	function focusToday(){
		if (ele("currentA")){
			$("#currentA").click();
		}
	} 
</script>
</head>
<body onload="focusToday()">
<div class="container" style="height:100%">
	<div class="sidebar1" style="height:100%;overflow:scroll;overflow-x:hidden">		
		<div style="position:fixed; top:0; left: 0;background-color:#FFF;width:16%;">
		<form id="form1" name="form1" method="post" action="<%=pageBean.getHandlerURL()%>">
	  	<table class="queryTable" style="border:none;background:none;">
	            <td>
	            &nbsp;<input type="button" name="button" id="button" value="查询" class="formbutton" onclick="showQueryBox()" />
                &nbsp;<input type="button" name="button" id="button" value="新增" class="formbutton" onclick="showInsertBox()" />
                &nbsp;<input type="button" name="button" id="button" value="设置" class="formbutton" onclick="setDisplayCountBox()" />
	            </td>
	        </tr>
	    </table>
  		<input type="hidden" name="actionType" id="actionType" />
  		<input type="hidden" name="startTime" id="startTime" value=""/>
  		<input type="hidden" name="endTime" id="endTime" value=""/>
  		<input type="hidden" name="content" id="content" value=""/>
  		<input type="hidden" name="DISPLAY_COUNT" id="DISPLAY_COUNT" />
    	</form>
		</div>
		<ul class="nav" style="margin-top: 36px;">
			<%
				List dayWorkRecords = (List) pageBean.getAttribute("dayWorkRecords");
				Date currentDate = DateUtil.getDateTime(DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,new Date()));
				pageBean.setRsList(dayWorkRecords);
				for (int i = 0; i < pageBean.getRsList().size(); i++) {
					DataRow row = (DataRow)pageBean.getRsList().get(i);
					Timestamp twTime = row.getTimestamp("TW_TIME");
					Date dateTemp = new Date(twTime.getTime());
					String weekText = DateUtil.getWeekText(dateTemp);
					String currentIdValue = "";
					if (DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,currentDate).equals(DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,dateTemp))){
						currentIdValue = "id='currentA'";
					}
			%>
			<li><a onmouseover="style.backgroundColor='#BEDAFF'" onmouseout="onMouseOut()" <%=currentIdValue%> onclick="triggerContent(this,'dayworkForm<%=i%>','<%=i%>')" href="#dayworkForm<%=i%>" ><%=pageBean.inputDate(i, "TW_TIME")%>&nbsp;&nbsp;<%=weekText%></a></li>
			<%
				}
			%>
			
			<%
				List noteRecords = (List) pageBean.getAttribute("noteRecords");
				pageBean.setRsList(noteRecords);
				for (int i = 0; i < pageBean.getRsList().size(); i++) {
					DataRow row = (DataRow)pageBean.getRsList().get(i);
					String currentIdValue = "";
					currentIdValue = "'current"+i+"'";
			%>
			<li><a onmouseover="style.backgroundColor='#BEDAFF'" onmouseout="onMouseOut()" id=<%=currentIdValue%> onclick="triggerContent(this,'noteForm<%=i%>','<%=i%>')" href="#noteForm<%=i%>"><%=pageBean.inputDate(i, "NOTE_TITLE")%></a></li>
			<%
				}
			%>			
		</ul>
		<!-- end .sidebar1 -->
	</div>
  <div class="content" style="height:100%;overflow:scroll;overflow-x:hidden">
<%
dayWorkRecords = (List) pageBean.getAttribute("dayWorkRecords");
pageBean.setRsList(dayWorkRecords);
int dayworkSize = dayWorkRecords.size();
for (int i = 0; i < pageBean.getRsList().size(); i++) {
	DataRow row = (DataRow)pageBean.getRsList().get(i);
	Timestamp twTime = row.getTimestamp("TW_TIME");
	Date dateTemp = new Date(twTime.getTime());
	long dateDiff = DateUtil.getDateDiff(currentDate, dateTemp, DateUtil.DAY);
	String weekText = DateUtil.getWeekText(dateTemp);
%>
    <div>
	<form id="dayworkForm<%=i%>" name="dayworkForm<%=i%>" method="post" action="<%=pageBean.getHandlerURL()%>">
	<table cellspacing="0" cellpadding="0" style="width:100%;margin-left:1px;">
		<tr>
			<td class="header">
			<span style="height: 30px;line-height:30px;">&nbsp;日期：<%=pageBean.inputDate(i,"TW_TIME")%> &nbsp;&nbsp;<%=weekText%>
	      	&nbsp;&nbsp;工作环境：
            <select id="TW_ENV" label="工作环境" name="TW_ENV" type="text"><%=pageBean.selectValue("TW_ENV",pageBean.inputValue(i,"TW_ENV"))%>
          	</select> 
          	<input type="hidden" name="TW_ID" id="TW_ID" value="<%=pageBean.inputValue(i,"TW_ID")%>" />			
			</span>
			<div id="__ToolBar__" style="float:right;padding-right: 10px;">
				<table border="0" cellpadding="0" cellspacing="1">
				<tr>
					<%
					if (dateDiff >=-1){
					%>
					<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSaveDayWork('<%=i%>',true)"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
					<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="doDeleteDayWork('<%=i%>')"><input value="&nbsp;" title="删除" type="button" class="delImgBtn" />删除</td></td>
					<%}%>
				</tr>
				</table>
			</div>      		
      		</td>    	
		</tr>
		<tr>
	      <td>
          <textarea style="width:100%" id="TW_CONTENT_<%=i%>" name="TW_CONTENT" cols="45" rows="15" class="xheditor {skin:'o2007blue',tools:'Bold,Italic,Underline,Strikethrough,FontColor,BackColor,SelectAll,Removeformat,List,Outdent,Indent,Link,Unlink,Fullscreen'}"><%=pageBean.inputValue(i,"TW_CONTENT")%></textarea>
          </td>
        </tr>
	</table>
	<input type="hidden" name="actionType" id="dayworkActionType<%=i%>" />
	<input type="hidden" name="index" id="index" />
	<input type="hidden" name="formSource" id="formSource" />
    </form>
    </div>
	<%
	if (dateDiff >=-1){
	%>
	<script>
	$(function(){
		$("#xhe<%=i%>_iframe").contents().find("body").focus(function (){
			var htmlContext = $("#xhe<%=i%>_iframe").contents().find("body").html();
			dayworkMap.put('<%=i%>',htmlContext);				
		});	
		
		document.getElementById("xhe<%=i%>_iframe").contentWindow.document.addEventListener('drop',function (e){
			$("#xhe<%=i%>_iframe").contents().find("body").focus();
		});	
		
		$("#xhe<%=i%>_iframe").contents().find("body").blur(function (){
			var curHtmlContext = $("#xhe<%=i%>_iframe").contents().find("body").html();
			var srcHtmlContext = dayworkMap.get('<%=i%>');
			if (curHtmlContext != srcHtmlContext){
				doSaveDayWork('<%=i%>',false);	
			}
			dayworkMap.remove('<%=i%>');
		});
		
		$("#xhe<%=i%>_iframe").contents().find("body").bind('keydown','ctrl+s',function (){
			doSaveDayWork('<%=i%>',true);
			return false;
		});
	});
	</script>
    <%}%>
<%
}
%>
<%
    	noteRecords = (List) pageBean.getAttribute("noteRecords");
		pageBean.setRsList(noteRecords);
		for (int i = 0; i < pageBean.getRsList().size(); i++) {
			DataRow row = (DataRow)pageBean.getRsList().get(i);
			String noteId = row.getString("NOTE_ID");
%>
    <div>
	<form id="noteForm<%=i%>" name="noteForm<%=i%>" method="post" action="<%=pageBean.getHandlerURL()%>">
	<table cellspacing="0" cellpadding="0"  style="width:100%;margin-left:1px;">
		<tr>
			<td class="header">
			<span style="height: 30px;line-height:30px;">
			标题：<input type="text" name="NOTE_TITLE" id="NOTE_TITLE<%=i%>" value="<%=pageBean.inputDate(i,"NOTE_TITLE")%>" />
          	</span>
          	<div id="__ToolBar__" style="float:right;padding-right: 10px;">
			<table border="0" cellpadding="0" cellspacing="1">
			<% 
			if (i >0){
			%>
			<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doMoveUp(<%=i%>);"><input id="upImgBtn" value="&nbsp;" title="上移" type="button" class="upImgBtn" style="margin-right:0px;" >上移</td>
			<%}%>
			<%if (i < (pageBean.getRsList().size()-1)) {%>
			<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doMoveDown(<%=i%>);triggerContent(current<%=i%>,'noteForm<%=i%>');"><input id="downImgBtn" value="&nbsp;" title="下移" type="button" class="downImgBtn" style="margin-right:0px;">下移</td>
   			<%}%>
   			<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSaveNote('<%=i%>',true)"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
			<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="doDeleteNote('<%=i%>')"><input value="&nbsp;" title="删除" type="button" class="delImgBtn" />删除</td>
			</table>
			<input type="hidden" name="NOTE_ID" id="NOTE_ID" value="<%=pageBean.inputValue(i,"NOTE_ID")%>" />
			<input type="hidden" name="USER_ID" id="USER_ID" value="<%=pageBean.inputValue(i,"USER_ID")%>" />
			<input type="hidden" name="NOTE_SORT" id="NOTE_SORT" value="<%=pageBean.inputValue(i,"NOTE_SORT")%>" />	
			</div>  
			</td>
		</tr>
		<tr>
	      <td>
          <textarea style="width:100%" id="NOTE_DESCRIBE_<%=i%>" name="NOTE_DESCRIBE" cols="45" rows="17" class="xheditor {skin:'o2007blue',tools:'Bold,Italic,Underline,Strikethrough,FontColor,BackColor,SelectAll,Removeformat,List,Outdent,Indent,Link,Unlink,Fullscreen'}"><%=pageBean.inputValue(i,"NOTE_DESCRIBE")%></textarea>
          </td>
        </tr>
	</table>
	<input type="hidden" name="actionType" id="noteActionType<%=i%>" />
	<input type="hidden" name="rowIndex" id="rowIndex" value=""/>	
    </form>
    </div>
	<script>
	$(function(){
		var index = (<%=dayworkSize%> + <%=i%>).toString();
		$("#xhe"+index+"_iframe").contents().find("body").focus(function (){
			var htmlContext = $("#xhe"+index+"_iframe").contents().find("body").html();
			daynoteMap.put('<%=i%>',htmlContext);				
		});	
		
		document.getElementById("xhe"+index+"_iframe").contentWindow.document.addEventListener('drop',function (e){
			$("#xhe"+index+"_iframe").contents().find("body").focus();
		});	
		
		$("#xhe"+index+"_iframe").contents().find("body").blur(function (){
			var curHtmlContext = $("#xhe"+index+"_iframe").contents().find("body").html();
			var srcHtmlContext = daynoteMap.get('<%=i%>');
			if (curHtmlContext != srcHtmlContext){
				doSaveNote('<%=i%>',false);	
			}
			daynoteMap.remove('<%=i%>');
		});
		
		$("#xhe"+index+"_iframe").contents().find("body").bind('keydown','ctrl+s',function (){
			doSaveNote('<%=i%>',true);
			return false;
		});
	});
	</script>    
<%
}
%>     
<!-- end .content -->
	</div>
  <!-- end .container -->
</div>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
