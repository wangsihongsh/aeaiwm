<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page language="java" import="java.util.*" %>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>日报审查</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
var grpIdBox;
function openGrpIdBox(){
	var handlerId = "GroupTreeSelect"; 
	if (!grpIdBox){
		grpIdBox = new PopupBox('grpIdBox','请选择所属工作组',{size:'normal',width:'300',top:'2px'});
	}
	var url = 'index?'+handlerId+'&targetId=GRP_ID&targetName=GRP_ID_NAME';
	grpIdBox.sendRequest(url);
}
function changeTab(tabId,userId){
    $("#currentTabId").val(tabId);
    $("#currentUserId").val(userId);
    
    doSubmit({actionType:'prepareDisplay'});
}
function queryEmp(){
   doSubmit({actionType:'prepareDisplay'});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__"  style="margin-top: 3px">
<table class="queryTable">
<tr>
  <td>
    &nbsp;日期&nbsp;
<input name="TW_TIME" type="text" class="text" id="TW_TIME" value="<%=pageBean.inputValue("TW_TIME")%>" size="10" readonly="readonly" label="时间" />
	&nbsp;<img id="TW_TIMEPicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
	&nbsp;<input type="button" name="button" id="button" value="查询" class="formbutton" onclick="doSubmit({actionType:'prepareDisplay'})" />
    &nbsp;<input type="button" name="button" id="button" value="前一天" class="formbutton" onclick="doSubmit({actionType:'beforeDay'})" /> 
    &nbsp;<input type="button" name="button" id="button" value="今天" class="formbutton" onclick="doSubmit({actionType:'currentDay'})" />
    &nbsp;<input type="button" name="button" id="button" value="后一天" class="formbutton" onclick="doSubmit({actionType:'nextDay'})" />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             工作组：&nbsp;<select id="grpId" label="工作组" name="grpId" class="select" onchange="queryEmp()"><%=pageBean.selectValue("grpId")%></select>
    &nbsp;<input type="hidden" name="button" id="button" value="查询" class="formbutton" onclick="doQuery()" />
  </td>
</tr>
</table>
</div>
<br/>

<div class="photobg1" id="tabHeader">
<%  List empRecords = (List)pageBean.getAttribute("empRecords");
	pageBean.setRsList(empRecords);
	int paramSize = pageBean.listSize();
	for(int i=0;i<paramSize;i++){
		if(!"Auditer".equals(pageBean.inputValue(i,"EMP_JOB"))){
	%>
    <div class="newarticle1" onclick="changeTab('<%=i%>','<%=pageBean.inputValue(i,"USER_ID")%>')"><%=pageBean.inputValue(i,"USER_NAME")%></div>
    	<%}%> 
	<%}%>
</div>

<%for(int i=0;i<paramSize;i++){%>
<div class="photobox newarticlebox" id="Layer<%=i%>" style="height:auto;display:none;overflow:hidden;">
<% 
if (pageBean.inputValue("currentUserId").equals(pageBean.inputValue(i,"USER_ID"))){
%>
<iframe id="UserFrame" src="index?DayExamination&USER_ID=<%=pageBean.inputValue(i,"USER_ID")%>&TW_TIME=<%=pageBean.inputValue("TW_TIME")%>&operaType=update&NOTE_ID=<%=pageBean.inputValue(i,"NOTE_ID")%>&EMP_JOB=<%=pageBean.inputValue(i,"EMP_JOB")%>" width="100%" height="auto" frameborder="0" scrolling="auto">
</iframe>
<%}%>

</div>
<%} %>

<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" id="currentUserId" name="currentUserId" value="<%=pageBean.inputValue("currentUserId")%>" />

<input type="hidden" id="currentTabId" name="currentTabId" value="<%=pageBean.inputValue("currentTabId")%>" />
</form>
<script type="text/javascript">
initCalendar('TW_TIME','%Y-%m-%d','TW_TIMEPicker');
datetimeValidators[0].set("yyyy-MM-dd").add("TW_TIME");
</script>
</body>
</html>

<%@include file="/jsp/inc/scripts.inc.jsp"%>
<script language="javascript">
var tab = new Tab('tab','tabHeader','Layer',0);
tab.focus(<%=pageBean.inputValue("currentTabId")%>);
$(function(){
	resetTabHeight(100);
	$("#UserFrame").height($("#form1").height()-100);
});
</script>