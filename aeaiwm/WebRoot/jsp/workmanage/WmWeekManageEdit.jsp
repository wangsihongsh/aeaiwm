<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page language="java" import="java.util.*" %>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<%
String currentSubTableId = pageBean.getStringValue("currentSubTableId");
String currentSubTableIndex = pageBean.getStringValue("currentSubTableIndex");
%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>周报管理</title>
<style>
tr.redTr td input{
	color:red;
}
tr.blackTr td input{
	color:black;
}
tr.greenTr td input{
	color:green;
}
.black-text{
	color:black !important;
}
</style>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script type="text/javascript" src="aeditors/xheditor/xheditor-1.2.2.min.js"></script>
<script type="text/javascript" src="aeditors/xheditor/xheditor_lang/zh-cn.js"></script>
<script type="text/javascript" src="js/jquery.hotkeys.js"></script>
<script language="javascript">
function saveMasterRecord(){
	$("#WW_STATE").removeAttr("disabled"); 
	var $exp = $("#EXPERIENCE").val();
	$exp = $.trim($exp);
	$('#WW_EXPERIENCE').val($exp);
	if($exp == "" && 'WmWeekfeel' == '<%=currentSubTableId%>'){
		writeErrorMsg('心得体会不能为空！');
		return;
	}
	if (validate()){
		if (ele("currentSubTableId")){
			var subTableId = $("#currentSubTableId").val();
			if (!checkEntryRecords(subTableId,'save')){
				return;
			}
		}
		showSplash();
		postRequest('form1',{actionType:'saveMasterRecord',showSplash:true,onComplete:function(responseText){
			hideSplash();
			if ("fail" != responseText){
				$('#operaType').val('update');
				$('#WW_ID').val(responseText);
				doSubmit({actionType:'prepareDisplay'});
			}
			else{
				hideSplash();
				writeErrorMsg('保存操作出错啦！');
			}
		}});
	}
}
function changeSubTable(targetTab){
	var $exp = $("#EXPERIENCE").val();
	if($.trim($exp) == "" && 'WmWeekfeel' == '<%=currentSubTableId%>'){
		writeErrorMsg('请完成心得体会！');
		tabComponent.interrupt();
		return;
	}
	if (validate() && checkEntryRecords('<%=currentSubTableId%>','changeSubTable')){
		showSplash();
		$("#WW_STATE").removeAttr("disabled"); 
		$('#operaType').val('update');
		postRequest('form1',{actionType:'saveMasterRecord',showSplash:true,onComplete:function(responseText){
			hideSplash();
			if ("fail" != responseText){
				$('#WW_ID').val(responseText);
				$('#currentSubTableId').val(targetTab);
				doSubmit({actionType:'prepareDisplay'});
			}
			else{
				hideSplash();
				writeErrorMsg('保存操作出错啦！');
			}
		}});				
	}else{
		if (targetTab != '<%=currentSubTableId%>'){
			tabComponent.interrupt();
		}
	}
}
function refreshPage(){
	doSubmit({actionType:'changeSubTable'});
}
function addEntryRecord(subTableId){
	$('#currentSubTableId').val(subTableId);
	doSubmit({actionType:'addEntryRecord'});
}
var arg="确认要删除该条记录吗？";
function deleteEntryRecord(subTableId){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	jConfirm(arg,function(r){
		if (r){
			$('#currentSubTableId').val(subTableId);
			doSubmit({actionType:'deleteEntryRecord'});	
		}
	});
}
var patrn = /^([1-9]\d{0,15}|0)(\.\d{1,1})?$/;
function checkEntryRecords(subTableId,source){
	var result = true;
	var currentRecordSize = $('#currentRecordSize').val();
	if ("WmWeekentry"==subTableId){
		for (var i=0;i < currentRecordSize;i++){
			if($('#ENTRY_GROUP'+"_"+i).val()==""){
				writeErrorMsg($("#ENTRY_GROUP_"+i).attr("label")+"未选择!");
				selectOrFocus('ENTRY_GROUP_'+i);
				return false;
			}
			if (!patrn.test($('#ENTRY_PLAN'+"_"+i).val())){
				writeErrorMsg($("#ENTRY_PLAN"+"_"+i).attr("label")+"小数点后只允许有一位!");
				selectOrFocus('ENTRY_PLAN'+'_'+i);
				return false;
			}
			if (ele('ENTRY_REALITY'+"_"+i)){
				if ("summary"==source || "save"==source ){
					if($('#ENTRY_REALITY'+"_"+i).val()!=""){
						if (!patrn.test($('#ENTRY_REALITY'+"_"+i).val())){
							writeErrorMsg($("#ENTRY_REALITY"+"_"+i).attr("label")+"小数点后只允许有一位!");
							selectOrFocus('ENTRY_REALITY'+'_'+i);
							return false;
						}
					}else{
						writeErrorMsg($("#ENTRY_REALITY"+"_"+i).attr("label")+"不能为空!");
						return false;
					}	
				}
			}
			if ("changeSubTable"!=source){
				if($('#ENTRY_FINISH'+"_"+i).val()==""){
					writeErrorMsg($("#ENTRY_FINISH_"+i).attr("label")+"未选择!");
					selectOrFocus('ENTRY_FINISH_'+i);
					return false;
				}
			}
		}
	}
	if ("WmPrepare"==subTableId){
		for (var i=0;i < currentRecordSize;i++){
			if($("#PRE_LOAD_"+i).val()==""){
				writeErrorMsg($("#PRE_LOAD_"+i).attr("label")+"不能为空!");
				selectOrFocus('PRE_LOAD_'+i);
				return false;
			}	
			if($("#PRE_DESCRIBE_"+i).val()==""){
				writeErrorMsg($("#PRE_DESCRIBE_"+i).attr("label")+"不能为空!");
				selectOrFocus('PRE_DESCRIBE_'+i);
				return false;
			}				
			if($("#PRE_LOAD_"+i).val()!=""){
				if (!patrn.test($('#PRE_LOAD'+"_"+i).val())){
					writeErrorMsg($("#PRE_LOAD"+"_"+i).attr("label")+"必须为整数!");
					selectOrFocus('PRE_LOAD'+'_'+i);
					return false;
				}
			}
		}
	}
	return result;
}
var insertSubRecordBox;
function insertSubRecordRequest(title,handlerId){
	if (!insertSubRecordBox){
		insertSubRecordBox = new PopupBox('insertSubRecordBox',title,{size:'normal',height:'300px',top:'10px'});
	}
	var url = 'index?'+handlerId+'&operaType=insert&WW_ID='+$('#WW_ID').val();
	insertSubRecordBox.sendRequest(url);	
}
var copySubRecordBox;
function copySubRecordRequest(title,handlerId,subPKField){
	clearSelection();
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	if (!copySubRecordBox){
		copySubRecordBox = new PopupBox('copySubRecordBox',title,{size:'normal',height:'300px',top:'10px'});
	}
	var url = 'index?'+handlerId+'&operaType=copy&'+subPKField+'='+$("#"+subPKField).val();
	copySubRecordBox.sendRequest(url);	
}
var viewSubRecordBox;
function viewSubRecordRequest(operaType,title,handlerId,subPKField){
	clearSelection();
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	if (!viewSubRecordBox){
		viewSubRecordBox = new PopupBox('viewSubRecordBox',title,{size:'normal',height:'300px',top:'10px'});
	}
	var url = 'index?'+handlerId+'&operaType='+operaType+'&'+subPKField+'='+$("#"+subPKField).val();
	viewSubRecordBox.sendRequest(url);
}
function deleteSubRecord(){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	jConfirm('确认要删除该条记录吗？',function(r){
		if (r){
			doSubmit({actionType:'deleteSubRecord'});	
		}
	});
}
function doMoveUp(){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	doSubmit({actionType:'moveUp'});
}
function doMoveDown(){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}	
	doSubmit({actionType:'moveDown'});
}
function SelectOnChanged(obj)
{
	obj.disabled="disable";
}

var twIdBox;
function openWtIdBox(){
	var handlerId ="WeekTimeListSelectList"; 
	if (!twIdBox){
		twIdBox = new PopupBox('twIdBox','请选择周报周期 ',{size:'normal',width:'400',top:'2px'});
	}
	var url = 'index?'+handlerId+'&targetId=WT_ID&targetBegin=WT_BEGIN&targetEnd=WT_END';
	twIdBox.sendRequest(url);
} 

function getEntryPrepareRecord(){
	jConfirm('确认提取上周未完成记录？',function(r){
		if (r){
			doSubmit({actionType:'getEntryPrepareRecord'});
		}
	});
}

function convertEntryRecord(subTableId){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	jConfirm('确认要将该计划转化为总结吗？',function(r){
		if (r){
			$('#currentSubTableId').val(subTableId);
			doSubmit({actionType:'convertEntryRecord'}); 
		}
	});
}

function convertFollowEntryRecord(subTableId){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	jConfirm('确认要将该计划转化为后续安排吗？',function(r){
		if (r){
			$('#currentSubTableId').val(subTableId);
			doSubmit({actionType:'convertPreRecord'}); 
		}
	});
}

function confirmed(){
	var targetTab = $('#currentSubTableId').val();
	jConfirm('是否要确认该周计划？',function(r){
		if (r){
			if (validate() && checkEntryRecords('<%=currentSubTableId%>','confirmed')){
				showSplash();
				$("#WW_STATE").removeAttr("disabled"); 
				$('#operaType').val('update');
				postRequest('form1',{actionType:'saveMasterRecord',showSplash:true,onComplete:function(responseText){
					hideSplash();
					if ("fail" != responseText){
						$('#WW_ID').val(responseText);
						$('#currentSubTableId').val(targetTab);
						doSubmit({actionType:'confirmed'});	
					}
					else{
						hideSplash();
						writeErrorMsg('保存操作出错啦！');
					}
				}});	
			}
			else{
				tabComponent.interrupt();
			}	
		}
	});
}
function summary(){
	var targetTab = $('#currentSubTableId').val();
	jConfirm('是否要总结确认该周总结？',function(r){
		if (r){
			if (validate() && checkEntryRecords('<%=currentSubTableId%>','summary')){
				showSplash();
				$("#WW_STATE").removeAttr("disabled"); 
				$('#operaType').val('update');
				postRequest('form1',{actionType:'saveMasterRecord',showSplash:true,onComplete:function(responseText){
					hideSplash();
					if ("fail" != responseText){
						$('#WW_ID').val(responseText);
						$('#currentSubTableId').val(targetTab);
						doSubmit({actionType:'summary'});
					}
					else{
						hideSplash();
						writeErrorMsg('保存操作出错啦！');
					}
					}});
			}
			else{
				tabComponent.interrupt();
			}
		}
	});
}

function controlWeekWorkEntryButton(currentState){
	if("0" == currentState){
		enableButton("delImgBtn");
	}
	else if("1" == currentState){
		disableButton("delImgBtn");
	}
}

</script>
</head>
<body >
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post" >
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<input  name="EMP_JOB" type="hidden" value="<%=pageBean.inputValue("EMP_JOB") %>"/>
<input  name="EMP_ID" type="hidden" value="<%=pageBean.inputValue("EMP_ID") %>"/>
<input  name="userId" type="hidden" value="<%=pageBean.inputValue("userId") %>"/>
<tr>
<% 
String STATE= pageBean.inputValue("STATE");
String canEdit=pageBean.inputValue("canEdit");
%>

<%if(!"SUMMARY".equals(pageBean.selectedValue("WW_STATE"))){%>
<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="saveMasterRecord();"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
<%}%>

<%if ("INITIALISE".equals(pageBean.selectedValue("WW_STATE"))){%>	
<% if(!pageBean.getBoolValue("ExtractShow")){%>
<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="getEntryPrepareRecord();"><input value="&nbsp;" type="button" class="moveImgBtn" title="提取" />提取</td>
<%}%>
<%}%>

<%if(STATE.equals("Audit")){%>
<%if (("Master".equals(pageBean.inputValue("EMP_JOB")) || "Auditer".equals(pageBean.inputValue("EMP_JOB"))) && "INITIALISE".equals(pageBean.selectedValue("WW_STATE")) && !pageBean.inputValue("EMP_ID").equals(pageBean.inputValue("userId"))){%>
<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="confirmed();"><input value="&nbsp;" type="button" class="submitImgBtn" title="提交" />提交</td>
<%}%>

<%if (("Master".equals(pageBean.inputValue("EMP_JOB")) || "Auditer".equals(pageBean.inputValue("EMP_JOB"))) && "CONFIRMED".equals(pageBean.selectedValue("WW_STATE")) && !pageBean.inputValue("EMP_ID").equals(pageBean.inputValue("userId")) ){%>
<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="summary();"><input value="&nbsp;" type="button" class="approveImgBtn" title="总结确认" />总结确认</td>
<%}%>
<%}%>

<%if(!STATE.equals("Audit")){%>
<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
<%}%>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr >
	<th width="100" colspan="3" nowrap>起止时间</th>
	<td><input id="WT_BEGIN" label="起始时间" name="WT_BEGIN" type="text" readonly="readonly" value="<%=pageBean.inputValue("WT_BEGIN")%>" size="10" class="text" />
		至
    	<input id="WT_END" label="结束时间" name="WT_END" type="text" readonly="readonly" value="<%=pageBean.inputValue("WT_END")%>" size="10" class="text" />
	<%if(canEdit.equals("false")){ %>
	&nbsp;<img id="weekTimeIdSelectImage" src="images/sta.gif" width="16" height="16" onclick="openWtIdBox()" />
	<%} %>
</td>

	<th width="100" nowrap>有效工作日</th>
	<td><input id="WW_DAY" label="有效工作日" name="WW_DAY" type="text" value="<%=pageBean.inputValue("WW_DAY")%>" size="25" class="text" />
</td>
</tr>
<tr>
	<th width="100" colspan="3" nowrap>工作量完成度</th>
	<td>
	  <% if("INITIALISE".equals(pageBean.selectedValue("WW_STATE")) || "SUMMARY".equals(pageBean.selectedValue("WW_STATE"))){%>
	  <input id="WW_COMPLETION_TEXT" label="工作量完成度" name="WW_COMPLETION_TEXT" type="text" value="<%=pageBean.selectedText("WW_COMPLETION")%>" size="25"  class="text" readonly="readonly"/>
	  <input id="WW_COMPLETION" label="工作量完成度" name="WW_COMPLETION" type="hidden" value="<%=pageBean.selectedValue("WW_COMPLETION")%>" />
	  <% }
	else{%>
	  <select id="WW_COMPLETION" label="周报记录状态" name="WW_COMPLETION" class="select" onchanged="SelectOnChanged(this)" style="width:222px;"><%=pageBean.selectValue("WW_COMPLETION")%></select>
	  <%} %>
</td>
	<th width="100" nowrap>周报记录状态</th>
	<td>
	<input id="WW_STATE_TEXT" label="周报记录状态" name="WW_STATE_TEXT" type="text" value="<%=pageBean.selectedText("WW_STATE")%>" size="25"  class="text" readonly="readonly"/>
	<input id="WW_STATE" label="周报记录状态" name="WW_STATE" type="hidden" value="<%=pageBean.selectedValue("WW_STATE")%>" />
</td>
</tr>
<input id="WT_ID" label="周报标识" name="WT_ID" type="hidden" value="<%=pageBean.inputValue("WT_ID")%>" size="15" class="text" />
</table>
<%if (!"insert".equals(pageBean.getOperaType())){%>
<div class="photobg1" id="tabHeader">
<%if ("INITIALISE".equals(pageBean.selectedValue("WW_STATE"))){%>	
 <div class="newarticle1" onclick="changeSubTable('WmWeekentry')">计划初稿</div>
<%}else if ("CONFIRMED".equals(pageBean.selectedValue("WW_STATE"))){%>	
 <div class="newarticle1" onclick="changeSubTable('WmWeekentry')">工作计划</div>
<%}else{%>
 <div class="newarticle1" onclick="changeSubTable('WmWeekentry')">工作总结</div>
<%}%> 
 <div class="newarticle1" onclick="changeSubTable('WmPrepare')">后续安排</div>
 <%if (!"INITIALISE".equals(pageBean.selectedValue("WW_STATE"))){%>
 <div class="newarticle1" onclick="changeSubTable('WmWeekfeel')">心得体会</div>
<%}%>
</div>
<%if ("WmWeekentry".equals(currentSubTableId)){ %>
<div class="photobox newarticlebox" id="Layer0" style="height:auto;">    
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
<%
if( ("Master".equals(pageBean.inputValue("EMP_JOB")) || "Auditer".equals(pageBean.inputValue("EMP_JOB")) 
		|| pageBean.inputValue("EMP_ID").equals(pageBean.inputValue("userId")) || "USER".equals(STATE)) && !"SUMMARY".equals(pageBean.selectedValue("WW_STATE"))){
%>
	<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="A" align="center" onclick="addEntryRecord('WmWeekentry')"><input id="createImgBtn" value="&nbsp;" title="新增" type="button" class="createImgBtn" />新增</td>
	<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="deleteEntryRecord('WmWeekentry')" ><input id="delImgBtn" value="&nbsp;" title="删除" type="button" class="delImgBtn"/>删除</td>
	<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="C" align="center" onclick="refreshPage()"><input value="&nbsp;" title="取消" type="button" class="cancelImgBtn" />取消</td>
	<td onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="doMoveUp()" class="bartdx" align="center"><input id="upImgBtn" value="&nbsp;" title="上移" type="button" class="upImgBtn" style="margin-right:0px;" />上移</td>
	<td onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="doMoveDown()" class="bartdx" align="center"><input id="downImgBtn" value="&nbsp;" title="下移" type="button" class="downImgBtn" style="margin-right:0px;" />下移</td>
<%if(pageBean.getBoolValue("showBtn")){ %>
	<td onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="convertFollowEntryRecord('WmWeekentry')" class="bartdx" align="center"><input id="downImgBtn" value="&nbsp;" title="下移" type="button" class="downImgBtn" style="margin-right:0px;" />转化为后续安排</td>
<%}%>   
<%}%> 
</tr>   
   </table>
</div>
<div style="margin:2px;">
<table border="0" cellpadding="0" cellspacing="0" class="dataTable ecSide" id="dataTable">
<thead>
  <tr>
    <th width="5%" align="center" nowrap="nowrap">序号</th>
	<th width="57%" align="center">任务描述</th>
	<th width="6%" align="center">计划天数</th>
<%
if (!"INITIALISE".equals(pageBean.selectedValue("WW_STATE"))){
%>	
	<th width="6%" align="center">实际天数</th>
	<th width="10%" align="center">是否完成</th>
<%}%>	
	<th width="10%" align="center">所属工作组</th>
	<th width="6%" align="center">记录状态</th>
  </tr>
</thead>
<tbody>
<%
List paramRecords = (List)pageBean.getAttribute("WmWeekentryRecords");
pageBean.setRsList(paramRecords);
int paramSize = pageBean.listSize();
for (int i=0;i < paramSize;i++){
	String trStyle = "";
	if (!"INITIALISE".equals(pageBean.selectedValue("WW_STATE"))){
		String entryState = pageBean.inputValue(i,"ENTRY_STATE");
		String entryFinish = pageBean.inputValue(i,"ENTRY_FINISH");
		if ("1".equals(entryFinish)){
			trStyle = "class='greenTr'";
		}else{
			if ("1".equals(entryState) && "0".equals(entryFinish)){
				trStyle = "class='redTr'";
			}
			else if ("0".equals(entryState) && "0".equals(entryFinish)){
				trStyle = "class='blackTr'";
			}			
		}
	}
%>
<tr <%=trStyle%> onmouseout="ECSideUtil.unlightRow(this);" onmouseover="ECSideUtil.lightRow(this);" onclick="ECSideUtil.selectRow(this,'form1');selectRow(this,{currentRecordIndex:'<%=i%>'});controlWeekWorkEntryButton('<%=pageBean.inputValue(i,"ENTRY_STATE")%>')">
	<td style="text-align:center"><%=i+1%>
		<input type="hidden" id="ENTRY_ID_<%=i%>" label="ENTRY_ID" name="ENTRY_ID_<%=i%>" value="<%=pageBean.inputValue(i,"ENTRY_ID")%>" />
		<input type="hidden" id="WW_ID_<%=i%>" label="WW_ID" name="WW_ID_<%=i%>" value="<%=pageBean.inputValue(i,"WW_ID")%>" />
		<input type="hidden" id="ENTRY_SORT_<%=i%>" label="计划排序" name="ENTRY_SORT_<%=i%>" value="<%=pageBean.inputValue(i,"ENTRY_SORT")%>" />
		<input id="state_<%=i%>" name="state_<%=i%>" label="计划状态" type="hidden" value="<%=pageBean.inputValue(i,"_state")%>" />
	</td>
	<td><input id="ENTRY_DESCRIBE_<%=i%>" label="任务描述" name="ENTRY_DESCRIBE_<%=i%>" type="text" value="<%=pageBean.inputValue(i,"ENTRY_DESCRIBE")%>" style="width:98%;"  class="text" <%if("1".equals(pageBean.inputValue(i,"ENTRY_STATE"))){%>readonly="readonly"<%}%>/>
</td>
	<td><input id="ENTRY_PLAN_<%=i%>" label="计划天数" name="ENTRY_PLAN_<%=i%>" type="text" value="<%=pageBean.inputValue(i,"ENTRY_PLAN")%>"  style="width:95%;" class="text" <%if("1".equals(pageBean.inputValue(i,"ENTRY_STATE"))){%>readonly="readonly"<%}%> />
</td>
<%
if (!"INITIALISE".equals(pageBean.selectedValue("WW_STATE"))){
%>
	<td><input id="ENTRY_REALITY_<%=i%>" label="实际天数" name="ENTRY_REALITY_<%=i%>" type="text"  value="<%=pageBean.inputDate(i,"ENTRY_REALITY")%>" style="width:95%;"  class="text" />
</td>
	<td><select id="ENTRY_FINISH_<%=i%>" label="是否完成" name="ENTRY_FINISH_<%=i%>" class="select" style="width:95%;"><%=pageBean.selectValue(i, "ENTRY_FINISH", "WORK_FINISHSelect")%></select>
</td>
<%}%>
<%
if (!"SUMMARY".equals(pageBean.selectedValue("WW_STATE"))){
%>
	<td><select id="ENTRY_GROUP_<%=i%>"  label="所属工作组" name="ENTRY_GROUP_<%=i%>" class="select" style="width:95%;"><%=pageBean.selectValue(i,"ENTRY_GROUP","grpId")%></select>
</td>
<%}%>
<%else{%>
	<td>
	<input id="ENTRY_GROUP_TEXT_<%=i%>" label="所属工作组" name="ENTRY_GROUP_TEXT_<%=i%>" type="text" value="<%=pageBean.inputValue(i, "ENTRY_GROUP_TEXT")%>" style="width:98%;"  class="black-text" readonly="readonly"/> 
	<input id="ENTRY_GROUP_<%=i%>" label="所属工作组" name="ENTRY_GROUP_<%=i%>" type="hidden"   value="<%=pageBean.inputValue(i,"ENTRY_GROUP")%>"/>
</td>
<%}%>
	<td><input id="ENTRY_STATE_TEXT_<%=i%>" label="记录状态" name="ENTRY_STATE_TEXT_<%=i%>" type="text" value="<%=pageBean.selectText("ENTRY_STATESelect",pageBean.inputValue(i, "ENTRY_STATE"))%>" style="width:95%;"  class="text" readonly="readonly"/> 
		<input id="ENTRY_STATE_<%=i%>" label="记录状态" name="ENTRY_STATE_<%=i%>" type="hidden"   value="<%=pageBean.inputValue(i,"ENTRY_STATE")%>"/> 
</td>
</tr>
<%}%>
</tbody>  
</table>
</div>
</div>
<input type="hidden" id="currentRecordSize" name="currentRecordSize" value="<%=pageBean.listSize()%>" />
<input type="hidden" id="currentRecordIndex" name="currentRecordIndex" value="" />
<script language="javascript">
<%for (int i=0;i < paramSize;i++){%>
	$("input[id$='_<%=i%>'],select[id$='_<%=i%>']").change(function(){
		if ($("#state_<%=i%>").val()==""){
			$("#state_<%=i%>").val('update');
		}
	});
<%}%>
setRsIdTag('currentRecordIndex');
$("#dataTable tr:eq(<%=pageBean.inputValue("currentRecordIndex")%>)").click();
</script>
<%}%>
<%if ("WmPrepare".equals(currentSubTableId)){ %>
<div class="photobox newarticlebox" id="Layer1" style="height:auto;">    
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
<%
if( ("Master".equals(pageBean.inputValue("EMP_JOB")) || "Auditer".equals(pageBean.inputValue("EMP_JOB")) 
		|| pageBean.inputValue("EMP_ID").equals(pageBean.inputValue("userId")) || "USER".equals(STATE)) &&  !"SUMMARY".equals(pageBean.selectedValue("WW_STATE"))){
%>
	<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="A" align="center" onclick="addEntryRecord('WmPrepare')"><input value="&nbsp;" title="新增" type="button" class="createImgBtn" />新增</td>
	<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="deleteEntryRecord('WmPrepare')"><input value="&nbsp;" title="删除" type="button" class="delImgBtn" />删除</td>
	<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="C" align="center" onclick="refreshPage()"><input value="&nbsp;" title="取消" type="button" class="cancelImgBtn" />取消</td>
	<td onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="doMoveUp()" class="bartdx" align="center"><input id="upImgBtn" value="&nbsp;" title="上移" type="button" class="upImgBtn" style="margin-right:0px;" />上移</td>
	<td onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="doMoveDown()" class="bartdx" align="center"><input id="downImgBtn" value="&nbsp;" title="下移" type="button" class="downImgBtn" style="margin-right:0px;" />下移</td>      
	<td onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="convertEntryRecord('WmPrepare')" class="bartdx" align="center"><input id="downImgBtn" value="&nbsp;" title="下移" type="button" class="downImgBtn" style="margin-right:0px;" />转化为计划总结</td>
<%}%>     
</tr>   
   </table>
</div>
<div style="margin:2px;">
<table border="0" cellpadding="0" cellspacing="0" class="dataTable ecSide" id="dataTable">
<thead>
  <tr>
    <th width="10%" align="center" nowrap="nowrap">序号</th>
	<th width="70%" align="center">计划内容</th>
	<th width="20%" align="center">工作量评估（天）</th>
  </tr>
</thead>
<tbody>
<%
List paramRecords = (List)pageBean.getAttribute("WmPrepareRecords");
pageBean.setRsList(paramRecords);
int paramSize = pageBean.listSize();
for (int i=0;i < paramSize;i++){
	String readOnlySytax = "readonly='readonly'";
	if( ("Master".equals(pageBean.inputValue("EMP_JOB")) || "Auditer".equals(pageBean.inputValue("EMP_JOB")) 
			|| pageBean.inputValue("EMP_ID").equals(pageBean.inputValue("userId")) || "USER".equals(STATE)) ){
		if (!"SUMMARY".equals(pageBean.selectedValue("WW_STATE"))){
			readOnlySytax = "";
		}
	}
%>
  <tr onmouseout="ECSideUtil.unlightRow(this);" onmouseover="ECSideUtil.lightRow(this);" onclick="ECSideUtil.selectRow(this,'form1');selectRow(this,{currentRecordIndex:'<%=i%>'})">
	<td style="text-align:center"><%=i+1%>
		<input type="hidden" id="PRE_ID_<%=i%>" label="PRE_ID" name="PRE_ID_<%=i%>" value="<%=pageBean.inputValue(i,"PRE_ID")%>" />
		<input type="hidden" id="WW_ID_<%=i%>" label="WW_ID" name="WW_ID_<%=i%>" value="<%=pageBean.inputValue(i,"WW_ID")%>" />
		<input type="hidden" id="PRE_SORT_<%=i%>" label="计划排序" name="PRE_SORT_<%=i%>" value="<%=pageBean.inputValue(i,"PRE_SORT")%>" />
		<input id="state_<%=i%>" name="state_<%=i%>" type="hidden" value="<%=pageBean.inputValue(i,"_state")%>" />
	</td>
	<td><input id="PRE_DESCRIBE_<%=i%>" label="计划内容" name="PRE_DESCRIBE_<%=i%>" type="text" value="<%=pageBean.inputValue(i,"PRE_DESCRIBE")%>" <%=readOnlySytax%> style="width:99%;"   class="text" />
</td>
	<td><input id="PRE_LOAD_<%=i%>" label="工作量评估" name="PRE_LOAD_<%=i%>" type="text" value="<%=pageBean.inputValue(i,"PRE_LOAD")%>" <%=readOnlySytax%> style="width:95%;"  class="text" />
</td>
  </tr>
<%}%>
</tbody>  
</table>
</div>
</div>
<input type="hidden" id="currentRecordSize" name="currentRecordSize" value="<%=pageBean.listSize()%>" />
<input type="hidden" id="currentRecordIndex" name="currentRecordIndex" value="" />
<script language="javascript">
<%for (int i=0;i < paramSize;i++){%>
	$("input[id$='_<%=i%>'],select[id$='_<%=i%>']").change(function(){
		if ($("#state_<%=i%>").val()==""){
			$("#state_<%=i%>").val('update');
		}
	});
<%}%>
setRsIdTag('currentRecordIndex');

$("#dataTable tr:eq(<%=pageBean.inputValue("currentRecordIndex")%>)").click();

</script>
<%}%>
<%if ("WmWeekfeel".equals(currentSubTableId)){ %>
<div class="photobox" id="Layer2" style="height:auto;margin-top: 20px">
	<textarea style="width:100%" id="EXPERIENCE" name="EXPERIENCE" cols="45" rows="15" 
		class="xheditor {skin:'o2007blue',tools:'Bold,Italic,Underline,Strikethrough,FontColor,BackColor,SelectAll,Removeformat,List,Outdent,Indent,Link,Unlink,Fullscreen'}">
		<%=pageBean.inputValue("WW_EXPERIENCE")%>
	</textarea>
</div>
<%}%>
<input type="hidden" name="WW_EXPERIENCE" id="WW_EXPERIENCE" value="<%=pageBean.inputValue("WW_EXPERIENCE")%>"/>
<input type="hidden" id="currentSubTableId" name="currentSubTableId" value="<%=pageBean.inputValue("currentSubTableId")%>" />
<%}%>

<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="STATE" id="STATE" value="<%=pageBean.inputValue("STATE") %>"/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="WW_ID" name="WW_ID" value="<%=pageBean.inputValue4DetailOrUpdate("WW_ID","")%>" />
<input type="hidden" id="USER_ID" name="USER_ID" value="<%=pageBean.inputValue("USER_ID")%>" />
</form>
<script>
$(function(){
	$("#xhe0_iframe").contents().find("body").bind('keydown','ctrl+s',function (){
		saveMasterRecord();
		return false;
	});
});
var tabComponent = new Tab('tab','tabHeader','Layer',<%=currentSubTableIndex%>);
initDetailOpertionImage();
requiredValidator.add("WW_DAY");
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
