<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>周报管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script type="text/javascript">
function controlWeekWorkEntryButton(currentState){
	if("INITIALISE" == currentState){
		enableButton("delImgBtn");
	}
	else{
		disableButton("delImgBtn");
	}
}

function exportExcelFile(){
	doSubmit({actionType:'exportExcelFile'});
	hideSplash();
}
function dateJude(){
	var sdate = $("#START_TIME").val();
	var edate = $("#END_TIME").val();
	if(dateCompare(sdate,edate)){
		doQuery();
	}
	
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ToolBar__">
<table class="toolBar" border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="A" align="center" onclick="doRequest('insertRequest')"><input value="&nbsp;" title="新增" type="button" class="createImgBtn" />新增</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="E" align="center" onclick="doRequest('updateRequest')"><input value="&nbsp;" title="编辑" type="button" class="editImgBtn" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="C" align="center" onclick="doRequest('copyRequest')"><input value="&nbsp;" title="复制" type="button" class="copyImgBtn" />复制</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="V" align="center" onclick="doRequest('viewDetail')"><input value="&nbsp;" title="查看" type="button" class="detailImgBtn" />查看</td>   
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="doDelete($('#'+rsIdTagId).val());"><input id="delImgBtn" value="&nbsp;" title="删除" type="button" class="delImgBtn" />删除</td>
</tr>
</table>
</div>
<div id="__ParamBar__">
<table class="queryTable"><tr><td>
&nbsp;工作内容<input id="PRE_DESCRIBE" label="工作内容" name="PRE_DESCRIBE" type="text" value="<%=pageBean.inputValue("PRE_DESCRIBE")%>" size="15" class="text" />
&nbsp;开始时间<input id="START_TIME" label="起始时间" name="START_TIME" type="text" value="<%=pageBean.inputDate("START_TIME")%>" size="15" class="text" /><img id="ST_TIMEPicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
&nbsp;结束时间<input id="END_TIME" label="结束时间" name="END_TIME" type="text" value="<%=pageBean.inputDate("END_TIME")%>" size="15" class="text" /><img id="ET_TIMEPicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
&nbsp;<input type="button" name="button" id="button" value="查询" class="formbutton" onclick="dateJude()" />
&nbsp;<input type="button" name="button" id="button" value="导出为excel文档" class="formbutton" onclick="exportExcelFile()" />
</td></tr></table>
</div>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" csvFileName="周报管理.csv"
retrieveRowsCallback="process" xlsFileName="周报管理.xls"
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |export|extend|status"
width="100%" rowsDisplayed="15"
listWidth="100%" 
height="390px"
>
<ec:row styleClass="odd" ondblclick="clearSelection();doRequest('viewDetail')" oncontextmenu="selectRow(this,{WW_ID:'${row.WW_ID}'});refreshConextmenu()" onclick="selectRow(this,{WW_ID:'${row.WW_ID}'});controlWeekWorkEntryButton('${row.WW_STATE}')">
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="100" property="WT_BEGIN" title="起始时间"   />
	<ec:column width="100" property="WT_END" title="结束时间"   />
	<ec:column width="100" property="WW_DAY" title="有效工作日"   />
	<ec:column width="100" property="WW_STATE" title="状态" mappingItem="WW_STATE"  />
	<ec:column width="100" property="WW_COMPLETION" title="工作完成度"   />
</ec:row>
</ec:table>
<input type="hidden" name="USER_ID" id="USER_ID" value="" />
<input type="hidden" name="WT_ID" id="WT_ID" value=""/>
<input type="hidden" name="WW_ID" id="WW_ID" value="" />
<input type="hidden" name="actionType" id="actionType" />
<script language="JavaScript">
initCalendar('START_TIME','%Y-%m-%d','ST_TIMEPicker');
datetimeValidators[0].set("yyyy-MM-dd").add("START_TIME");
initCalendar('END_TIME','%Y-%m-%d','ET_TIMEPicker');
datetimeValidators[0].set("yyyy-MM-dd").add("END_TIME");
setRsIdTag('WW_ID');
var ectableMenu = new EctableMenu('contextMenu','ec_table');
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
