<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.agileai.hotweb.domain.PageBean"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page language="java" import="java.util.*" %>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>周报审查</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
var grpIdBox;
function openGrpIdBox(){
	var handlerId = "GroupTreeSelect"; 
	if (!grpIdBox){
		grpIdBox = new PopupBox('grpIdBox','请选择所属工作组',{size:'normal',width:'300',top:'2px'});
	}
	var url = 'index?'+handlerId+'&targetId=GRP_ID&targetName=GRP_ID_NAME';
	grpIdBox.sendRequest(url);
}
function changeTab(tabId,userId){
    $('#currentTabId').val(tabId);
    $('#currentUserId').val(userId);
    doSubmit({actionType:'prepareDisplay'});
}
function exportExcleFile(){
	 doSubmit({actionType:'exportExcleFile'});
	 hideSplash();
}
var twIdBox;
function openWtIdBox(){
	var handlerId ="WeekTimeListSelectList";
	if (!twIdBox){
		twIdBox = new PopupBox('twIdBox','请选择周报周期 ',{size:'normal',width:'400',top:'2px'});
	}
	var url = 'index?'+handlerId+'&targetId=WT_ID&targetBegin=WT_BEGIN&targetEnd=WT_END';
	twIdBox.sendRequest(url);
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__">
<table class="queryTable">
<tr>
  <td>
    &nbsp;起止日期&nbsp;
<input name="WT_BEGIN" type="text" class="text" id="WT_BEGIN" value="<%=pageBean.inputValue("WT_BEGIN")%>" size="10" readonly="readonly" label="起始时间" />
-
<input name="WT_END" type="text" class="text" id="WT_END" value="<%=pageBean.inputValue("WT_END")%>" size="10" readonly="readonly" label="终止时间" />
&nbsp;<img id="weekTimeIdSelectImage" src="images/sta.gif" width="16" height="16" onclick="openWtIdBox()" />
    &nbsp;<input type="button" name="button" id="button" value="上一周" class="formbutton" onclick="doSubmit({actionType:'beforeWeek'})" /> 
    &nbsp;<input type="button" name="button" id="button" value="本周" class="formbutton" onclick="doSubmit({actionType:'currentWeek'})" />
    &nbsp;<input type="button" name="button" id="button" value="下一周" class="formbutton" onclick="doSubmit({actionType:'nextWeek'})" />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             工作组：&nbsp;<select id="grpId" label="工作组" name="grpId" class="select" onchange="doQuery()"><%=pageBean.selectValue("grpId")%></select>
<%if(pageBean.getBoolValue("exportExcleShow")){ %>          
    &nbsp;<input type="button" name="button" id="button" value="导出excel文件" class="formbutton" onclick="exportExcleFile()" /> 
<%} %>
    &nbsp;<input type="hidden" name="button" id="button" value="查询" class="formbutton" onclick="doQuery()" />
  </td>
</tr>
<input id="WT_ID" label="周期标识" name="WT_ID" type="hidden" value="<%=pageBean.inputValue("WT_ID")%>" size="15" class="text" />
</table>
</div>
<br/>
<div class="photobg1" id="tabHeader">
<%  List empRecords = (List)pageBean.getAttribute("empRecords");
	pageBean.setRsList(empRecords);
	int paramSize = pageBean.listSize();
	for(int i=0;i<paramSize;i++){%>
    <div class="newarticle1" onclick="changeTab('<%=i%>','<%=pageBean.inputValue(i,"USER_ID")%>')" ><%=pageBean.inputValue(i,"USER_NAME")%>
    <input type="hidden" id="EMP_JOB<%=i%>" name="EMP_JOB<%=i%>" value="<%=pageBean.inputValue(i,"EMP_JOB")%>"/>
    </div>
<%}%>
</div>
<%  
for(int i=0;i<paramSize;i++){
%>
<div class="photobox newarticlebox" id="Layer<%=i%>" style="height:427px;;display:none;overflow:hidden;">
<% 
if (pageBean.inputValue("currentUserId").equals(pageBean.inputValue(i,"USER_ID"))){
%>
<iframe id="UserFrame" src="index?WmWeekManageEdit&WW_ID=<%=pageBean.inputValue("WW_ID")%>&operaType=update&WT_ID=<%=pageBean.inputValue("WT_ID")%>&EMP_ID=<%=pageBean.inputValue(i,"USER_ID")%>&STATE=Audit&EMP_JOB=<%=pageBean.inputValue("EMP_JOB")%>&USER_ID=<%=pageBean.inputValue("currentUserId")%>" width="100%" height="450" frameborder="0" scrolling="auto"></iframe>
<%}%>
</div>
<% } %>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value=""/>
<input type="hidden" id="currentUserId" name="currentUserId" value="<%=pageBean.inputValue("currentUserId")%>"/>
<input type="hidden" id="currentTabId" name="currentTabId" value="<%=pageBean.inputValue("currentTabId")%>" />
<input type="hidden" id="currentWeekId" name="currentWeekId" value="<%=pageBean.inputValue("currentWeekId")%>" />
</form>
</body>
</html>
<script language="javascript">
<%if(pageBean.getBoolValue("isRootNode")){%>
setImgDisabled('saveImgBtn',true);
setImgDisabled('delImgBtn',true);
setImgDisabled('copyImgBtn',true);
setImgDisabled('moveImgBtn',true);
setImgDisabled('upImgBtn',true);
setImgDisabled('downImgBtn',true);
<%}%>
</script>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
<script language="javascript">
var tab = new Tab('tab','tabHeader','Layer',0);
tab.focus(<%=pageBean.inputValue("currentTabId")%>);
$(function(){
	resetTabHeight(100);
	$("#UserFrame").height($("#form1").height()-100);
});
</script>
