<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html; charset=UTF-8" errorPage="/jsp/frame/Error.jsp" %>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html>
<head>
<title>主页面</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<script type="text/javascript" src="js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="js/util.js"></script>
<link href="css/style.css" rel="stylesheet" type="text/css">
<style type="text/css">
.footer {
	height: 31px;
	line-height: 31px;
	font-size: 12px;
	text-align: center;
	color: #666666;
	width: 99%;
}
.mainContent{
	font-size: 14px;
	margin:8px 10px;
	line-height:25px;
}

div#wrap {
 padding-top: 87px;
}
</style>
</head>
<body>
<div id="wrap">
<table width="96%"  border="0" align="center">
  <tr>
    <td width="80" align="center"><img src="images/index/mainpic.jpg"></td>
    <td>
<div class="mainContent">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AEAI WM工作管理系统是一种基于AEAI DP开发的JavaWeb系统，通过对企业的各项目组和人员岗位进行明确的划分，以日报、周报的方式对个人工作、项目工作进行敏捷、准确的管理，便于企业对公司员工进行任务分配、确认，从而提升各项目团队工作效率。</div>
<div class="mainContent">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AEAI WM工作管理系统包括分组管理、日报管理、日报审核、周期定义、周报管理、周报审核六个模块。分组管理用来设置项目组，分配审查者、项目负责人、参与者；日报管理部分用来填写日报和后三天的计划，同一分组的组员可以相互查看其他的成员的日报信息，了解工作情况；周报管理部分用来新增周报，查看往期周报记录，并对本周的记录进行修改，管理人员可以编辑其他组员的周报记录。其中，日报提供导出为Word功能，周报提供导出Excel功能。</div>
<div class="mainContent">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沈阳数通畅联软件技术有限公司是耕耘于应用系统集成领域的专业技术团队，提供基于Java体系的SOA整合产品，主要包括应用集成平台（AEAI ESB）、门户集成平台（AEAI Portal）、流程集成平台（AEAI BPM）、应用开发平台（AEAI DP，也称Miscdp）、主数据管理平台（AEAI MDM）。其他产品更多详情请参见官网：www.agileai.com,或致电：024-22962011。</div>
</td>
  </tr>
</table>
</div>
</body>
</html>
<script language="javascript">
</script>
