/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : aeaiwm

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2018-02-28 14:05:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for security_group
-- ----------------------------
DROP TABLE IF EXISTS `security_group`;
CREATE TABLE `security_group` (
  `GRP_ID` varchar(36) NOT NULL,
  `GRP_CODE` varchar(32) DEFAULT NULL,
  `GRP_NAME` varchar(32) DEFAULT NULL,
  `GRP_PID` varchar(36) DEFAULT NULL,
  `GRP_DESC` varchar(128) DEFAULT NULL,
  `GRP_STATE` varchar(1) DEFAULT NULL,
  `GRP_SORT` int(11) DEFAULT NULL,
  `GRP_TYPE` varchar(32) DEFAULT NULL,
  `GRP_RANK` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`GRP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_group
-- ----------------------------
INSERT INTO `security_group` VALUES ('00000000-0000-0000-00000000000000000', 'Root', '数通畅联', null, null, '1', '0', 'company', '1');
INSERT INTO `security_group` VALUES ('19B18951-77A8-41CD-8876-7CBD1C560CB9', 'ShenYang', '沈阳分公司', '00000000-0000-0000-00000000000000000', '', '1', '6', 'company', '1');
INSERT INTO `security_group` VALUES ('44723B8E-EDD8-4571-87F1-6A99927E24C6', 'Tech', '技术部', '19B18951-77A8-41CD-8876-7CBD1C560CB9', '', '1', '1', 'department', '2');
INSERT INTO `security_group` VALUES ('66465842-2219-4FEA-A2E0-CFABF0E19073', 'Market', '营销部', '19B18951-77A8-41CD-8876-7CBD1C560CB9', '', '1', '3', 'department', '2');
INSERT INTO `security_group` VALUES ('68ABC008-0547-4CC3-B074-152EFF3812FA', 'HR', '人力部', '19B18951-77A8-41CD-8876-7CBD1C560CB9', '', '1', '2', 'department', '2');

-- ----------------------------
-- Table structure for security_group_auth
-- ----------------------------
DROP TABLE IF EXISTS `security_group_auth`;
CREATE TABLE `security_group_auth` (
  `GRP_AUTH_ID` varchar(36) NOT NULL,
  `GRP_ID` varchar(36) DEFAULT NULL,
  `RES_TYPE` varchar(32) DEFAULT NULL,
  `RES_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`GRP_AUTH_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_group_auth
-- ----------------------------

-- ----------------------------
-- Table structure for security_rg_auth
-- ----------------------------
DROP TABLE IF EXISTS `security_rg_auth`;
CREATE TABLE `security_rg_auth` (
  `RG_AUTH_ID` char(36) NOT NULL,
  `RG_ID` char(36) DEFAULT NULL,
  `RES_TYPE` varchar(32) DEFAULT NULL,
  `RES_ID` char(36) DEFAULT NULL,
  PRIMARY KEY (`RG_AUTH_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_rg_auth
-- ----------------------------

-- ----------------------------
-- Table structure for security_role
-- ----------------------------
DROP TABLE IF EXISTS `security_role`;
CREATE TABLE `security_role` (
  `ROLE_ID` varchar(36) NOT NULL,
  `ROLE_CODE` varchar(32) DEFAULT NULL,
  `ROLE_NAME` varchar(32) DEFAULT NULL,
  `ROLE_PID` varchar(36) DEFAULT NULL,
  `ROLE_DESC` varchar(128) DEFAULT NULL,
  `ROLE_STATE` varchar(32) DEFAULT NULL,
  `ROLE_SORT` int(11) DEFAULT NULL,
  `ROLE_TYPE` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_role
-- ----------------------------
INSERT INTO `security_role` VALUES ('00000000-0000-0000-00000000000000000', 'System', '系统角色', null, null, '1', null, '');
INSERT INTO `security_role` VALUES ('299BFEBC-8CF0-4D8F-A4A0-08520F14D257', 'Employee', '普通员工', '00000000-0000-0000-00000000000000000', '', '1', '5', 'part');
INSERT INTO `security_role` VALUES ('2A355C2A-59B2-4DDC-9425-A1DC237660D4', 'GenManager', '经理', '00000000-0000-0000-00000000000000000', '', '1', '3', 'part');
INSERT INTO `security_role` VALUES ('8752C789-44BD-4941-AEF1-B9CC8B5C3CC6', 'ITManager', 'IT主管', '00000000-0000-0000-00000000000000000', '', '1', '4', 'part');
INSERT INTO `security_role` VALUES ('B805EEFE-C067-4B54-95CE-4C50AF5692D9', 'HRManager', '人力负责人', '00000000-0000-0000-00000000000000000', '', '1', '7', 'part');
INSERT INTO `security_role` VALUES ('DBF04F17-7DD2-4072-90BA-198E724A672F', 'WorkMaster', '工作负责人', '00000000-0000-0000-00000000000000000', '', '1', '6', 'part');

-- ----------------------------
-- Table structure for security_role_auth
-- ----------------------------
DROP TABLE IF EXISTS `security_role_auth`;
CREATE TABLE `security_role_auth` (
  `ROLE_AUTH_ID` varchar(36) NOT NULL,
  `ROLE_ID` varchar(36) DEFAULT NULL,
  `RES_TYPE` varchar(32) DEFAULT NULL,
  `RES_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`ROLE_AUTH_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_role_auth
-- ----------------------------
INSERT INTO `security_role_auth` VALUES ('1BCADE82-2FC5-420D-82D3-34C4F38942C7', '299BFEBC-8CF0-4D8F-A4A0-08520F14D257', 'Menu', 'D3582A2A-3173-4F92-B1AD-2F999A2CBE18');
INSERT INTO `security_role_auth` VALUES ('28DB0750-419E-4BFA-8BB6-2700DBDC84E9', 'DBF04F17-7DD2-4072-90BA-198E724A672F', 'Menu', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_role_auth` VALUES ('2E12DA12-FD53-4232-9B00-CA47CBDB4D64', '299BFEBC-8CF0-4D8F-A4A0-08520F14D257', 'Menu', '9D55885C-7B56-4B04-847F-B8F511761C3A');
INSERT INTO `security_role_auth` VALUES ('3012C3F4-04B2-4792-B0CE-C05C3C533FE9', 'DBF04F17-7DD2-4072-90BA-198E724A672F', 'Menu', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477');
INSERT INTO `security_role_auth` VALUES ('3D9CB951-CD45-402E-B3C8-69A544873620', '2A355C2A-59B2-4DDC-9425-A1DC237660D4', 'Menu', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477');
INSERT INTO `security_role_auth` VALUES ('586BF6FA-4066-4C65-9352-C0A17B6E2170', '8752C789-44BD-4941-AEF1-B9CC8B5C3CC6', 'Menu', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_role_auth` VALUES ('5917E828-600C-4642-BD5D-A10686756D1F', 'B805EEFE-C067-4B54-95CE-4C50AF5692D9', 'Menu', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477');
INSERT INTO `security_role_auth` VALUES ('61502166-3B36-414E-9F16-B3829ECE8DFC', '299BFEBC-8CF0-4D8F-A4A0-08520F14D257', 'Menu', 'DBB9E261-FCED-4A2D-8AE5-7088C1869E21');
INSERT INTO `security_role_auth` VALUES ('64118B54-5D38-4B23-B691-E13CC2F88EFF', '2A355C2A-59B2-4DDC-9425-A1DC237660D4', 'Menu', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_role_auth` VALUES ('8FA4F184-779E-4428-A795-0B7BFFBB9705', '299BFEBC-8CF0-4D8F-A4A0-08520F14D257', 'Menu', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477');
INSERT INTO `security_role_auth` VALUES ('A00209BF-3B8A-42E4-B4F1-B0B77519908E', '299BFEBC-8CF0-4D8F-A4A0-08520F14D257', 'Menu', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_role_auth` VALUES ('B9296FAD-890C-4F08-9361-A1DCF405D055', 'B805EEFE-C067-4B54-95CE-4C50AF5692D9', 'Menu', '5E50DF0C-E687-4B0D-85F8-256B8ADBB5A3');
INSERT INTO `security_role_auth` VALUES ('BAD10D68-091B-434E-AC17-1A4B611A36FC', 'DBF04F17-7DD2-4072-90BA-198E724A672F', 'Menu', '03F9F226-31DA-4A98-89CB-00CFAC5D26A4');
INSERT INTO `security_role_auth` VALUES ('C14B2E18-0A44-4750-AA2A-F231A1DC40E4', '2A355C2A-59B2-4DDC-9425-A1DC237660D4', 'Menu', '11DCF8E9-3EB8-4C83-A8B9-51C0C530F114');
INSERT INTO `security_role_auth` VALUES ('C25EAEEA-4E3F-463B-A370-4537742F466C', '299BFEBC-8CF0-4D8F-A4A0-08520F14D257', 'Menu', '00000000-0000-0000-00000000000000001');
INSERT INTO `security_role_auth` VALUES ('C8A79161-9524-4DC5-8A75-1172F12C5B30', 'B805EEFE-C067-4B54-95CE-4C50AF5692D9', 'Menu', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_role_auth` VALUES ('CD0C0C64-152B-4ED9-BB70-E0685FCD0A6E', '299BFEBC-8CF0-4D8F-A4A0-08520F14D257', 'Menu', '0CE7BBD4-0896-4D76-AB88-C185BF22C66F');

-- ----------------------------
-- Table structure for security_role_group_rel
-- ----------------------------
DROP TABLE IF EXISTS `security_role_group_rel`;
CREATE TABLE `security_role_group_rel` (
  `GRP_ID` varchar(36) NOT NULL,
  `ROLE_ID` varchar(36) NOT NULL,
  `RG_ID` char(36) NOT NULL,
  PRIMARY KEY (`RG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_role_group_rel
-- ----------------------------
INSERT INTO `security_role_group_rel` VALUES ('68ABC008-0547-4CC3-B074-152EFF3812FA', '299BFEBC-8CF0-4D8F-A4A0-08520F14D257', '050C01BE-58DB-4442-B574-05DAC810EBCC');
INSERT INTO `security_role_group_rel` VALUES ('44723B8E-EDD8-4571-87F1-6A99927E24C6', '299BFEBC-8CF0-4D8F-A4A0-08520F14D257', '08279F41-D8CD-414C-A95C-4A1D0443E48E');
INSERT INTO `security_role_group_rel` VALUES ('66465842-2219-4FEA-A2E0-CFABF0E19073', '299BFEBC-8CF0-4D8F-A4A0-08520F14D257', '0D551369-6193-49DF-B9C3-C98E8DB6CF30');
INSERT INTO `security_role_group_rel` VALUES ('19B18951-77A8-41CD-8876-7CBD1C560CB9', '2A355C2A-59B2-4DDC-9425-A1DC237660D4', '5FFBD5A3-8F88-4E79-BBE1-3A8E3C8AB7EB');
INSERT INTO `security_role_group_rel` VALUES ('44723B8E-EDD8-4571-87F1-6A99927E24C6', 'DBF04F17-7DD2-4072-90BA-198E724A672F', 'C1D5EA9D-2B83-42C1-B752-20EBFDE31B42');
INSERT INTO `security_role_group_rel` VALUES ('68ABC008-0547-4CC3-B074-152EFF3812FA', 'B805EEFE-C067-4B54-95CE-4C50AF5692D9', 'C5E53972-F65D-4391-9140-CBC3011387C9');
INSERT INTO `security_role_group_rel` VALUES ('66465842-2219-4FEA-A2E0-CFABF0E19073', 'DBF04F17-7DD2-4072-90BA-198E724A672F', 'C73DF0F1-7FD4-4DD4-BC2B-962E1E3FDA0A');
INSERT INTO `security_role_group_rel` VALUES ('00000000-0000-0000-00000000000000000', '00000000-0000-0000-00000000000000000', 'DD15AFD7-F504-4805-91FD-B1459D7B8CF2');

-- ----------------------------
-- Table structure for security_user
-- ----------------------------
DROP TABLE IF EXISTS `security_user`;
CREATE TABLE `security_user` (
  `USER_ID` varchar(36) NOT NULL,
  `USER_CODE` varchar(32) DEFAULT NULL,
  `USER_NAME` varchar(32) DEFAULT NULL,
  `USER_PWD` varchar(32) DEFAULT NULL,
  `USER_SEX` varchar(1) DEFAULT NULL,
  `USER_DESC` varchar(128) DEFAULT NULL,
  `USER_STATE` varchar(32) DEFAULT NULL,
  `USER_SORT` int(11) DEFAULT NULL,
  `USER_MAIL` varchar(64) DEFAULT NULL,
  `USER_PHONE` varchar(64) DEFAULT NULL,
  `DISPLAY_COUNT` int(11) DEFAULT NULL,
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_user
-- ----------------------------
INSERT INTO `security_user` VALUES ('5BCB9801-7C45-4F11-9E71-634D7913A145', 'CS05', '翟小五', 'DD9F799386E27C0D8CC93556AB819203', 'M', '', '1', '5', '', '', '20');
INSERT INTO `security_user` VALUES ('6565BFC1-E424-455F-88EB-AD602D7A19C2', 'CS01', '张老大', 'DAE6CDC2B49F7C32164BE2AF1A7916AA', 'M', '', '1', '1', '', '', '20');
INSERT INTO `security_user` VALUES ('7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24', 'admin', '管理员', '21232F297A57A5A743894A0E4A801FC3', 'M', '内置账户，勿删！！', '1', '0', '', '', '5');
INSERT INTO `security_user` VALUES ('8102F8E4-BDA0-42AE-9A45-2A08DC8C5F7A', 'CS03', '孙小三', '25EC07853BDC7B87DD021F5580C70855', 'M', '', '1', '3', '', '', '20');
INSERT INTO `security_user` VALUES ('9B84A740-64D6-4CAA-A4F2-D3B285B8969B', 'CS02', '赵小二', '39661AF3C6AFE19E95700A0E7373446A', 'M', '', '1', '2', '', '', '20');
INSERT INTO `security_user` VALUES ('B4EA9168-CB1E-4069-BAA9-39DE3FECA45D', 'CS04', '赵小四', 'FBAF749E6A085DCBBE41FF5C030EBF98', 'M', '', '1', '4', '', '', '20');

-- ----------------------------
-- Table structure for security_user_auth
-- ----------------------------
DROP TABLE IF EXISTS `security_user_auth`;
CREATE TABLE `security_user_auth` (
  `USER_AUTH_ID` varchar(36) NOT NULL,
  `USER_ID` varchar(36) DEFAULT NULL,
  `RES_TYPE` varchar(32) DEFAULT NULL,
  `RES_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`USER_AUTH_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_user_auth
-- ----------------------------

-- ----------------------------
-- Table structure for security_user_group_rel
-- ----------------------------
DROP TABLE IF EXISTS `security_user_group_rel`;
CREATE TABLE `security_user_group_rel` (
  `GRP_ID` varchar(36) NOT NULL,
  `USER_ID` varchar(36) NOT NULL,
  `GU_ID` char(36) NOT NULL,
  PRIMARY KEY (`GU_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_user_group_rel
-- ----------------------------
INSERT INTO `security_user_group_rel` VALUES ('44723B8E-EDD8-4571-87F1-6A99927E24C6', '5BCB9801-7C45-4F11-9E71-634D7913A145', '3D7EAF0B-B3C7-4DAE-B1AC-61FF8178E009');
INSERT INTO `security_user_group_rel` VALUES ('44723B8E-EDD8-4571-87F1-6A99927E24C6', '6565BFC1-E424-455F-88EB-AD602D7A19C2', '4DA9AADA-3DC2-4017-9BB3-401EEB31B507');
INSERT INTO `security_user_group_rel` VALUES ('44723B8E-EDD8-4571-87F1-6A99927E24C6', '8102F8E4-BDA0-42AE-9A45-2A08DC8C5F7A', '61A10216-BABB-40CC-96D5-DA3138D3EDFE');
INSERT INTO `security_user_group_rel` VALUES ('68ABC008-0547-4CC3-B074-152EFF3812FA', '9B84A740-64D6-4CAA-A4F2-D3B285B8969B', '64781D14-F976-4C13-9F48-B17DD57B98DB');
INSERT INTO `security_user_group_rel` VALUES ('66465842-2219-4FEA-A2E0-CFABF0E19073', '9B84A740-64D6-4CAA-A4F2-D3B285B8969B', '7E4CC620-723E-4FC9-85EE-C2017D85FA82');
INSERT INTO `security_user_group_rel` VALUES ('19B18951-77A8-41CD-8876-7CBD1C560CB9', '6565BFC1-E424-455F-88EB-AD602D7A19C2', '800C05FA-DAD3-49E2-9D5B-CE13E4DF100C');
INSERT INTO `security_user_group_rel` VALUES ('00000000-0000-0000-00000000000000000', '7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24', '8C386EEE-9A99-4B92-87E0-E7CD4FFC30D7');
INSERT INTO `security_user_group_rel` VALUES ('44723B8E-EDD8-4571-87F1-6A99927E24C6', 'B4EA9168-CB1E-4069-BAA9-39DE3FECA45D', 'CD167027-9D72-44F0-999B-572A7E45328E');

-- ----------------------------
-- Table structure for security_user_rg_rel
-- ----------------------------
DROP TABLE IF EXISTS `security_user_rg_rel`;
CREATE TABLE `security_user_rg_rel` (
  `URG_ID` char(36) NOT NULL,
  `USER_ID` varchar(36) DEFAULT NULL,
  `RG_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`URG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_user_rg_rel
-- ----------------------------
INSERT INTO `security_user_rg_rel` VALUES ('0C9220C7-88F2-4937-8156-BD56B70134BE', 'B4EA9168-CB1E-4069-BAA9-39DE3FECA45D', '08279F41-D8CD-414C-A95C-4A1D0443E48E');
INSERT INTO `security_user_rg_rel` VALUES ('28D6D012-B0CD-4661-9EBD-4FF424744A3F', '6565BFC1-E424-455F-88EB-AD602D7A19C2', 'C1D5EA9D-2B83-42C1-B752-20EBFDE31B42');
INSERT INTO `security_user_rg_rel` VALUES ('3FFCE9A0-D0B4-4284-8F7D-5A819210E493', '5BCB9801-7C45-4F11-9E71-634D7913A145', '08279F41-D8CD-414C-A95C-4A1D0443E48E');
INSERT INTO `security_user_rg_rel` VALUES ('4C43C307-915A-4FCA-AF03-8F34449C44DA', '8102F8E4-BDA0-42AE-9A45-2A08DC8C5F7A', '08279F41-D8CD-414C-A95C-4A1D0443E48E');
INSERT INTO `security_user_rg_rel` VALUES ('505904E2-D704-4E58-B527-1BB5276F5338', '6565BFC1-E424-455F-88EB-AD602D7A19C2', '08279F41-D8CD-414C-A95C-4A1D0443E48E');
INSERT INTO `security_user_rg_rel` VALUES ('54D3252D-9665-4E26-88FC-D642D1995283', 'DB9869DF-56AE-4D35-9B86-75E1FF0EA2EE', '0D551369-6193-49DF-B9C3-C98E8DB6CF30');
INSERT INTO `security_user_rg_rel` VALUES ('56103D69-5F0C-48A4-8542-C8C82F2A67A8', '9B84A740-64D6-4CAA-A4F2-D3B285B8969B', '0D551369-6193-49DF-B9C3-C98E8DB6CF30');
INSERT INTO `security_user_rg_rel` VALUES ('5CE72893-F614-4890-9AB3-522BB8D10809', '9B84A740-64D6-4CAA-A4F2-D3B285B8969B', 'C5E53972-F65D-4391-9140-CBC3011387C9');
INSERT INTO `security_user_rg_rel` VALUES ('6698E435-4772-4C96-9986-A491FD292B69', 'B4EA9168-CB1E-4069-BAA9-39DE3FECA45D', 'C1D5EA9D-2B83-42C1-B752-20EBFDE31B42');
INSERT INTO `security_user_rg_rel` VALUES ('9ADC21AD-AF75-49A6-AB0C-5EEF3D7B0AFD', '6565BFC1-E424-455F-88EB-AD602D7A19C2', '5FFBD5A3-8F88-4E79-BBE1-3A8E3C8AB7EB');
INSERT INTO `security_user_rg_rel` VALUES ('A13F3340-5AC2-4C4F-AECE-076FA143E74E', '8102F8E4-BDA0-42AE-9A45-2A08DC8C5F7A', 'C1D5EA9D-2B83-42C1-B752-20EBFDE31B42');
INSERT INTO `security_user_rg_rel` VALUES ('BD8D5AAE-5D62-46D1-8E87-5DFD630F5DD7', '9B84A740-64D6-4CAA-A4F2-D3B285B8969B', 'C73DF0F1-7FD4-4DD4-BC2B-962E1E3FDA0A');

-- ----------------------------
-- Table structure for sys_codelist
-- ----------------------------
DROP TABLE IF EXISTS `sys_codelist`;
CREATE TABLE `sys_codelist` (
  `TYPE_ID` varchar(32) NOT NULL,
  `CODE_ID` varchar(32) NOT NULL,
  `CODE_NAME` varchar(32) DEFAULT NULL,
  `CODE_DESC` varchar(128) DEFAULT NULL,
  `CODE_SORT` int(11) DEFAULT NULL,
  `CODE_FLAG` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`TYPE_ID`,`CODE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_codelist
-- ----------------------------
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'Bottom', 'BottomHandler', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'Building', 'BuildingHandler', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'Homepage', 'HomepageHandler', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'Logo', 'LogoHandler', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'MainWin', 'MainWinHandler', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'MenuTree', 'MenuTreeHandler', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'Navigater', 'NavigaterHandler', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('BOOL_DEFINE', 'N', '否', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('BOOL_DEFINE', 'Y', '是', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('CODE_TYPE_GROUP', 'app_code_define', '应用编码', 'null', '1', '1');
INSERT INTO `sys_codelist` VALUES ('CODE_TYPE_GROUP', 'sys_code_define', '系统编码', '系统编码123a1', '3', '1');
INSERT INTO `sys_codelist` VALUES ('EMP_JOB_TYPE', 'Auditer', '审查人', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('EMP_JOB_TYPE', 'Master', '负责人', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('EMP_JOB_TYPE', 'Participant', '参与人', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('ENTRY_STATE', '0', '初始化', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('ENTRY_STATE', '1', '已确认', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('FUNCTION_TYPE', 'funcmenu', '功能菜单', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('FUNCTION_TYPE', 'funcnode', '功能节点', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('GROUP_TYPE', '0', '虚拟组', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('GROUP_TYPE', '1', '实际组', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('HANDLER_TYPE', 'MAIN', '主处理器', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('HANDLER_TYPE', 'OTHER', '其他处理器', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('INFO_TYPE', 'menu', '目录', null, '1', '1');
INSERT INTO `sys_codelist` VALUES ('INFO_TYPE', 'part', '角色', null, '2', '1');
INSERT INTO `sys_codelist` VALUES ('MENUTREE_CASCADE', '0', '关闭', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('MENUTREE_CASCADE', '1', '展开', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('OPER_CTR_TYPE', 'disableMode', '不能操作', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('OPER_CTR_TYPE', 'hiddenMode', '隐藏按钮', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('ORGNAZITION_RANK', '1', '一级', null, '1', '1');
INSERT INTO `sys_codelist` VALUES ('ORGNAZITION_RANK', '2', '二级', null, '2', '1');
INSERT INTO `sys_codelist` VALUES ('ORGNAZITION_RANK', '3', '三级', null, '3', '1');
INSERT INTO `sys_codelist` VALUES ('ORGNAZITION_TYPE', 'company', '公司', null, '1', '1');
INSERT INTO `sys_codelist` VALUES ('ORGNAZITION_TYPE', 'department', '部门', null, '2', '1');
INSERT INTO `sys_codelist` VALUES ('POSITION_TYPE', 'dummy_postion', '虚拟岗位', 'null', '1', '1');
INSERT INTO `sys_codelist` VALUES ('POSITION_TYPE', 'real_postion', '实际岗位', 'null', '1', '1');
INSERT INTO `sys_codelist` VALUES ('RES_TYPE', 'IMAGE', '图片文件', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('RES_TYPE', 'ISO', '镜像文件', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('RES_TYPE', 'VIDEO', '视频文件', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('SYS_VALID_TYPE', '0', '无效', 'null', '2', '1');
INSERT INTO `sys_codelist` VALUES ('SYS_VALID_TYPE', '1', '有效', 'null', '1', '1');
INSERT INTO `sys_codelist` VALUES ('TW_ENV_TYPE', 'BusinessOffice', '出差办公', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('TW_ENV_TYPE', 'HomeOffice', '在家办公', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('TW_ENV_TYPE', 'InOffice', '公司办公', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('UNIT_TYPE', 'dept', '部门', '', '10', '1');
INSERT INTO `sys_codelist` VALUES ('UNIT_TYPE', 'org', '机构', '', '20', '1');
INSERT INTO `sys_codelist` VALUES ('UNIT_TYPE', 'post', '岗位', '', '30', '1');
INSERT INTO `sys_codelist` VALUES ('USER_SEX', 'F', '女', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('USER_SEX', 'M', '男', 'null', '1', '1');
INSERT INTO `sys_codelist` VALUES ('WEEK_WORK_STATE', 'CONFIRMED', '已确认', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('WEEK_WORK_STATE', 'INITIALISE', '初始化', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('WEEK_WORK_STATE', 'SUMMARY', '已总结', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('WORK_FINISH', '0', '未完成', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('WORK_FINISH', '1', '已完成', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('WORK_FINISH_VALUE', '0', '0', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('WORK_FINISH_VALUE', '10', '10', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('WORK_FINISH_VALUE', '100', '100', '', '11', '1');
INSERT INTO `sys_codelist` VALUES ('WORK_FINISH_VALUE', '20', '20', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('WORK_FINISH_VALUE', '30', '30', '', '4', '1');
INSERT INTO `sys_codelist` VALUES ('WORK_FINISH_VALUE', '40', '40', '', '5', '1');
INSERT INTO `sys_codelist` VALUES ('WORK_FINISH_VALUE', '50', '50', '', '6', '1');
INSERT INTO `sys_codelist` VALUES ('WORK_FINISH_VALUE', '60', '60', '', '7', '1');
INSERT INTO `sys_codelist` VALUES ('WORK_FINISH_VALUE', '70', '70', '', '8', '1');
INSERT INTO `sys_codelist` VALUES ('WORK_FINISH_VALUE', '80', '80', '', '9', '1');
INSERT INTO `sys_codelist` VALUES ('WORK_FINISH_VALUE', '90', '90', '', '10', '1');

-- ----------------------------
-- Table structure for sys_codetype
-- ----------------------------
DROP TABLE IF EXISTS `sys_codetype`;
CREATE TABLE `sys_codetype` (
  `TYPE_ID` varchar(32) NOT NULL,
  `TYPE_NAME` varchar(32) DEFAULT NULL,
  `TYPE_GROUP` varchar(32) DEFAULT NULL,
  `TYPE_DESC` varchar(128) DEFAULT NULL,
  `IS_CACHED` char(1) DEFAULT NULL,
  `IS_UNITEADMIN` char(1) DEFAULT NULL,
  `IS_EDITABLE` char(1) DEFAULT NULL,
  `LEGNTT_LIMIT` varchar(6) DEFAULT NULL,
  `CHARACTER_LIMIT` char(1) DEFAULT NULL,
  `EXTEND_SQL` char(1) DEFAULT NULL,
  `SQL_BODY` varchar(512) DEFAULT NULL,
  `SQL_COND` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`TYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_codetype
-- ----------------------------
INSERT INTO `sys_codetype` VALUES ('AuthedHandlerId', '认证Handler定义', 'sys_code_define', '', 'Y', 'Y', 'Y', '', 'B', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('BOOL_DEFINE', '布尔定义', 'sys_code_define', '', 'Y', 'Y', 'Y', '1', 'C', '', '', '');
INSERT INTO `sys_codetype` VALUES ('CODE_TYPE_GROUP', '编码类型分组', 'app_code_define', '编码类型分组', null, null, null, null, null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('EMP_JOB_TYPE', '工作组岗位', 'sys_code_define', '', 'N', 'Y', 'Y', '32', 'C', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('ENTRY_STATE', '计划审核状态', 'sys_code_define', '', 'Y', 'Y', 'Y', '5', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('FUNCTION_TYPE', '功能类型', 'sys_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('GROUP_TYPE', '工作组类型', 'sys_code_define', null, 'Y', 'Y', 'Y', '', null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('HANDLER_TYPE', '控制器类型', 'sys_code_define', '', 'N', 'Y', 'Y', '32', 'C', null, null, null);
INSERT INTO `sys_codetype` VALUES ('INFO_TYPE', '信息类型', 'sys_code_define', '', 'Y', 'Y', 'Y', '32', 'C', null, null, null);
INSERT INTO `sys_codetype` VALUES ('MENUTREE_CASCADE', '是否展开', 'sys_code_define', '', 'Y', 'Y', 'Y', '1', 'N', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('OPER_CTR_TYPE', '操作控制类型', 'sys_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('ORGNAZITION_RANK', '组织级别', 'app_code_define', null, 'Y', 'Y', 'Y', null, null, 'N', null, null);
INSERT INTO `sys_codetype` VALUES ('ORGNAZITION_TYPE', '组织类别', 'app_code_define', '', 'Y', 'Y', 'Y', '', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('POSITION_TYPE', '岗位类型', 'app_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('RES_TYPE', '资源类型', 'sys_code_define', '', 'N', 'Y', 'Y', '16', 'C', null, null, null);
INSERT INTO `sys_codetype` VALUES ('SYS_VALID_TYPE', '有效标识符', 'app_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('TEST', '测试', 'sys_code_define', '', 'Y', 'Y', 'Y', '', 'X', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('TW_ENV_TYPE', '办公环境', 'sys_code_define', '', 'Y', 'Y', 'Y', '20', 'C', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('UNIT_TYPE', '单位类型', 'app_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('USER_SEX', '性别类型', 'sys_code_define', '', 'N', 'Y', 'Y', '16', 'C', null, null, null);
INSERT INTO `sys_codetype` VALUES ('WEEK_WORK_STATE', '周报记录状态', 'sys_code_define', '', 'Y', 'Y', 'Y', '10', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('WORK_FINISH', '工作完成情况', 'sys_code_define', '', 'Y', 'Y', 'Y', '5', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('WORK_FINISH_VALUE', '工作完成度', 'sys_code_define', '', 'Y', 'Y', 'Y', '10', 'N', 'N', '', '');

-- ----------------------------
-- Table structure for sys_function
-- ----------------------------
DROP TABLE IF EXISTS `sys_function`;
CREATE TABLE `sys_function` (
  `FUNC_ID` varchar(36) NOT NULL,
  `FUNC_NAME` varchar(64) DEFAULT NULL,
  `FUNC_TYPE` varchar(32) DEFAULT NULL,
  `MAIN_HANDLER` varchar(36) DEFAULT NULL,
  `FUNC_PID` varchar(36) DEFAULT NULL,
  `FUNC_STATE` char(1) DEFAULT NULL,
  `FUNC_SORT` int(11) DEFAULT NULL,
  `FUNC_DESC` varchar(256) DEFAULT NULL,
  `FUNC_ICON` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`FUNC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_function
-- ----------------------------
INSERT INTO `sys_function` VALUES ('00000000-0000-0000-00000000000000000', '工作管理系统', 'funcmenu', null, null, '1', null, null, null);
INSERT INTO `sys_function` VALUES ('00000000-0000-0000-00000000000000001', '系统管理', 'funcmenu', '', '00000000-0000-0000-00000000000000000', '1', '100', '', null);
INSERT INTO `sys_function` VALUES ('03F9F226-31DA-4A98-89CB-00CFAC5D26A4', '日报审查', 'funcnode', '4D875DF5-6252-4692-81B8-B426F3E4E0CE', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477', '1', '4', '', null);
INSERT INTO `sys_function` VALUES ('0CE7BBD4-0896-4D76-AB88-C185BF22C66F', '周报查看', 'funcnode', '50C6517B-B972-459E-AA14-9A24055085C0', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477', '1', '8', '', null);
INSERT INTO `sys_function` VALUES ('11DCF8E9-3EB8-4C83-A8B9-51C0C530F114', '分组管理', 'funcnode', '5AD31E4E-0B7B-42DF-91FA-644AD9FED4D8', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477', '1', '10', '', null);
INSERT INTO `sys_function` VALUES ('5E50DF0C-E687-4B0D-85F8-256B8ADBB5A3', '周期定义', 'funcnode', 'A277504E-CF9D-42FE-8F9A-892F99306C6F', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477', '1', '9', '', null);
INSERT INTO `sys_function` VALUES ('5FDEE3AB-6D32-4C5F-AD65-EF1EE5FFBAE6', '附件管理', 'funcnode', '1F617665-FC8B-4E8C-ABE4-540C363A17A8', '00000000-0000-0000-00000000000000001', '1', '8', '', null);
INSERT INTO `sys_function` VALUES ('67BA273A-DD31-48D0-B78C-1D60D5316074', '系统日志', 'funcnode', '494DF09B-7573-4CCA-85C1-97F4DC58C86B', '00000000-0000-0000-00000000000000001', '1', '7', null, null);
INSERT INTO `sys_function` VALUES ('692B0D37-2E66-4E82-92B4-E59BCF76EE76', '编码管理', 'funcnode', 'B4FE5722-9EA6-47D8-8770-D999A3F6A354', '00000000-0000-0000-00000000000000001', '1', '6', null, null);
INSERT INTO `sys_function` VALUES ('8C84B439-2788-4608-89C4-8F5AA076D124', '组织机构', 'funcnode', '439949F0-C6B7-49FF-8ED1-2A1B5062E7B9', '00000000-0000-0000-00000000000000001', '1', '1', null, null);
INSERT INTO `sys_function` VALUES ('9D55885C-7B56-4B04-847F-B8F511761C3A', '日报管理', 'funcnode', '5DF667FA-AA5C-4334-8968-B7D54EC72B4D', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477', '1', '1', '', null);
INSERT INTO `sys_function` VALUES ('A0334956-426E-4E49-831B-EB00E37285FD', '编码类型', 'funcnode', '9A16D554-F989-438A-B92D-C8C8AC6BF9B8', '00000000-0000-0000-00000000000000001', '1', '5', null, null);
INSERT INTO `sys_function` VALUES ('AD5188DB-BF4F-4CFB-853E-8CA469D3E477', '工作管理', 'funcmenu', '', '00000000-0000-0000-00000000000000000', '1', '99', '', null);
INSERT INTO `sys_function` VALUES ('C977BC31-C78F-4B16-B0C6-769783E46A06', '功能管理', 'funcnode', '46C52D33-8797-4251-951F-F7CA23C76BD7', '00000000-0000-0000-00000000000000001', '1', '4', null, null);
INSERT INTO `sys_function` VALUES ('CC0B472B-0AEB-4812-ABDA-8C7E91434CB7', '人员管理', 'funcnode', 'F7746D8A-F3AE-4004-8B65-644E6A0CE628', '00000000-0000-0000-00000000000000001', '1', '3', '', null);
INSERT INTO `sys_function` VALUES ('D3582A2A-3173-4F92-B1AD-2F999A2CBE18', '修改密码', 'funcnode', '88882DB9-967F-430E-BA9C-D0BBBBD2BD0C', '00000000-0000-0000-00000000000000001', '1', '9', '', null);
INSERT INTO `sys_function` VALUES ('DBB9E261-FCED-4A2D-8AE5-7088C1869E21', '周报管理', 'funcnode', 'F70D8B75-2AD1-4286-846D-AD8C1D11E3DD', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477', '1', '7', '个人日报管理', null);
INSERT INTO `sys_function` VALUES ('DFE8BE4C-4024-4A7B-8DF2-630003832AE9', '角色管理', 'funcnode', '0CE03AD4-FF29-4FDB-8FEE-DA8AA38B649F', '00000000-0000-0000-00000000000000001', '1', '2', null, null);

-- ----------------------------
-- Table structure for sys_handler
-- ----------------------------
DROP TABLE IF EXISTS `sys_handler`;
CREATE TABLE `sys_handler` (
  `HANLER_ID` varchar(36) NOT NULL,
  `HANLER_CODE` varchar(64) DEFAULT NULL,
  `HANLER_TYPE` varchar(32) DEFAULT NULL,
  `HANLER_URL` varchar(128) DEFAULT NULL,
  `FUNC_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`HANLER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_handler
-- ----------------------------
INSERT INTO `sys_handler` VALUES ('08764984-5C97-4AD8-B089-D1FC1789171D', 'SecurityUserRGManageList', 'OTHER', '', 'CC0B472B-0AEB-4812-ABDA-8C7E91434CB7');
INSERT INTO `sys_handler` VALUES ('0B968C9D-3F3D-4941-97EA-12DD4E7806D8', 'WmDayworkManageSetDisplayCount', 'OTHER', 'index?WmDayworkManageSetDisplayCount', '03F9F226-31DA-4A98-89CB-00CFAC5D26A4');
INSERT INTO `sys_handler` VALUES ('0CE03AD4-FF29-4FDB-8FEE-DA8AA38B649F', 'SecurityRoleTreeManage', 'MAIN', 'index?SecurityRoleTreeManage', 'DFE8BE4C-4024-4A7B-8DF2-630003832AE9');
INSERT INTO `sys_handler` VALUES ('1B501177-83FB-4B65-8368-8E998867FA7E', 'SecurityUserQuery', 'OTHER', '', 'CC0B472B-0AEB-4812-ABDA-8C7E91434CB7');
INSERT INTO `sys_handler` VALUES ('1CEE9A6A-8052-4410-A981-EFACC354B0C6', 'WmWeekManageEdit', 'OTHER', 'index?WmWeekManageEdit', 'DBB9E261-FCED-4A2D-8AE5-7088C1869E21');
INSERT INTO `sys_handler` VALUES ('1F617665-FC8B-4E8C-ABE4-540C363A17A8', 'WcmGeneralGroup8ContentList', 'MAIN', 'index?WcmGeneralGroup8ContentList', '5FDEE3AB-6D32-4C5F-AD65-EF1EE5FFBAE6');
INSERT INTO `sys_handler` VALUES ('439949F0-C6B7-49FF-8ED1-2A1B5062E7B9', 'SecurityGroupList', 'MAIN', 'index?SecurityGroupList', '8C84B439-2788-4608-89C4-8F5AA076D124');
INSERT INTO `sys_handler` VALUES ('46C52D33-8797-4251-951F-F7CA23C76BD7', 'FunctionTreeManage', 'MAIN', 'index?FunctionTreeManage', 'C977BC31-C78F-4B16-B0C6-769783E46A06');
INSERT INTO `sys_handler` VALUES ('494DF09B-7573-4CCA-85C1-97F4DC58C86B', 'SysLogQueryList', 'MAIN', 'index?SysLogQueryList', '67BA273A-DD31-48D0-B78C-1D60D5316074');
INSERT INTO `sys_handler` VALUES ('4D875DF5-6252-4692-81B8-B426F3E4E0CE', 'DayCheck', 'MAIN', 'index?DayCheck', '03F9F226-31DA-4A98-89CB-00CFAC5D26A4');
INSERT INTO `sys_handler` VALUES ('503FE098-25FC-49D7-AA14-45763C8E9F1D', 'WeekWorkAudit', 'OTHER', 'index?WeekWorkAudit', 'DBB9E261-FCED-4A2D-8AE5-7088C1869E21');
INSERT INTO `sys_handler` VALUES ('50C6517B-B972-459E-AA14-9A24055085C0', 'WeekWorkAudit', 'MAIN', 'index?WeekWorkAudit', '0CE7BBD4-0896-4D76-AB88-C185BF22C66F');
INSERT INTO `sys_handler` VALUES ('537AB930-777B-4A44-BE00-F9942C9C1CC4', 'GroupUserQueryList', 'OTHER', 'index?GroupUserQueryList', '11DCF8E9-3EB8-4C83-A8B9-51C0C530F114');
INSERT INTO `sys_handler` VALUES ('540C3960-15A9-43B8-B4DE-10A5E9D130E8', 'DayExamination', 'OTHER', 'index?DayExamination', '03F9F226-31DA-4A98-89CB-00CFAC5D26A4');
INSERT INTO `sys_handler` VALUES ('5646C255-DC3A-4B31-841E-725700D3F1CD', 'SecurityGroupTreeSelect', 'OTHER', 'index?SecurityGroupTreeSelect', 'DFE8BE4C-4024-4A7B-8DF2-630003832AE9');
INSERT INTO `sys_handler` VALUES ('5AD31E4E-0B7B-42DF-91FA-644AD9FED4D8', 'WmGroupTreeManage', 'MAIN', 'index?WmGroupTreeManage', '11DCF8E9-3EB8-4C83-A8B9-51C0C530F114');
INSERT INTO `sys_handler` VALUES ('5DF667FA-AA5C-4334-8968-B7D54EC72B4D', 'WmDayworkManageList', 'MAIN', 'index?WmDayworkManageList', '9D55885C-7B56-4B04-847F-B8F511761C3A');
INSERT INTO `sys_handler` VALUES ('5EDE2D14-4E50-41AE-A293-B4EE92758823', 'WmDayworkManageSetDisplayCount', 'OTHER', 'index?WmDayworkManageSetDisplayCount', '9D55885C-7B56-4B04-847F-B8F511761C3A');
INSERT INTO `sys_handler` VALUES ('627309FC-47C2-4C14-8258-9FE1ADE99714', 'WmDayworkManageEdit', 'OTHER', 'index?WmDayworkManageEdit', '9D55885C-7B56-4B04-847F-B8F511761C3A');
INSERT INTO `sys_handler` VALUES ('67DE64A8-A810-48FE-AF71-C541FAF7F152', 'WmWeekManageEdit', 'OTHER', 'index?WmWeekManageEdit', '0CE7BBD4-0896-4D76-AB88-C185BF22C66F');
INSERT INTO `sys_handler` VALUES ('6CD02395-B1B0-42FB-BDCB-F2AEC53D2D3C', 'WmDayworkManageQuery', 'OTHER', 'index?WmDayworkManageQuery', '03F9F226-31DA-4A98-89CB-00CFAC5D26A4');
INSERT INTO `sys_handler` VALUES ('729F59DC-3C2D-4E48-B7FC-D84E7E1FEC49', 'WmWeektimeManageEdit', 'OTHER', 'index?WmWeektimeManageEdit', '5E50DF0C-E687-4B0D-85F8-256B8ADBB5A3');
INSERT INTO `sys_handler` VALUES ('77639332-7789-4574-85F6-705CD3C9235A', 'SecurityUserManagerTreePick', 'OTHER', '', 'CC0B472B-0AEB-4812-ABDA-8C7E91434CB7');
INSERT INTO `sys_handler` VALUES ('88882DB9-967F-430E-BA9C-D0BBBBD2BD0C', 'ModifyPassword', 'MAIN', 'index?ModifyPassword', 'D3582A2A-3173-4F92-B1AD-2F999A2CBE18');
INSERT INTO `sys_handler` VALUES ('972E5382-9719-4163-A5BB-828780B71325', 'SecurityUserQueryList', 'OTHER', 'index?SecurityUserQueryList', 'DFE8BE4C-4024-4A7B-8DF2-630003832AE9');
INSERT INTO `sys_handler` VALUES ('9A16D554-F989-438A-B92D-C8C8AC6BF9B8', 'CodeTypeManageList', 'MAIN', null, 'A0334956-426E-4E49-831B-EB00E37285FD');
INSERT INTO `sys_handler` VALUES ('9BCBD67A-020E-49CC-A555-8EF4562607A3', 'SecurityGroupList', 'OTHER', 'index?SecurityGroupList', '8C84B439-2788-4608-89C4-8F5AA076D124');
INSERT INTO `sys_handler` VALUES ('A0550A05-7C49-4FCE-9EBD-26BDA2C1BB41', 'WmDayworkManageQuery', 'OTHER', 'index?WmDayworkManageQuery', '9D55885C-7B56-4B04-847F-B8F511761C3A');
INSERT INTO `sys_handler` VALUES ('A277504E-CF9D-42FE-8F9A-892F99306C6F', 'WmWeektimeManageList', 'MAIN', 'index?WmWeektimeManageList', '5E50DF0C-E687-4B0D-85F8-256B8ADBB5A3');
INSERT INTO `sys_handler` VALUES ('A323C53B-DAD5-40D0-AC3B-18A486E4ACC8', 'GroupTreeSelect', 'OTHER', 'index?GroupTreeSelect', 'DBB9E261-FCED-4A2D-8AE5-7088C1869E21');
INSERT INTO `sys_handler` VALUES ('A59F4491-C990-4BF8-A459-76C90A080349', 'GroupUserTreeSelect', 'OTHER', 'index?GroupUserTreeSelect', '11DCF8E9-3EB8-4C83-A8B9-51C0C530F114');
INSERT INTO `sys_handler` VALUES ('A5CCDE25-4495-4B51-A26B-852CB07B110D', 'SecurityGroupQueryList', 'OTHER', 'index?SecurityGroupQueryList', 'DFE8BE4C-4024-4A7B-8DF2-630003832AE9');
INSERT INTO `sys_handler` VALUES ('AD0D5DF8-8C6E-4749-BC28-6A7518CFB76E', 'WmGroupParentSelect', 'OTHER', 'index?WmGroupParentSelect', '11DCF8E9-3EB8-4C83-A8B9-51C0C530F114');
INSERT INTO `sys_handler` VALUES ('AFD9F60E-EA69-4784-A697-CB0FA0C715AA', 'WeekTimeListSelectList', 'OTHER', 'index?WeekTimeListSelectList', 'DBB9E261-FCED-4A2D-8AE5-7088C1869E21');
INSERT INTO `sys_handler` VALUES ('B4FE5722-9EA6-47D8-8770-D999A3F6A354', 'CodeListManageList', 'MAIN', null, '692B0D37-2E66-4E82-92B4-E59BCF76EE76');
INSERT INTO `sys_handler` VALUES ('B9C499C5-8785-4F51-BE04-1097CE5931E6', 'SecurityGroupPick', 'OTHER', 'index?SecurityGroupPick', '8C84B439-2788-4608-89C4-8F5AA076D124');
INSERT INTO `sys_handler` VALUES ('BB579E25-306F-4EFC-8524-9D1CC0CB689F', 'WeekTimeListSelectList', 'OTHER', 'index?WeekTimeListSelectList', '5E50DF0C-E687-4B0D-85F8-256B8ADBB5A3');
INSERT INTO `sys_handler` VALUES ('C19A8D7C-5DD2-4FC9-B894-1B114354E7D9', 'SecurityGroupTreeSelect', 'OTHER', 'index?SecurityGroupTreeSelect', 'DFE8BE4C-4024-4A7B-8DF2-630003832AE9');
INSERT INTO `sys_handler` VALUES ('EA731710-9F2D-411E-8B8E-BA65A3B61039', 'RelPropertieEdit', 'OTHER', 'index?RelPropertieEdit', '11DCF8E9-3EB8-4C83-A8B9-51C0C530F114');
INSERT INTO `sys_handler` VALUES ('F1924066-441A-43D4-9FA7-F678AE062FA9', 'SecurityUserEdit', 'OTHER', 'index?SecurityUserEdit', '8C84B439-2788-4608-89C4-8F5AA076D124');
INSERT INTO `sys_handler` VALUES ('F5724256-FFB2-409B-B568-2661413B8560', 'WmDayworkManageList', 'OTHER', 'index?WmDayworkManageList', '03F9F226-31DA-4A98-89CB-00CFAC5D26A4');
INSERT INTO `sys_handler` VALUES ('F70D8B75-2AD1-4286-846D-AD8C1D11E3DD', 'WmWeekManageList', 'MAIN', 'index?WmWeekManageList', 'DBB9E261-FCED-4A2D-8AE5-7088C1869E21');
INSERT INTO `sys_handler` VALUES ('F7746D8A-F3AE-4004-8B65-644E6A0CE628', 'SecurityUserList', 'MAIN', null, 'CC0B472B-0AEB-4812-ABDA-8C7E91434CB7');
INSERT INTO `sys_handler` VALUES ('FDF0A2C2-8264-4983-9025-C4BECB24DB1C', 'SysLogQueryDetail', 'MAIN', 'index?SysLogQueryDetail', '67BA273A-DD31-48D0-B78C-1D60D5316074');

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `ID` char(36) DEFAULT NULL,
  `OPER_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `IP_ADDTRESS` varchar(32) DEFAULT NULL,
  `USER_ID` varchar(32) DEFAULT NULL,
  `USER_NAME` varchar(32) DEFAULT NULL,
  `FUNC_NAME` varchar(64) DEFAULT NULL,
  `ACTION_TYPE` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_onlinecount
-- ----------------------------
DROP TABLE IF EXISTS `sys_onlinecount`;
CREATE TABLE `sys_onlinecount` (
  `IPADDRRESS` varchar(64) NOT NULL,
  `ONLINECOUNT` int(11) DEFAULT NULL,
  PRIMARY KEY (`IPADDRRESS`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_onlinecount
-- ----------------------------

-- ----------------------------
-- Table structure for sys_operation
-- ----------------------------
DROP TABLE IF EXISTS `sys_operation`;
CREATE TABLE `sys_operation` (
  `OPER_ID` char(36) NOT NULL,
  `HANLER_ID` varchar(36) DEFAULT NULL,
  `OPER_CODE` varchar(64) DEFAULT NULL,
  `OPER_NAME` varchar(64) DEFAULT NULL,
  `OPER_ACTIONTPYE` varchar(64) DEFAULT NULL,
  `OPER_SORT` int(11) DEFAULT NULL,
  PRIMARY KEY (`OPER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_operation
-- ----------------------------
INSERT INTO `sys_operation` VALUES ('3EC41FB8-ED98-41F3-B86D-F7473AEDD002', '494DF09B-7573-4CCA-85C1-97F4DC58C86B', 'viewDetail', '查看', 'viewDetail', '1');
INSERT INTO `sys_operation` VALUES ('6BF7A157-D333-4F40-8612-F61D3FD4D258', '494DF09B-7573-4CCA-85C1-97F4DC58C86B', 'refreshImgBtn', '刷新', 'refresh', '2');

-- ----------------------------
-- Table structure for wcm_general_group
-- ----------------------------
DROP TABLE IF EXISTS `wcm_general_group`;
CREATE TABLE `wcm_general_group` (
  `GRP_ID` varchar(36) NOT NULL,
  `GRP_NAME` varchar(64) DEFAULT NULL,
  `GRP_PID` varchar(36) DEFAULT NULL,
  `GRP_ORDERNO` int(11) DEFAULT NULL,
  `GRP_IS_SYSTEM` varchar(32) DEFAULT NULL,
  `GRP_RES_TYPE_DESC` varchar(32) DEFAULT NULL,
  `GRP_RES_TYPE_EXTS` varchar(128) DEFAULT NULL,
  `GRP_RES_SIZE_LIMIT` varchar(32) DEFAULT NULL,
  `GRP_DESC` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`GRP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wcm_general_group
-- ----------------------------
INSERT INTO `wcm_general_group` VALUES ('77777777-7777-7777-7777-777777777777', '附件目录', '', null, '', '', '', '', '');
INSERT INTO `wcm_general_group` VALUES ('A6018D88-8345-46EE-A452-CE362FAC72E2', '视频文件', '77777777-7777-7777-7777-777777777777', '1', 'Y', '视频', '*.mp4;*.3gp;*.wmv;*.avi;*.rm;*.rmvb;*.flv', '100MB', '');
INSERT INTO `wcm_general_group` VALUES ('CF35D1E6-102E-428A-B39C-0072D491D5B1', '业务附件', '77777777-7777-7777-7777-777777777777', '2', 'Y', '文档', '*.pdf;*.txt;*.xls;*.doc;*.docx;*.xlsx', '2MB', '');

-- ----------------------------
-- Table structure for wcm_general_resource
-- ----------------------------
DROP TABLE IF EXISTS `wcm_general_resource`;
CREATE TABLE `wcm_general_resource` (
  `RES_ID` varchar(36) NOT NULL,
  `GRP_ID` varchar(36) DEFAULT NULL,
  `RES_NAME` varchar(64) DEFAULT NULL,
  `RES_SHAREABLE` varchar(32) DEFAULT NULL,
  `RES_LOCATION` varchar(256) DEFAULT NULL,
  `RES_SIZE` varchar(64) DEFAULT NULL,
  `RES_SUFFIX` varchar(32) DEFAULT NULL,
  `RES_DESCRIPTION` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`RES_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wcm_general_resource
-- ----------------------------

-- ----------------------------
-- Table structure for wm_daywork
-- ----------------------------
DROP TABLE IF EXISTS `wm_daywork`;
CREATE TABLE `wm_daywork` (
  `TW_ID` char(36) NOT NULL,
  `USER_ID` char(36) DEFAULT NULL,
  `TW_TIME` datetime DEFAULT NULL,
  `TW_ENV` varchar(32) DEFAULT NULL,
  `TW_CONTENT` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`TW_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wm_daywork
-- ----------------------------

-- ----------------------------
-- Table structure for wm_group
-- ----------------------------
DROP TABLE IF EXISTS `wm_group`;
CREATE TABLE `wm_group` (
  `GRP_ID` char(36) NOT NULL,
  `GRP_NAME` char(36) DEFAULT NULL,
  `GRP_TYPE` varchar(32) DEFAULT NULL,
  `GRP_PID` char(36) DEFAULT NULL,
  `GRP_STATE` varchar(32) DEFAULT NULL,
  `GRP_START_TIME` date DEFAULT NULL,
  `GRP_END_TIME` date DEFAULT NULL,
  `GRP_NUMBER` int(11) DEFAULT NULL,
  PRIMARY KEY (`GRP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wm_group
-- ----------------------------
INSERT INTO `wm_group` VALUES ('00000000-0000-0000-00000000000000000', '根节点', '0', null, '1', null, null, null);
INSERT INTO `wm_group` VALUES ('3CEAE7B1-D0AB-4A18-ACF8-77CF9DC60A60', '数通畅联集成项目', '1', '00000000-0000-0000-00000000000000000', '1', '2015-11-26', null, '7');
INSERT INTO `wm_group` VALUES ('7CD4549E-D318-4C54-938A-8C517E2D4326', '数通畅联MAP', '1', '00000000-0000-0000-00000000000000000', '0', '2017-02-28', null, '13');
INSERT INTO `wm_group` VALUES ('AB03893B-78F8-4858-B956-1CEC5895317A', '数通畅联Portal', '1', '00000000-0000-0000-00000000000000000', '1', '2015-09-30', null, '11');
INSERT INTO `wm_group` VALUES ('CAA25C7C-3450-43C6-861A-8BE6F6F6B8F7', '数通畅联DP', '1', '00000000-0000-0000-00000000000000000', '1', '2015-09-30', null, '9');
INSERT INTO `wm_group` VALUES ('CD0D8523-E47A-4B87-948B-64D9F386C44D', '数通畅联ESB', '1', '00000000-0000-0000-00000000000000000', '1', '2015-08-10', null, '10');
INSERT INTO `wm_group` VALUES ('F7BCBA80-6EA3-4A46-A080-3D9B3030A880', '数通畅联BPM', '1', '00000000-0000-0000-00000000000000000', '1', '2014-09-30', null, '12');

-- ----------------------------
-- Table structure for wm_grp_emp_rel
-- ----------------------------
DROP TABLE IF EXISTS `wm_grp_emp_rel`;
CREATE TABLE `wm_grp_emp_rel` (
  `GRP_ID` char(36) NOT NULL,
  `USER_ID` char(36) NOT NULL,
  `EMP_JOB` varchar(32) DEFAULT NULL,
  `EMP_SORT` int(11) DEFAULT NULL,
  PRIMARY KEY (`GRP_ID`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wm_grp_emp_rel
-- ----------------------------
INSERT INTO `wm_grp_emp_rel` VALUES ('3CEAE7B1-D0AB-4A18-ACF8-77CF9DC60A60', '5BCB9801-7C45-4F11-9E71-634D7913A145', 'Participant', '5');
INSERT INTO `wm_grp_emp_rel` VALUES ('3CEAE7B1-D0AB-4A18-ACF8-77CF9DC60A60', '6565BFC1-E424-455F-88EB-AD602D7A19C2', 'Master', '1');
INSERT INTO `wm_grp_emp_rel` VALUES ('3CEAE7B1-D0AB-4A18-ACF8-77CF9DC60A60', '8102F8E4-BDA0-42AE-9A45-2A08DC8C5F7A', 'Participant', '4');
INSERT INTO `wm_grp_emp_rel` VALUES ('3CEAE7B1-D0AB-4A18-ACF8-77CF9DC60A60', '9B84A740-64D6-4CAA-A4F2-D3B285B8969B', 'Participant', '2');
INSERT INTO `wm_grp_emp_rel` VALUES ('3CEAE7B1-D0AB-4A18-ACF8-77CF9DC60A60', 'B4EA9168-CB1E-4069-BAA9-39DE3FECA45D', 'Participant', '3');
INSERT INTO `wm_grp_emp_rel` VALUES ('AB03893B-78F8-4858-B956-1CEC5895317A', '6565BFC1-E424-455F-88EB-AD602D7A19C2', 'Auditer', '4');
INSERT INTO `wm_grp_emp_rel` VALUES ('AB03893B-78F8-4858-B956-1CEC5895317A', 'B4EA9168-CB1E-4069-BAA9-39DE3FECA45D', 'Master', '1');
INSERT INTO `wm_grp_emp_rel` VALUES ('CAA25C7C-3450-43C6-861A-8BE6F6F6B8F7', '5BCB9801-7C45-4F11-9E71-634D7913A145', 'Participant', '6');
INSERT INTO `wm_grp_emp_rel` VALUES ('CAA25C7C-3450-43C6-861A-8BE6F6F6B8F7', '6565BFC1-E424-455F-88EB-AD602D7A19C2', 'Auditer', '2');
INSERT INTO `wm_grp_emp_rel` VALUES ('CAA25C7C-3450-43C6-861A-8BE6F6F6B8F7', '709F595E-BB40-4397-B5D6-AA0BB3359F1F', 'Master', '1');
INSERT INTO `wm_grp_emp_rel` VALUES ('CAA25C7C-3450-43C6-861A-8BE6F6F6B8F7', '8102F8E4-BDA0-42AE-9A45-2A08DC8C5F7A', 'Participant', '4');
INSERT INTO `wm_grp_emp_rel` VALUES ('CAA25C7C-3450-43C6-861A-8BE6F6F6B8F7', '9B84A740-64D6-4CAA-A4F2-D3B285B8969B', 'Participant', '3');
INSERT INTO `wm_grp_emp_rel` VALUES ('CAA25C7C-3450-43C6-861A-8BE6F6F6B8F7', 'B4EA9168-CB1E-4069-BAA9-39DE3FECA45D', 'Participant', '5');
INSERT INTO `wm_grp_emp_rel` VALUES ('CD0D8523-E47A-4B87-948B-64D9F386C44D', '4A1E1BE7-E0B2-46EE-A034-1E7463A60ADD', 'Master', '2');
INSERT INTO `wm_grp_emp_rel` VALUES ('CD0D8523-E47A-4B87-948B-64D9F386C44D', '5BBF2094-63A8-4CBB-9602-17EF1B5E68C8', 'Auditer', '3');
INSERT INTO `wm_grp_emp_rel` VALUES ('CD0D8523-E47A-4B87-948B-64D9F386C44D', '5BCB9801-7C45-4F11-9E71-634D7913A145', 'Participant', '2');
INSERT INTO `wm_grp_emp_rel` VALUES ('CD0D8523-E47A-4B87-948B-64D9F386C44D', '5F76005F-140B-481B-9A52-F197DCE4A04F', 'Participant', '1');
INSERT INTO `wm_grp_emp_rel` VALUES ('CD0D8523-E47A-4B87-948B-64D9F386C44D', '6565BFC1-E424-455F-88EB-AD602D7A19C2', 'Auditer', '1');
INSERT INTO `wm_grp_emp_rel` VALUES ('CD0D8523-E47A-4B87-948B-64D9F386C44D', '9B84A740-64D6-4CAA-A4F2-D3B285B8969B', 'Master', '0');
INSERT INTO `wm_grp_emp_rel` VALUES ('F7BCBA80-6EA3-4A46-A080-3D9B3030A880', '6565BFC1-E424-455F-88EB-AD602D7A19C2', 'Auditer', '2');
INSERT INTO `wm_grp_emp_rel` VALUES ('F7BCBA80-6EA3-4A46-A080-3D9B3030A880', '8102F8E4-BDA0-42AE-9A45-2A08DC8C5F7A', 'Master', '1');

-- ----------------------------
-- Table structure for wm_note
-- ----------------------------
DROP TABLE IF EXISTS `wm_note`;
CREATE TABLE `wm_note` (
  `NOTE_ID` char(36) NOT NULL,
  `USER_ID` char(36) DEFAULT NULL,
  `NOTE_TITLE` varchar(64) DEFAULT NULL,
  `NOTE_DESCRIBE` varchar(1000) DEFAULT NULL,
  `NOTE_SORT` int(11) DEFAULT NULL,
  PRIMARY KEY (`NOTE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wm_note
-- ----------------------------

-- ----------------------------
-- Table structure for wm_weektime
-- ----------------------------
DROP TABLE IF EXISTS `wm_weektime`;
CREATE TABLE `wm_weektime` (
  `WT_ID` char(36) NOT NULL,
  `WT_BEGIN` date DEFAULT NULL,
  `WT_END` date DEFAULT NULL,
  `WT_STAND_DAY` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`WT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wm_weektime
-- ----------------------------
INSERT INTO `wm_weektime` VALUES ('03BA78B5-568B-4B49-A9CE-5957BFB0CB1B', '2015-12-14', '2015-12-18', '5');
INSERT INTO `wm_weektime` VALUES ('1656A5FF-6BAA-4A2C-984E-8A8F2ED6D014', '2015-09-21', '2015-09-25', '5');
INSERT INTO `wm_weektime` VALUES ('53D69480-32F5-4C3B-A7D9-7D364583C19C', '2015-12-07', '2015-12-11', '5');
INSERT INTO `wm_weektime` VALUES ('54370715-9B56-4998-A6F9-47F46FF3A8B1', '2015-11-02', '2015-11-06', '5');
INSERT INTO `wm_weektime` VALUES ('684A1B40-FAF3-40D9-A775-38311462C0E1', '2015-10-26', '2015-10-30', '5');
INSERT INTO `wm_weektime` VALUES ('70333A04-E47F-4AB0-B94C-B69CDF5A9C56', '2015-11-30', '2015-12-04', '5');
INSERT INTO `wm_weektime` VALUES ('812813F9-8267-4989-A0E6-EEB99BCFBEEE', '2017-02-20', '2017-02-24', '5');
INSERT INTO `wm_weektime` VALUES ('8F79ACA4-6897-4E41-A9E2-68CF5E93C5FA', '2017-03-06', '2017-03-10', '5');
INSERT INTO `wm_weektime` VALUES ('98C6AFD5-429F-42F5-97BF-CD1B4CEF6F52', '2017-02-27', '2017-03-03', '5');
INSERT INTO `wm_weektime` VALUES ('9DE15182-77E1-4F38-AE6F-19FF1D197C43', '2015-10-08', '2015-10-10', '3');
INSERT INTO `wm_weektime` VALUES ('9F17D581-B24D-4ED4-90C4-277716681A21', '2015-11-09', '2015-11-13', '5');
INSERT INTO `wm_weektime` VALUES ('AF1071CE-05CF-44C5-9FBC-DC573595C668', '2015-11-16', '2015-11-20', '5');
INSERT INTO `wm_weektime` VALUES ('BF11CF0D-46B4-47FA-8E88-0CFCF80E4D82', '2015-10-12', '2015-10-16', '5');
INSERT INTO `wm_weektime` VALUES ('DD1A6833-E8C0-4CC7-913B-E1AC48B36DFF', '2015-11-23', '2015-11-27', '5');
INSERT INTO `wm_weektime` VALUES ('E7DCF5D0-A6C8-4D9B-BC5D-50BA60081EAE', '2015-10-19', '2015-10-23', '5');
INSERT INTO `wm_weektime` VALUES ('F4257A0F-AEA8-47C5-B76F-1F0DCA5A8046', '2015-09-28', '2015-09-30', '3');

-- ----------------------------
-- Table structure for wm_weekwork
-- ----------------------------
DROP TABLE IF EXISTS `wm_weekwork`;
CREATE TABLE `wm_weekwork` (
  `WW_ID` char(36) NOT NULL,
  `USER_ID` char(36) DEFAULT NULL,
  `WT_ID` char(36) DEFAULT NULL,
  `WW_DAY` varchar(5) DEFAULT NULL,
  `WW_COMPLETION` varchar(32) DEFAULT NULL,
  `WW_STATE` varchar(32) DEFAULT NULL,
  `WW_EXPERIENCE` text,
  PRIMARY KEY (`WW_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wm_weekwork
-- ----------------------------
INSERT INTO `wm_weekwork` VALUES ('3551F1A9-7C61-4DA3-AEE8-ED9274A7F52A', '5BCB9801-7C45-4F11-9E71-634D7913A145', '812813F9-8267-4989-A0E6-EEB99BCFBEEE', '5', '80', 'SUMMARY',null);
INSERT INTO `wm_weekwork` VALUES ('3551F1A9-7C61-4DA3-AEE8-ED9274A7F52C', '9B84A740-64D6-4CAA-A4F2-D3B285B8969B', '812813F9-8267-4989-A0E6-EEB99BCFBEEE', '5', '80', 'SUMMARY',null);
INSERT INTO `wm_weekwork` VALUES ('3551F1A9-7C61-4DA3-AEE8-ED9274A7F52D', '8102F8E4-BDA0-42AE-9A45-2A08DC8C5F7A', '812813F9-8267-4989-A0E6-EEB99BCFBEEE', '5', '80', 'SUMMARY',null);
INSERT INTO `wm_weekwork` VALUES ('3551F1A9-7C61-4DA3-AEE8-ED9274A7F52K', 'B4EA9168-CB1E-4069-BAA9-39DE3FECA45D', '812813F9-8267-4989-A0E6-EEB99BCFBEEE', '5', '80', 'SUMMARY',null);
INSERT INTO `wm_weekwork` VALUES ('5264FAF3-BE84-4FCA-917A-BECDE6F42CD0', '5BCB9801-7C45-4F11-9E71-634D7913A145', '8F79ACA4-6897-4E41-A9E2-68CF5E93C5FA', '5', '0', 'INITIALISE',null);
INSERT INTO `wm_weekwork` VALUES ('5264FAF3-BE84-4FCA-917A-BECDE6F42CD3', '8102F8E4-BDA0-42AE-9A45-2A08DC8C5F7A', '8F79ACA4-6897-4E41-A9E2-68CF5E93C5FA', '5', '0', 'INITIALISE',null);
INSERT INTO `wm_weekwork` VALUES ('5264FAF3-BE84-4FCA-917A-BECDE6F42CD4', '9B84A740-64D6-4CAA-A4F2-D3B285B8969B', '8F79ACA4-6897-4E41-A9E2-68CF5E93C5FA', '5', '0', 'INITIALISE',null);
INSERT INTO `wm_weekwork` VALUES ('5264FAF3-BE84-4FCA-917A-BECDE6F42CDT', 'B4EA9168-CB1E-4069-BAA9-39DE3FECA45D', '8F79ACA4-6897-4E41-A9E2-68CF5E93C5FA', '5', '0', 'INITIALISE',null);
INSERT INTO `wm_weekwork` VALUES ('82DFF02F-DF1F-414B-B2B9-AD2A39BBF0A0', '5BCB9801-7C45-4F11-9E71-634D7913A145', '98C6AFD5-429F-42F5-97BF-CD1B4CEF6F52', '5', '0', 'CONFIRMED',null);
INSERT INTO `wm_weekwork` VALUES ('82DFF02F-DF1F-414B-B2B9-AD2A39BBF0A4', '8102F8E4-BDA0-42AE-9A45-2A08DC8C5F7A', '98C6AFD5-429F-42F5-97BF-CD1B4CEF6F52', '5', '0', 'CONFIRMED',null);
INSERT INTO `wm_weekwork` VALUES ('82DFF02F-DF1F-414B-B2B9-AD2A39BBF0A5', '9B84A740-64D6-4CAA-A4F2-D3B285B8969B', '98C6AFD5-429F-42F5-97BF-CD1B4CEF6F52', '5', '0', 'CONFIRMED',null);
INSERT INTO `wm_weekwork` VALUES ('82DFF02F-DF1F-414B-B2B9-AD2A39BBF0AG', 'B4EA9168-CB1E-4069-BAA9-39DE3FECA45D', '98C6AFD5-429F-42F5-97BF-CD1B4CEF6F52', '5', '0', 'CONFIRMED',null);

-- ----------------------------
-- Table structure for wm_weekwork_entry
-- ----------------------------
DROP TABLE IF EXISTS `wm_weekwork_entry`;
CREATE TABLE `wm_weekwork_entry` (
  `ENTRY_ID` char(36) NOT NULL,
  `WW_ID` char(36) DEFAULT NULL,
  `ENTRY_DESCRIBE` varchar(512) DEFAULT NULL,
  `ENTRY_PLAN` varchar(32) DEFAULT NULL,
  `ENTRY_REALITY` varchar(32) DEFAULT NULL,
  `ENTRY_FINISH` varchar(32) DEFAULT NULL,
  `ENTRY_GROUP` varchar(36) DEFAULT NULL,
  `ENTRY_STATE` varchar(32) DEFAULT NULL,
  `ENTRY_SORT` int(11) DEFAULT NULL,
  PRIMARY KEY (`ENTRY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wm_weekwork_entry
-- ----------------------------
INSERT INTO `wm_weekwork_entry` VALUES ('19BB1343-CCE3-47C2-A0B5-523148ABFEF1', '3551F1A9-7C61-4DA3-AEE8-ED9274A7F52C', '数通畅联集成项目需求文档修改', '1', '2', '1', '3CEAE7B1-D0AB-4A18-ACF8-77CF9DC60A60', '1', '3');
INSERT INTO `wm_weekwork_entry` VALUES ('19BB1343-CCE3-47C2-A0B5-523148ABFEF2', '3551F1A9-7C61-4DA3-AEE8-ED9274A7F52D', '数通畅联集成项目需求文档修改', '1', '2', '1', '3CEAE7B1-D0AB-4A18-ACF8-77CF9DC60A60', '1', '3');
INSERT INTO `wm_weekwork_entry` VALUES ('19BB1343-CCE3-47C2-A0B5-523148ABFEF3', '3551F1A9-7C61-4DA3-AEE8-ED9274A7F52K', '数通畅联集成项目需求文档修改', '1', '2', '1', '3CEAE7B1-D0AB-4A18-ACF8-77CF9DC60A60', '1', '3');
INSERT INTO `wm_weekwork_entry` VALUES ('19BB1343-CCE3-47C2-A0B5-523148ABFEF5', '3551F1A9-7C61-4DA3-AEE8-ED9274A7F52A', '数通畅联集成项目需求文档修改', '1', '2', '1', '3CEAE7B1-D0AB-4A18-ACF8-77CF9DC60A60', '1', '3');
INSERT INTO `wm_weekwork_entry` VALUES ('470467BB-C478-4891-83DA-0305C0223692', '82DFF02F-DF1F-414B-B2B9-AD2A39BBF0A5', 'AEAI-DP运行机制学习', '1', null, null, 'CAA25C7C-3450-43C6-861A-8BE6F6F6B8F7', '1', '3');
INSERT INTO `wm_weekwork_entry` VALUES ('470467BB-C478-4891-83DA-0305C0223693', '82DFF02F-DF1F-414B-B2B9-AD2A39BBF0A4', 'AEAI-DP运行机制学习', '1', null, null, 'CAA25C7C-3450-43C6-861A-8BE6F6F6B8F7', '1', '3');
INSERT INTO `wm_weekwork_entry` VALUES ('470467BB-C478-4891-83DA-0305C0223698', '82DFF02F-DF1F-414B-B2B9-AD2A39BBF0A0', 'AEAI-DP运行机制学习', '1', null, null, 'CAA25C7C-3450-43C6-861A-8BE6F6F6B8F7', '1', '3');
INSERT INTO `wm_weekwork_entry` VALUES ('470467BB-C478-4891-83DA-0305C022369R', '82DFF02F-DF1F-414B-B2B9-AD2A39BBF0AG', 'AEAI-DP运行机制学习', '1', null, null, 'CAA25C7C-3450-43C6-861A-8BE6F6F6B8F7', '1', '3');
INSERT INTO `wm_weekwork_entry` VALUES ('602EE64A-B474-451F-A68C-9066DEBA8B12', '3551F1A9-7C61-4DA3-AEE8-ED9274A7F52D', '数通畅联集成项目需求文档撰写', '2', '2', '0', 'CAA25C7C-3450-43C6-861A-8BE6F6F6B8F7', '1', '2');
INSERT INTO `wm_weekwork_entry` VALUES ('602EE64A-B474-451F-A68C-9066DEBA8B13', '3551F1A9-7C61-4DA3-AEE8-ED9274A7F52K', '数通畅联集成项目需求文档撰写', '2', '2', '0', 'CAA25C7C-3450-43C6-861A-8BE6F6F6B8F7', '1', '2');
INSERT INTO `wm_weekwork_entry` VALUES ('602EE64A-B474-451F-A68C-9066DEBA8B15', '3551F1A9-7C61-4DA3-AEE8-ED9274A7F52C', '数通畅联集成项目需求文档撰写', '2', '2', '0', 'CAA25C7C-3450-43C6-861A-8BE6F6F6B8F7', '1', '2');
INSERT INTO `wm_weekwork_entry` VALUES ('602EE64A-B474-451F-A68C-9066DEBA8B19', '3551F1A9-7C61-4DA3-AEE8-ED9274A7F52A', '数通畅联集成项目需求文档撰写', '2', '2', '0', 'CAA25C7C-3450-43C6-861A-8BE6F6F6B8F7', '1', '2');
INSERT INTO `wm_weekwork_entry` VALUES ('6DFEC580-30FB-44F2-9BCD-CCDB26A76293', '3551F1A9-7C61-4DA3-AEE8-ED9274A7F52D', '数通畅联集成项目需求调研', '2', '2', '0', '3CEAE7B1-D0AB-4A18-ACF8-77CF9DC60A60', '1', '1');
INSERT INTO `wm_weekwork_entry` VALUES ('6DFEC580-30FB-44F2-9BCD-CCDB26A76294', '3551F1A9-7C61-4DA3-AEE8-ED9274A7F52K', '数通畅联集成项目需求调研', '2', '2', '0', '3CEAE7B1-D0AB-4A18-ACF8-77CF9DC60A60', '1', '1');
INSERT INTO `wm_weekwork_entry` VALUES ('6DFEC580-30FB-44F2-9BCD-CCDB26A76295', '3551F1A9-7C61-4DA3-AEE8-ED9274A7F52C', '数通畅联集成项目需求调研', '2', '2', '0', '3CEAE7B1-D0AB-4A18-ACF8-77CF9DC60A60', '1', '1');
INSERT INTO `wm_weekwork_entry` VALUES ('6DFEC580-30FB-44F2-9BCD-CCDB26A7629A', '3551F1A9-7C61-4DA3-AEE8-ED9274A7F52A', '数通畅联集成项目需求调研', '2', '2', '0', '3CEAE7B1-D0AB-4A18-ACF8-77CF9DC60A60', '1', '1');
INSERT INTO `wm_weekwork_entry` VALUES ('A9AC2A2B-DA80-452B-9997-C59A6EC58ED1', '82DFF02F-DF1F-414B-B2B9-AD2A39BBF0A0', 'AEAI-DP产品完善', '1', null, null, 'CAA25C7C-3450-43C6-861A-8BE6F6F6B8F7', '1', '1');
INSERT INTO `wm_weekwork_entry` VALUES ('A9AC2A2B-DA80-452B-9997-C59A6EC58ED2', '82DFF02F-DF1F-414B-B2B9-AD2A39BBF0A4', 'AEAI-DP产品完善', '1', null, null, 'CAA25C7C-3450-43C6-861A-8BE6F6F6B8F7', '1', '1');
INSERT INTO `wm_weekwork_entry` VALUES ('A9AC2A2B-DA80-452B-9997-C59A6EC58ED5', '82DFF02F-DF1F-414B-B2B9-AD2A39BBF0A5', 'AEAI-DP产品完善', '1', null, null, 'CAA25C7C-3450-43C6-861A-8BE6F6F6B8F7', '1', '1');
INSERT INTO `wm_weekwork_entry` VALUES ('A9AC2A2B-DA80-452B-9997-C59A6EC58ED6', '82DFF02F-DF1F-414B-B2B9-AD2A39BBF0AG', 'AEAI-DP产品完善', '1', null, null, 'CAA25C7C-3450-43C6-861A-8BE6F6F6B8F7', '1', '1');
INSERT INTO `wm_weekwork_entry` VALUES ('B6E5CE48-6991-495A-8413-7179F6A35603', '5264FAF3-BE84-4FCA-917A-BECDE6F42CD3', 'ESB样例组件学习', '1', null, null, 'CD0D8523-E47A-4B87-948B-64D9F386C44D', '0', '3');
INSERT INTO `wm_weekwork_entry` VALUES ('B6E5CE48-6991-495A-8413-7179F6A35605', '5264FAF3-BE84-4FCA-917A-BECDE6F42CD4', 'ESB样例组件学习', '1', null, null, 'CD0D8523-E47A-4B87-948B-64D9F386C44D', '0', '3');
INSERT INTO `wm_weekwork_entry` VALUES ('B6E5CE48-6991-495A-8413-7179F6A35606', '5264FAF3-BE84-4FCA-917A-BECDE6F42CDT', 'ESB样例组件学习', '1', null, null, 'CD0D8523-E47A-4B87-948B-64D9F386C44D', '0', '3');
INSERT INTO `wm_weekwork_entry` VALUES ('B6E5CE48-6991-495A-8413-7179F6A3560E', '5264FAF3-BE84-4FCA-917A-BECDE6F42CD0', 'ESB样例组件学习', '1', null, null, 'CD0D8523-E47A-4B87-948B-64D9F386C44D', '0', '3');
INSERT INTO `wm_weekwork_entry` VALUES ('C68EE26E-F507-4A6A-9FAA-052FAD38CC42', '5264FAF3-BE84-4FCA-917A-BECDE6F42CD3', 'ESB产品环境搭建', '1', null, null, 'CD0D8523-E47A-4B87-948B-64D9F386C44D', '0', '1');
INSERT INTO `wm_weekwork_entry` VALUES ('C68EE26E-F507-4A6A-9FAA-052FAD38CC45', '5264FAF3-BE84-4FCA-917A-BECDE6F42CD4', 'ESB产品环境搭建', '1', null, null, 'CD0D8523-E47A-4B87-948B-64D9F386C44D', '0', '1');
INSERT INTO `wm_weekwork_entry` VALUES ('C68EE26E-F507-4A6A-9FAA-052FAD38CC46', '5264FAF3-BE84-4FCA-917A-BECDE6F42CDT', 'ESB产品环境搭建', '1', null, null, 'CD0D8523-E47A-4B87-948B-64D9F386C44D', '0', '1');
INSERT INTO `wm_weekwork_entry` VALUES ('C68EE26E-F507-4A6A-9FAA-052FAD38CC4E', '5264FAF3-BE84-4FCA-917A-BECDE6F42CD0', 'ESB产品环境搭建', '1', null, null, 'CD0D8523-E47A-4B87-948B-64D9F386C44D', '0', '1');
INSERT INTO `wm_weekwork_entry` VALUES ('D0D8926C-302B-4A3B-9D1A-89FA199E65B1', '82DFF02F-DF1F-414B-B2B9-AD2A39BBF0A0', 'AEAI-MAP产品完善', '1', null, null, 'CD0D8523-E47A-4B87-948B-64D9F386C44D', '1', '4');
INSERT INTO `wm_weekwork_entry` VALUES ('D0D8926C-302B-4A3B-9D1A-89FA199E65B3', '82DFF02F-DF1F-414B-B2B9-AD2A39BBF0A4', 'AEAI-MAP产品完善', '1', null, null, 'CD0D8523-E47A-4B87-948B-64D9F386C44D', '1', '4');
INSERT INTO `wm_weekwork_entry` VALUES ('D0D8926C-302B-4A3B-9D1A-89FA199E65B5', '82DFF02F-DF1F-414B-B2B9-AD2A39BBF0A5', 'AEAI-MAP产品完善', '1', null, null, 'CD0D8523-E47A-4B87-948B-64D9F386C44D', '1', '4');
INSERT INTO `wm_weekwork_entry` VALUES ('D0D8926C-302B-4A3B-9D1A-89FA199E65B6', '82DFF02F-DF1F-414B-B2B9-AD2A39BBF0AG', 'AEAI-MAP产品完善', '1', null, null, 'CD0D8523-E47A-4B87-948B-64D9F386C44D', '1', '4');
INSERT INTO `wm_weekwork_entry` VALUES ('DC4350F9-F025-4CFF-A433-6D712CF1AA02', '82DFF02F-DF1F-414B-B2B9-AD2A39BBF0A4', 'AEAI-DP预制样例代码扩充', '1', null, null, 'CAA25C7C-3450-43C6-861A-8BE6F6F6B8F7', '1', '2');
INSERT INTO `wm_weekwork_entry` VALUES ('DC4350F9-F025-4CFF-A433-6D712CF1AA05', '82DFF02F-DF1F-414B-B2B9-AD2A39BBF0A5', 'AEAI-DP预制样例代码扩充', '1', null, null, 'CAA25C7C-3450-43C6-861A-8BE6F6F6B8F7', '1', '2');
INSERT INTO `wm_weekwork_entry` VALUES ('DC4350F9-F025-4CFF-A433-6D712CF1AA06', '82DFF02F-DF1F-414B-B2B9-AD2A39BBF0AG', 'AEAI-DP预制样例代码扩充', '1', null, null, 'CAA25C7C-3450-43C6-861A-8BE6F6F6B8F7', '1', '2');
INSERT INTO `wm_weekwork_entry` VALUES ('DC4350F9-F025-4CFF-A433-6D712CF1AA0E', '82DFF02F-DF1F-414B-B2B9-AD2A39BBF0A0', 'AEAI-DP预制样例代码扩充', '1', null, null, 'CAA25C7C-3450-43C6-861A-8BE6F6F6B8F7', '1', '2');
INSERT INTO `wm_weekwork_entry` VALUES ('E544E728-FEB5-47F1-AD50-F0520D91D8A3', '5264FAF3-BE84-4FCA-917A-BECDE6F42CD3', 'ESB产品知识学习', '1', null, null, 'CD0D8523-E47A-4B87-948B-64D9F386C44D', '0', '2');
INSERT INTO `wm_weekwork_entry` VALUES ('E544E728-FEB5-47F1-AD50-F0520D91D8A5', '5264FAF3-BE84-4FCA-917A-BECDE6F42CD4', 'ESB产品知识学习', '1', null, null, 'CD0D8523-E47A-4B87-948B-64D9F386C44D', '0', '2');
INSERT INTO `wm_weekwork_entry` VALUES ('E544E728-FEB5-47F1-AD50-F0520D91D8A6', '5264FAF3-BE84-4FCA-917A-BECDE6F42CDT', 'ESB产品知识学习', '1', null, null, 'CD0D8523-E47A-4B87-948B-64D9F386C44D', '0', '2');
INSERT INTO `wm_weekwork_entry` VALUES ('E544E728-FEB5-47F1-AD50-F0520D91D8A7', '5264FAF3-BE84-4FCA-917A-BECDE6F42CD0', 'ESB产品知识学习', '1', null, null, 'CD0D8523-E47A-4B87-948B-64D9F386C44D', '0', '2');

-- ----------------------------
-- Table structure for wm_weekwork_prepare
-- ----------------------------
DROP TABLE IF EXISTS `wm_weekwork_prepare`;
CREATE TABLE `wm_weekwork_prepare` (
  `PRE_ID` char(36) NOT NULL,
  `WW_ID` char(36) DEFAULT NULL,
  `PRE_DESCRIBE` varchar(512) DEFAULT NULL,
  `PRE_LOAD` varchar(32) DEFAULT NULL,
  `PRE_SORT` int(11) DEFAULT NULL,
  PRIMARY KEY (`PRE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wm_weekwork_prepare
-- ----------------------------
INSERT INTO `wm_weekwork_prepare` VALUES ('161C10B6-4E02-48F3-942F-ADBD99A69BC5', '5264FAF3-BE84-4FCA-917A-BECDE6F42CD0', 'ESB产品功能学习', '1', '1');
INSERT INTO `wm_weekwork_prepare` VALUES ('267C6C23-AFCA-4043-A9E2-F9D727D36BEB', '82DFF02F-DF1F-414B-B2B9-AD2A39BBF0A0', 'AEAI-MAP预制样例完善', '1', '1');
INSERT INTO `wm_weekwork_prepare` VALUES ('B8C5D2E1-704F-4168-8EFC-E52039B7E3CE', '82DFF02F-DF1F-414B-B2B9-AD2A39BBF0A0', 'AEAI-MAP功能样例学习', '1', '2');
INSERT INTO `wm_weekwork_prepare` VALUES ('DB1905F6-3ACB-41E1-9F11-F6D91EBF965F', '3551F1A9-7C61-4DA3-AEE8-ED9274A7F52A', '产品完善升级', '1', '1');
INSERT INTO `wm_weekwork_prepare` VALUES ('E6F08935-51B8-4858-85CD-6FBA1F3207A2', '3551F1A9-7C61-4DA3-AEE8-ED9274A7F52A', '数通畅联产品知识学习', '1', '2');
