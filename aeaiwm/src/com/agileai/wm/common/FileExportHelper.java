package com.agileai.wm.common;

import java.io.File;
import java.net.URL;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.agileai.hotweb.common.FileStreamHelper;

public class FileExportHelper {
	private FileStreamHelper fileStreamHelper = null;
	
	public FileExportHelper(HttpServletRequest request,HttpServletResponse response){
		this.fileStreamHelper = new FileStreamHelper(request, response);
	}
	
	public String getTemplateDirPath(){
		String result = null;
		try {
			  URL url = Thread.currentThread().getContextClassLoader().getResource("");
			  File classesFolder = new File(url.toURI());
			  result = classesFolder.getParentFile().getParentFile().getAbsolutePath()
					  +File.separatorChar+"templates";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	@SuppressWarnings("rawtypes")
	public void exportFile(String templateDir,String templateFile,String exportFileName,Map modelMap,String charencoding){
		this.fileStreamHelper.exportFile(templateDir,templateFile,exportFileName, modelMap,charencoding);
	}
}
