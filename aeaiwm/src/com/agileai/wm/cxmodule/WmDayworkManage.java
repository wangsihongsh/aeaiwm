package com.agileai.wm.cxmodule;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface WmDayworkManage
        extends StandardService {
	public DataRow getLastDayRecord(String currentUserId);
	public void saveDayWorkRecord(DataParam param); 
	
	public List<DataRow> findDayWorkRecords(DataParam param);
	public List<DataRow> findNoteRecords(DataParam param);
	
	public List<DataRow> getMngGroupRecords(String userId);
	public List<DataRow> queryDayEmpRecords(String grpId);
	
	public DataRow getDayRow(String twtime);
	public DataRow getUserRecord(DataParam param);
	public void updateUserRecord(DataParam param); 
	
	public String getBeforeDayRow(String twtime);
	public String getNextDayRow(String twtime);
	public String retrieveCurrentDayRow(String twtime);
	
	List<DataRow> findPastDayWorkRecords(String userId,String minTime);
	DataRow findCurrentDayworkRecords(String userId,String currentTime);
	List<DataRow> findFllowDayworkRecords(String userId,String maxTime);
	public void createDayWorkRecord(DataParam param); 
	public void delDayWorkRecord(String dailyWorkId);
	public DataRow getWorkDailyInfoRecord(String dailyWorkId);
	public void updateDayWorkRecord(String dailyWorkId,String twenv,String twcontent);
	public DataRow getUserRecord(String userId);
	public List<DataRow> initDayExamGroupInfos(String userId);
	List<DataRow> initUserRecords(String grpId);
	DataRow findActiveUserId(String userCode);
	public List<DataRow> initNoteRecords(String userId);
	List<DataRow> initDayExamInfos(String userId,String startTime);
	public DataRow queryGroupRecord(String grpId);
	
	public void syncUserFavorites(String userId,String wnUserFav);
}
