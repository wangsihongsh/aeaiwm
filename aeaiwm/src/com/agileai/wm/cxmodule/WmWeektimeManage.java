package com.agileai.wm.cxmodule;

import java.util.List;

import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface WmWeektimeManage
        extends StandardService {
	List<DataRow> findWeekTimeRecords();
	public void creteWorkTimeListInfo(String stime,String etime,String standDay);
	DataRow getWeekTimeInfo(String weekTimeId);
	public void updateWeekWorkRecord(String id,String stime,String etime,String standDay);
	public void delWeekTimeRecord(String id);
}
