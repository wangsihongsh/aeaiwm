package com.agileai.wm.module.weektime.exteral;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.ws.BaseRestService;
import com.agileai.util.DateUtil;
import com.agileai.wm.cxmodule.WmWeektimeManage;
import com.agileai.wm.module.weektime.exteral.model.WeekTimeModel;

public class WeekTimeImpl extends BaseRestService implements WeekTime {
	private static String ERROR = "error";
	private static String NEGATIVE = "negative";
	private static String CREATE = "create";
	private static String UPDATE = "update";
	
	@Override
	public String findWeekTimeListInfos() {
		String responseText = BaseHandler.FAIL;
		try {
			WmWeektimeManage wmWeektimeManage = this.lookupService(WmWeektimeManage.class);
			JSONObject jsonObject = new JSONObject();
			List<DataRow> weektimeRecords = wmWeektimeManage.findWeekTimeRecords();
			JSONArray jsonArray = new JSONArray();
			if(weektimeRecords.size() != 0){
				for(int i=0;i<weektimeRecords.size();i++){
					DataRow row = weektimeRecords.get(i);
					JSONObject weektimeJson = new JSONObject();
					weektimeJson.put("num", i+1);
					weektimeJson.put("id", row.stringValue("WT_ID"));
					weektimeJson.put("startTime", row.stringValue("WT_BEGIN"));
					weektimeJson.put("endTime", row.stringValue("WT_END"));
					weektimeJson.put("standDay", row.stringValue("WT_STAND_DAY"));
					jsonArray.put(weektimeJson);
				}
			}else{
				JSONObject weektimeJson = new JSONObject();
				weektimeJson.put("num", 1);
				weektimeJson.put("startTime", "无");
				jsonArray.put(weektimeJson);
			}
			jsonObject.put("weekTime", jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return responseText;
	}

	@Override
	public String getWeekTime(String weekTimeId) {
		String responseText = BaseHandler.FAIL;
		try {
        	if(!weekTimeId.isEmpty()){
        		WmWeektimeManage wmWeektimeManage = this.lookupService(WmWeektimeManage.class);
            	DataRow weekTimeRow = wmWeektimeManage.getWeekTimeInfo(weekTimeId);
            	JSONObject jsonObject = new JSONObject();
            	jsonObject.put("stime", weekTimeRow.stringValue("WT_BEGIN").substring(0, 10));
            	jsonObject.put("etime", weekTimeRow.stringValue("WT_END").substring(0, 10));
            	jsonObject.put("standDay", weekTimeRow.stringValue("WT_STAND_DAY"));
            	responseText = jsonObject.toString();
        	}
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return responseText;
	}

	@Override
	public String delWeekTime(String weekTimeId) {
		String responseText = BaseHandler.FAIL;
		try {
        	if(!weekTimeId.isEmpty()){
        		WmWeektimeManage wmWeektimeManage = this.lookupService(WmWeektimeManage.class);
        		wmWeektimeManage.delWeekTimeRecord(weekTimeId);
            	responseText = BaseHandler.SUCCESS;
        	}
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return responseText;
	}

	@Override
	public String calculateStandDay(String stimeUTC, String etimeUTC) {
		String responseText = BaseHandler.FAIL;
		try {
			SimpleDateFormat sf = new SimpleDateFormat("EEE MMM dd yyyy hh:mm:ss z", Locale.ENGLISH);
			stimeUTC = stimeUTC.substring(0,33);
			stimeUTC = stimeUTC.replaceAll("0800", "08:00");
			Date date = sf.parse(stimeUTC); 
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
    		String stime = sdf.format(date); 
    		
    		SimpleDateFormat endFormat = new SimpleDateFormat("EEE MMM dd yyyy hh:mm:ss z", Locale.ENGLISH);
    		etimeUTC = etimeUTC.substring(0,33);
    		etimeUTC = etimeUTC.replaceAll("0800", "08:00");
    		Date ed = endFormat.parse(etimeUTC);
    		String etime = DateUtil.format(DateUtil.YYMMDDHHMISS_HORIZONTAL, ed);
    		
    		long diff = ed.getTime() - date.getTime();
    		long days = diff / (1000 * 60 * 60 * 24);
    		String standDay = String.valueOf(days+1);
    		
    		JSONObject jsonObject = new JSONObject();
    		jsonObject.put("stime", stime.substring(0, 10));
    		jsonObject.put("etime", etime.substring(0, 10));
    		jsonObject.put("standDay", standDay);
        	responseText = jsonObject.toString();
        	
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return responseText;
	}

	@Override
	public String saveWeekTime(WeekTimeModel model) {
		String responseText = BaseHandler.FAIL;
		String type = model.getType();
		if(CREATE.equals(type)){
			responseText = this.creteWorkTimeListInfo(model);
		}else if(UPDATE.equals(type)){
			responseText = this.updateWeekTimeRecord(model);
		}
		return responseText;
	}
	
	private String creteWorkTimeListInfo(WeekTimeModel model){
		String responseText = BaseHandler.FAIL;
		try {
        	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS Z");
    		String stimeUTC = model.getStime().replace("Z", " UTC");
    		Date sd = format.parse(stimeUTC);
    		String stime = DateUtil.format(DateUtil.YYMMDDHHMISS_HORIZONTAL, sd);
    		
    		SimpleDateFormat endFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS Z");
    		String etimeUTC = model.getEtime().replace("Z", " UTC");
    		Date ed = endFormat.parse(etimeUTC);
    		String etime = DateUtil.format(DateUtil.YYMMDDHHMISS_HORIZONTAL, ed);
    		
    		long diff = ed.getTime() - sd.getTime();
    		long days = diff / (1000 * 60 * 60 * 24);
    		String standDay = String.valueOf(days+1);
    		
    		WmWeektimeManage wmWeektimeManage = this.lookupService(WmWeektimeManage.class);
    		
        	if(days < 0){
        		responseText = NEGATIVE;
        	}else if(days>=0 && days<= 10){
            	wmWeektimeManage.creteWorkTimeListInfo(stime,etime,standDay);
            	responseText = BaseHandler.SUCCESS;
        	}else{
        		responseText = ERROR;
        	}
        	
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return responseText;
	}
	
	private String updateWeekTimeRecord(WeekTimeModel model){
		String responseText = BaseHandler.FAIL;
		try {
        	String id = model.getId();
        	
        	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS Z");
    		String stimeUTC = model.getStime().replace("Z", " UTC");
    		Date sd = format.parse(stimeUTC);
    		String stime = DateUtil.format(DateUtil.YYMMDDHHMISS_HORIZONTAL, sd);
    		
    		SimpleDateFormat endFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS Z");
    		String etimeUTC = model.getEtime().toString().replace("Z", " UTC");
    		Date ed = endFormat.parse(etimeUTC);
    		String etime = DateUtil.format(DateUtil.YYMMDDHHMISS_HORIZONTAL, ed);
    		
    		long diff = ed.getTime() - sd.getTime();
    		long days = diff / (1000 * 60 * 60 * 24);
    		String standDay = String.valueOf(days+1);
    		
        	WmWeektimeManage wmWeektimeManage = this.lookupService(WmWeektimeManage.class);
        	if(days < 0){
        		responseText = NEGATIVE;
        	}else if(days>0 && days<= 10){
        		wmWeektimeManage.updateWeekWorkRecord(id,stime,etime,standDay);
            	responseText = BaseHandler.SUCCESS;
        	}else{
        		responseText = ERROR;
        	}
        	
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return responseText;
	}
	
}
