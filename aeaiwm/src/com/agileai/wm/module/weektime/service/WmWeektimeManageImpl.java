package com.agileai.wm.module.weektime.service;

import java.util.List;

import com.agileai.common.KeyGenerator;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;
import com.agileai.wm.cxmodule.WmWeektimeManage;

public class WmWeektimeManageImpl
        extends StandardServiceImpl
        implements WmWeektimeManage {
    public WmWeektimeManageImpl() {
        super();
    }

	@Override
	public List<DataRow> findWeekTimeRecords() {
		DataParam param = new DataParam();
		String statementId = sqlNameSpace+"."+"mobilefindRecords";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}

	@Override
	public void creteWorkTimeListInfo(String stime, String etime,String standDay) {
		DataParam param = new DataParam("WT_BEGIN",stime,"WT_END",etime,"WT_STAND_DAY",standDay);
		param.put("WT_ID", KeyGenerator.instance().genKey());
		String statementId = sqlNameSpace+"."+"insertRecord";
		this.daoHelper.insertRecord(statementId, param);
	}

	@Override
	public DataRow getWeekTimeInfo(String weekTimeId) {
		DataParam param = new DataParam("WT_ID",weekTimeId);
		String statementId = sqlNameSpace+"."+"getRecord";
		DataRow result = this.daoHelper.getRecord(statementId, param);
		return result;
	}

	@Override
	public void updateWeekWorkRecord(String id, String stime, String etime,String standDay) {
		DataParam param = new DataParam("WT_ID",id,"WT_BEGIN",stime,"WT_END",etime,"WT_STAND_DAY",standDay);
		String statementId = sqlNameSpace+"."+"updateRecord";
		this.daoHelper.updateRecord(statementId, param);
	}

	@Override
	public void delWeekTimeRecord(String id) {
		DataParam param = new DataParam("WT_ID",id);
		String statementId = sqlNameSpace+"."+"deleteRecord";
		this.daoHelper.deleteRecords(statementId, param);
	}
}
