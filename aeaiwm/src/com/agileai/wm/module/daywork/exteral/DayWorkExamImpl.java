package com.agileai.wm.module.daywork.exteral;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.ws.BaseRestService;
import com.agileai.util.DateUtil;
import com.agileai.util.StringUtil;
import com.agileai.wm.cxmodule.WmDayworkManage;

public class DayWorkExamImpl extends BaseRestService implements DayWorkExam {
	private String UNDEFINED = "undefined";
	
	@Override
	public String retrieveDayExamUserInfos(String grpId) {
		String responseText = BaseHandler.FAIL;
		try {
			JSONObject jsonObject = new JSONObject();
				User user =  (User)this.getUser();
				if(UNDEFINED.equals(grpId)){
					String userFavor = (String)user.getProperties().get("WM_USER_FAVORITES");
					if(!StringUtil.isNullOrEmpty(userFavor)){
						JSONObject userFavJson = new JSONObject(userFavor);
						if(!userFavJson.isNull("dayProjectId")){
							grpId = userFavJson.getString("dayProjectId");
						}else{
							WmDayworkManage wmDayworkManage = this.lookupService(WmDayworkManage.class);
							List<DataRow> projectRecords = wmDayworkManage.getMngGroupRecords(user.getUserId());
							if(projectRecords.size() > 0){
								grpId = projectRecords.get(0).getString("GRP_ID");
							}
						}
					}else{
						WmDayworkManage wmDayworkManage = this.lookupService(WmDayworkManage.class);
						List<DataRow> projectRecords = wmDayworkManage.getMngGroupRecords(user.getUserId());
						if(projectRecords.size() > 0){
							grpId = projectRecords.get(0).getString("GRP_ID");
						}
					}
				}

				WmDayworkManage wmDayworkManage = this.lookupService(WmDayworkManage.class);
				List<DataRow> userRecords = wmDayworkManage.initUserRecords(grpId);
				JSONArray userInfoArray = new JSONArray();
				JSONArray userCodeArray = new JSONArray();
				if(userRecords.size() > 0 ){
					for(int i=0;i<userRecords.size();i++){
						DataRow row = userRecords.get(i);
						JSONObject userInfoJson = new JSONObject();
						userInfoJson.put("userId", row.stringValue("USER_ID"));
						userInfoJson.put("userName",row.stringValue("USER_NAME"));
						userInfoJson.put("userCode", row.stringValue("USER_CODE"));
						userInfoArray.put(userInfoJson);
						userCodeArray.put(row.stringValue("USER_CODE"));
					}
				}else{
					JSONObject userInfoJson = new JSONObject();
					userInfoJson.put("userName","无记录");
					userInfoArray.put(userInfoJson);
					userCodeArray.put("无记录");
				}
				jsonObject.put("userInfos", userInfoArray);
				jsonObject.put("userCodes", userCodeArray);
				jsonObject.put("grpId", grpId);

			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}

	@Override
	public String findDayWorkInfos(String userId,String grpId) {
		String responseText = BaseHandler.FAIL;
		try {
			String startTime = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,new Date());
			
			WmDayworkManage wmDayworkManage = this.lookupService(WmDayworkManage.class);
			JSONObject jsonObject = new JSONObject();
			List<DataRow> records = wmDayworkManage.initDayExamInfos(userId,startTime);
			String grpName = "无";
			if(!UNDEFINED.equals(grpId)){
				DataRow grpRow = wmDayworkManage.queryGroupRecord(grpId);
				grpName = grpRow.stringValue("GRP_NAME");
			}
			
			JSONArray dayworkArray = new JSONArray();
			if(records.size() >0){
				for(int i=0;i<records.size();i++){
					DataRow row = records.get(i);
					String weekText = DateUtil.getWeekText(row.getTimestamp("TW_TIME"));
					JSONObject dayworkJson = new JSONObject();
					String time = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, row.getTimestamp("TW_TIME"));
					
					time = time.substring(5, 10);
					dayworkJson.put("time", time);
					dayworkJson.put("weekText", weekText);
					dayworkJson.put("id", row.stringValue("TW_ID"));
					dayworkJson.put("content", row.stringValue("TW_CONTENT"));
					dayworkJson.put("env", row.stringValue("TW_ENV_NAME"));
					dayworkArray.put(dayworkJson);
				}
			}
			
			List<DataRow> noteRecords = wmDayworkManage.initNoteRecords(userId);
			JSONArray noteArray = new JSONArray();
			if(noteRecords.size() != 0){
				int size = 5;
				if(noteRecords.size() < 5){
					size = noteRecords.size();
				}
				for(int i=0;i<size;i++){
					DataRow row = noteRecords.get(i);
					JSONObject noteJson = new JSONObject();
					noteJson.put("title", row.stringValue("NOTE_TITLE"));
					noteJson.put("content", row.stringValue("NOTE_DESCRIBE"));
					noteJson.put("id", row.stringValue("NOTE_ID"));
					noteArray.put(noteJson);
				}
			}
			if(records.size() == 0 && noteRecords.size() == 0){
				JSONObject noteJson = new JSONObject();
				noteJson.put("content", "无记录");
				noteArray.put(noteJson);
			}
			
			
			jsonObject.put("notes", noteArray);
			
			jsonObject.put("grpName", grpName);
			jsonObject.put("startTime", startTime);
			jsonObject.put("dayworks", dayworkArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}

	@Override
	public String findProDayWorkListInfos(String userId, String startTime,String grpId) {
		String responseText = BaseHandler.FAIL;
		try {
			if("null".equals(startTime) || UNDEFINED.equals(startTime)){
				startTime = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(new Date(), DateUtil.DAY, -1));
			}else{
				startTime = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(DateUtil.getDateTime(startTime), DateUtil.DAY, -1));
			}
			
			WmDayworkManage wmDayworkManage = this.lookupService(WmDayworkManage.class);
			JSONObject jsonObject = new JSONObject();
			List<DataRow> records = wmDayworkManage.initDayExamInfos(userId,startTime);
			String grpName = "无";
			if(!UNDEFINED.equals(grpId)){
				DataRow grpRow = wmDayworkManage.queryGroupRecord(grpId);
				grpName = grpRow.stringValue("GRP_NAME");
			}
			
			JSONArray dayworkArray = new JSONArray();
			if(records.size() >0){
				for(int i=0;i<records.size();i++){
					DataRow row = records.get(i);
					String weekText = DateUtil.getWeekText(row.getTimestamp("TW_TIME"));
					JSONObject dayworkJson = new JSONObject();
					String time = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, row.getTimestamp("TW_TIME"));
					
					time = time.substring(5, 10);
					dayworkJson.put("time", time);
					dayworkJson.put("weekText", weekText);
					dayworkJson.put("id", row.stringValue("TW_ID"));
					dayworkJson.put("content", row.stringValue("TW_CONTENT"));
					dayworkJson.put("env", row.stringValue("TW_ENV_NAME"));
					dayworkArray.put(dayworkJson);
				}
			}
			
			List<DataRow> noteRecords = wmDayworkManage.initNoteRecords(userId);
			JSONArray noteArray = new JSONArray();
			if(noteRecords.size() != 0){
				int size = 5;
				if(noteRecords.size() < 5){
					size = noteRecords.size();
				}
				for(int i=0;i<size;i++){
					DataRow row = noteRecords.get(i);
					JSONObject noteJson = new JSONObject();
					noteJson.put("title", row.stringValue("NOTE_TITLE"));
					noteJson.put("content", row.stringValue("NOTE_DESCRIBE"));
					noteJson.put("id", row.stringValue("NOTE_ID"));
					noteArray.put(noteJson);
				}
			}
			if(records.size() == 0 && noteRecords.size() == 0){
				JSONObject noteJson = new JSONObject();
				noteJson.put("content", "无记录");
				noteArray.put(noteJson);
			}
			
			jsonObject.put("notes", noteArray);
			
			jsonObject.put("grpName", grpName);
			jsonObject.put("startTime", startTime);
			jsonObject.put("dayworks", dayworkArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}

	@Override
	public String findLastDayWorkListInfos(String userId, String startTime,String grpId) {
		String responseText = BaseHandler.FAIL;
		try {
			if("null".equals(startTime) || UNDEFINED.equals(startTime)){
				startTime = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(new Date(), DateUtil.DAY, -1));
			}else{
				startTime = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(DateUtil.getDateTime(startTime), DateUtil.DAY, 1));
			}
			
			WmDayworkManage wmDayworkManage = this.lookupService(WmDayworkManage.class);
			JSONObject jsonObject = new JSONObject();
			List<DataRow> records = wmDayworkManage.initDayExamInfos(userId,startTime);
			String grpName = "无";
			if(!UNDEFINED.equals(grpId)){
				DataRow grpRow = wmDayworkManage.queryGroupRecord(grpId);
				grpName = grpRow.stringValue("GRP_NAME");
			}
			JSONArray dayworkArray = new JSONArray();
			if(records.size() >0){
				for(int i=0;i<records.size();i++){
					DataRow row = records.get(i);
					String weekText = DateUtil.getWeekText(row.getTimestamp("TW_TIME"));
					JSONObject dayworkJson = new JSONObject();
					String time = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, row.getTimestamp("TW_TIME"));
					
					time = time.substring(5, 10);
					dayworkJson.put("time", time);
					dayworkJson.put("weekText", weekText);
					dayworkJson.put("id", row.stringValue("TW_ID"));
					dayworkJson.put("content", row.stringValue("TW_CONTENT"));
					dayworkJson.put("env", row.stringValue("TW_ENV_NAME"));
					dayworkArray.put(dayworkJson);
				}
			}
			
			List<DataRow> noteRecords = wmDayworkManage.initNoteRecords(userId);
			JSONArray noteArray = new JSONArray();
			if(noteRecords.size() != 0){
				int size = 5;
				if(noteRecords.size() < 5){
					size = noteRecords.size();
				}
				for(int i=0;i<size;i++){
					DataRow row = noteRecords.get(i);
					JSONObject noteJson = new JSONObject();
					noteJson.put("title", row.stringValue("NOTE_TITLE"));
					noteJson.put("content", row.stringValue("NOTE_DESCRIBE"));
					noteJson.put("id", row.stringValue("NOTE_ID"));
					noteArray.put(noteJson);
				}
			}
			if(records.size() == 0 && noteRecords.size() == 0){
				JSONObject noteJson = new JSONObject();
				noteJson.put("content", "无记录");
				noteArray.put(noteJson);
			}
			
			jsonObject.put("notes", noteArray);
			
			jsonObject.put("grpName", grpName);
			jsonObject.put("startTime", startTime);
			jsonObject.put("dayworks", dayworkArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}

	@Override
	public String findProjectInfos() {
		String responseText = BaseHandler.FAIL;
		try {
			User user = (User)this.getUser();
			String userId = user.getUserId();
			String wmUserFavorites = (String) user.getProperties().get("WM_USER_FAVORITES");
			String wmUserProjectId = "";
			if(!StringUtil.isNullOrEmpty(wmUserFavorites)){
				JSONObject wmUserFavJson = new JSONObject(wmUserFavorites);
				if(!wmUserFavJson.isNull("dayProjectId")){
					wmUserProjectId = wmUserFavJson.getString("dayProjectId");
				}
			}
			
			WmDayworkManage wmDayworkManage = this.lookupService(WmDayworkManage.class);
			List<DataRow> projectRecords = wmDayworkManage.getMngGroupRecords(userId);
			List<String> projectGrps = new ArrayList<String>();
			for(int i=0;i<projectRecords.size();i++){
				String id = projectRecords.get(i).getString("GRP_ID");
				projectGrps.add(id);
			}
	
			if(!(projectGrps.contains(wmUserProjectId)) && projectRecords.size() > 0){
				wmUserProjectId =  projectRecords.get(0).getString("GRP_ID");
			}
			
			JSONObject jsonObject = new JSONObject();
			JSONArray projectArray = new JSONArray();
			if(projectRecords.size() > 0){
				for(int i=0;i<projectRecords.size();i++){
					DataRow row = projectRecords.get(i);
					JSONObject projectJson = new JSONObject();
					projectJson.put("projectId", row.stringValue("GRP_ID"));
					projectJson.put("projectName",row.stringValue("GRP_NAME"));
					projectArray.put(projectJson);
				}
			}else{
				JSONObject projectJson = new JSONObject();
				projectJson.put("projectName","无记录");
				projectArray.put(projectJson);
			}
			
			jsonObject.put("projectInfos", projectArray);
			jsonObject.put("projectId", wmUserProjectId);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}

	@Override
	public String getActiveUserId(String userCode) {
		String responseText = BaseHandler.FAIL;
		try {
			WmDayworkManage wmDayworkManage = this.lookupService(WmDayworkManage.class);
			DataRow userInfo = wmDayworkManage.findActiveUserId(userCode);
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("userId", userInfo.stringValue("USER_ID"));
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}

	@Override
	public String retrieveCurSeleDayInfos(String userId, String startTime, String grpId) {
		String responseText = BaseHandler.FAIL;
		try {
			if(UNDEFINED.equals(startTime)){
				startTime = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(new Date(), DateUtil.DAY, -1));
			}
			
			WmDayworkManage wmDayworkManage = this.lookupService(WmDayworkManage.class);
			JSONObject jsonObject = new JSONObject();
			List<DataRow> records = wmDayworkManage.initDayExamInfos(userId,startTime);
			DataRow grpRow = wmDayworkManage.queryGroupRecord(grpId);
			String grpName = grpRow.stringValue("GRP_NAME");
			JSONArray dayworksArray = new JSONArray();
			if(records.size() >0){
				for(int i=0;i<records.size();i++){
					DataRow row = records.get(i);
					String weekText = DateUtil.getWeekText(row.getTimestamp("TW_TIME"));
					JSONObject dayworkjson = new JSONObject();
					String time = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, row.getTimestamp("TW_TIME"));
					
					time = time.substring(5, 10);
					dayworkjson.put("time", time);
					dayworkjson.put("weekText", weekText);
					dayworkjson.put("id", row.stringValue("TW_ID"));
					dayworkjson.put("content", row.stringValue("TW_CONTENT"));
					dayworkjson.put("env", row.stringValue("TW_ENV_NAME"));
					dayworksArray.put(dayworkjson);
				}
			}
			
			List<DataRow> noteRecords = wmDayworkManage.initNoteRecords(userId);
			JSONArray noteJsonArray = new JSONArray();
			if(noteRecords.size() != 0){
				int size = 5;
				if(noteRecords.size() < 5){
					size = noteRecords.size();
				}
				for(int i=0;i<size;i++){
					DataRow row = noteRecords.get(i);
					JSONObject noteJson = new JSONObject();
					noteJson.put("title", row.stringValue("NOTE_TITLE"));
					noteJson.put("content", row.stringValue("NOTE_DESCRIBE"));
					noteJson.put("id", row.stringValue("NOTE_ID"));
					noteJsonArray.put(noteJson);
				}
			}
			
			if(records.size() == 0 && noteRecords.size() == 0){
				JSONObject noteJson = new JSONObject();
				noteJson.put("content", "无记录");
				noteJsonArray.put(noteJson);
			}
			
			jsonObject.put("grpName", grpName);
			jsonObject.put("startTime", startTime);
			jsonObject.put("dayworks", dayworksArray);
			jsonObject.put("notes", noteJsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}

	@Override
	public String syncUserProjectId(String grpId) {
		String responseText = BaseHandler.FAIL;
		try {
			User user = (User)this.getUser();
			String userId = user.getUserId();
			String wnUserFav = (String) user.getProperties().get("WM_USER_FAVORITES");
			JSONObject jsonObject = new JSONObject();
			if(!StringUtil.isNullOrEmpty(wnUserFav)){
				jsonObject = new JSONObject(wnUserFav);
			}
			jsonObject.put("dayProjectId", grpId);
			wnUserFav = jsonObject.toString();
			WmDayworkManage wmDayworkManage = this.lookupService(WmDayworkManage.class);
			wmDayworkManage.syncUserFavorites(userId,wnUserFav);
			user.getProperties().put("WM_USER_FAVORITES",wnUserFav);
			wnUserFav = (String) user.getProperties().get("WM_USER_FAVORITES");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		responseText = BaseHandler.SUCCESS;
		return responseText;
	}

}
