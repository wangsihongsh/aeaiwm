package com.agileai.wm.module.daywork.handler;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class WmDayworkManageQueryHandler
        extends StandardEditHandler {
    public WmDayworkManageQueryHandler() {
        super();
    }
    public ViewRenderer prepareDisplay(DataParam param) {
    	String startTime = (String) this.getSessionAttribute("startTime");
    	String endTime = (String) this.getSessionAttribute("endTime");
    	String content = (String) this.getSessionAttribute("content");
    	if("".equals(startTime) || startTime==null){
    		setAttribute("START_TIME", getStartTime());
    	}else{
    		setAttribute("START_TIME", startTime);
    	}
    	
    	if("".equals(endTime) || endTime==null){
    		setAttribute("END_TIME", getEndTime());
    	}else{
    		setAttribute("END_TIME", endTime);
    	}
    	setAttribute("CONTENT", content);
		processPageAttributes(param);
		this.setOperaType(OperaType.UPDATE);
		return new LocalRenderer(getPage());
	}
	private  String  getStartTime(){
		Calendar c = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	    c.set(c.get(Calendar.YEAR)-1,0,1);
	    Date date=c.getTime();
		return sdf.format(date);
	}
	private String getEndTime(){
		Calendar c = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	    c.set(c.get(Calendar.YEAR),c.get(Calendar.MONTH),c.get(Calendar.DAY_OF_MONTH)+3);
	    Date end = c.getTime();
		return sdf.format(end);
	}
	
	@PageAction
	 public ViewRenderer conditionReset(DataParam param){
		String responseText = FAIL;
		try {
	    	this.removeSessionAttribute("startTime");
	    	this.removeSessionAttribute("endTime");
	    	this.removeSessionAttribute("content");
	    	responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
}
