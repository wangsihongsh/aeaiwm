package com.agileai.wm.module.daywork.service;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;

public class WmNoteManageImpl
        extends StandardServiceImpl
        implements WmNoteManage {
	protected String idField = "NOTE_ID";
	protected String nameField = "NOTE_TITLE";
	protected String sortField = "NOTE_SORT";	
    public WmNoteManageImpl() {
        super();
    }
    
	public void createRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"insertRecord";
		processDataType(param, tableName);
		processPrimaryKeys(param);
		String userId = param.get("USER_ID");
		int maxSort = this.getMaxSort(userId);
		param.put("NOTE_SORT",maxSort);
		this.daoHelper.insertRecord(statementId, param);
	}
	
	private int getMaxSort(String userId){
		int result = 0;
		DataParam param = new DataParam();
		String statementId = sqlNameSpace+"."+"getMaxSort";
		DataRow row = this.daoHelper.getRecord(statementId, param);
		if (row != null && row.size() > 0){
			int maxSort = row.getInt("MAX_SORT");
			result = maxSort+1;
		}
		return result;
	}

	@Override
	public void changeCurrentSort(String currentId, boolean isUp) {
		String statementId = this.sqlNameSpace+"."+"queryCurLevelRecords";
		List<DataRow> records = this.daoHelper.queryRecords(statementId, currentId);
		DataRow curRow = null;
		String curSort = null;
		if (isUp){
			DataRow beforeRow = null;
			for (int i=0;i < records.size();i++){
				DataRow row = records.get(i);
				String tempMenuId = row.stringValue(idField);
				if (currentId.equals(tempMenuId)){
					curRow = row;
					beforeRow = records.get(i-1);
					break;
				}
			}
			curSort = curRow.stringValue(sortField);
			String beforeSort = beforeRow.stringValue(sortField);
			curRow.put(sortField,beforeSort);
			beforeRow.put(sortField,curSort);
			this.updateCurrentRecord(curRow.toDataParam());
			this.updateCurrentRecord(beforeRow.toDataParam());
		}else{
			DataRow nextRow = null;
			for (int i=0;i < records.size();i++){
				DataRow row = records.get(i);
				String tempMenuId = row.stringValue(idField);
				if (currentId.equals(tempMenuId)){
					curRow = row;
					nextRow = records.get(i+1);
					break;
				}
			}
			curSort = curRow.stringValue(sortField);
			String nextSort = nextRow.stringValue(sortField);
			curRow.put(sortField,nextSort);
			nextRow.put(sortField,curSort);
			this.updateCurrentRecord(curRow.toDataParam());
			this.updateCurrentRecord(nextRow.toDataParam());
		}
	}
	public DataRow queryCurrentRecord(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"queryTreeRecord";
		return this.daoHelper.getRecord(statementId, param);
	}
	public void updateCurrentRecord(DataParam param) {
		String currentId = param.get(idField);
		DataParam queryParam = new DataParam(idField,currentId);
		DataRow row = this.queryCurrentRecord(queryParam);
		DataParam newParam = row.toDataParam(true);
		newParam.update(param);
		String statementId = this.sqlNameSpace+"."+"updateRecord";
		this.daoHelper.updateRecord(statementId, newParam);
	}	
}
