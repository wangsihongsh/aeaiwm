package com.agileai.wm.module.workmanage.service;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.BaseInterface;

public interface WeekWorkAuditManage
        extends BaseInterface {
	
	List<DataRow> getGroupRecords(String userId);
	List<DataRow> queryEmpRecords(String grpId);
	List<DataRow> getEmpJobRecords(String userId,String groupId);
	DataRow queryWeekRecord(String userId,String weekId);
	List<DataRow> queryEntryRecords(String userId,String weekId);
	List<DataRow> queryPrepareRecords(String userId,String weekId);
	
	DataRow retrieveCurrentWeekRow();
	DataRow getWeekRow(String weekId);
	
	DataRow getBeforeWeekRow(String weekId);
	DataRow getNextWeekRow(String weekId);
	DataRow getGroupName(DataParam param);
	List<DataRow> getUserList(DataParam param);
	List<DataRow> getWeekList(DataParam param);
	List<DataRow> getWeekPrepareList(DataParam param);
	List<DataRow> getUserGroupRecords(String userId);
	
	
}
