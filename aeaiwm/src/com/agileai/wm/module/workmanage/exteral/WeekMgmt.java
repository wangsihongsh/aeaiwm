package com.agileai.wm.module.workmanage.exteral;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/rest")
public interface WeekMgmt {
	
    @GET
    @Path("/get-weekwork-info")  
    @Produces(MediaType.APPLICATION_JSON)
	public String getWeekWorkInfo(); 
    
    @GET
    @Path("/find-thisweekwork-infos/{grpId}/{userId}")  
    @Produces(MediaType.APPLICATION_JSON)
	public String findThisWeekWorkInfos(@PathParam("grpId") String grpId,@PathParam("userId") String userId); 
    
    @GET
    @Path("/find-proweeklist-infos/{weekTimeId}/{grpId}/{userId}")  
    @Produces(MediaType.APPLICATION_JSON)
	public String findProWeekWorkListInfos(@PathParam("weekTimeId") String weekTimeId,@PathParam("grpId") String grpId,@PathParam("userId") String userId); 
    
    @GET
    @Path("/find-lastweekworklist-infos/{weekTimeId}/{grpId}/{userId}")  
    @Produces(MediaType.APPLICATION_JSON)
	public String findLastWeekWorkListInfos(@PathParam("weekTimeId") String weekTimeId,@PathParam("grpId") String grpId,@PathParam("userId") String userId); 
    
}
