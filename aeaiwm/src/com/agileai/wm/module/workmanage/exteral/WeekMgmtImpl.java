package com.agileai.wm.module.workmanage.exteral;

import java.util.Date;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.ws.BaseRestService;
import com.agileai.util.DateUtil;
import com.agileai.wm.cxmodule.WmWeekManage;

public class WeekMgmtImpl extends BaseRestService implements WeekMgmt {
	private String UNDEFINED = "undefined";
	private static String ERROR = "error";

	@Override
	public String getWeekWorkInfo() {
		String responseText = BaseHandler.FAIL;
		try {
			User user = (User)this.getUser();
			String userId = user.getUserId();
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			String currentDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,new Date());
			DataRow row1 = wmWeekManage.getCurrentWeek(currentDate);
			String currentWeekId = "";
			if(row1 != null && row1.size() > 0){
				currentWeekId = row1.getString("WT_ID");
			}else{
				row1 = wmWeekManage.getCurrentWeekTimeId(currentDate);
				currentWeekId = row1.getString("WT_ID");
			}
			
			JSONObject jsonObject = new JSONObject();
			List<DataRow> currentWeekRecords = wmWeekManage.getWeekWorkRecord(userId,currentWeekId);
			JSONArray currentWeekArray = new JSONArray();
			if(currentWeekRecords.size() != 0){
				int size = 5;
				if(currentWeekRecords.size() < 5){
					size = currentWeekRecords.size(); 
				}
				for(int i=0;i<size;i++){
					DataRow row = currentWeekRecords.get(i);
					JSONObject currentWeekJson = new JSONObject();
					currentWeekJson.put("describe", row.stringValue("ENTRY_DESCRIBE"));
					currentWeekJson.put("planday", row.stringValue("ENTRY_PLAN"));
					currentWeekJson.put("num", i+1);
					currentWeekArray.put(currentWeekJson);
				}
			}else{
				JSONObject currentWeekJson = new JSONObject();
				currentWeekJson.put("num", 1);
				currentWeekJson.put("describe", "无记录");
				currentWeekJson.put("planday", "0");
				currentWeekArray.put(currentWeekJson);
			}
			
			jsonObject.put("currentWeek", currentWeekArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}
	
	@Override
	public String findThisWeekWorkInfos(String grpId,String userId) {
		String responseText = BaseHandler.FAIL;
		try {
			User user = (User)this.getUser();
			if(UNDEFINED.equals(userId)){
				userId = user.getUserId();
			}
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			String grpName = "";
			if(!UNDEFINED.equals(grpId)){
				DataRow grpRow = wmWeekManage.queryGroupRecord(grpId);
				grpName = grpRow.stringValue("GRP_NAME");
			}
			
			String currentDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,new Date());
			DataRow row1 = wmWeekManage.getCurrentWeek(currentDate);
			String currentWeekId = "";
			String startTime = "";
			String endTime = "";
			if(row1 != null && row1.size() > 0){
				currentWeekId = row1.stringValue("WT_ID");
				startTime = row1.stringValue("WT_BEGIN");
				endTime = row1.stringValue("WT_END");
			}else{
				row1 = wmWeekManage.getCurrentWeekTimeId(currentDate);
				currentWeekId = row1.stringValue("WT_ID");
				startTime = row1.stringValue("WT_BEGIN");
				endTime = row1.stringValue("WT_END");
			}
			DataRow weekManageRow = wmWeekManage.getMasterWeekWorkRecord(userId,currentWeekId);
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("startTime", startTime);
			jsonObject.put("time", startTime);
			jsonObject.put("endTime", endTime);
			jsonObject.put("weekTimeId", currentWeekId);
			
			jsonObject.put("grpName", grpName);
			if(weekManageRow != null){
				jsonObject.put("wwDay", weekManageRow.stringValue("WW_DAY"));
				jsonObject.put("weekWorkId", weekManageRow.stringValue("WW_ID"));
				jsonObject.put("state", weekManageRow.stringValue("WW_STATE"));
				jsonObject.put("stateName", weekManageRow.stringValue("CODE_NAME"));
				String completion =  weekManageRow.stringValue("WW_COMPLETION");
				if("0".equals(completion)){
					jsonObject.put("completion","");
				}else{
					jsonObject.put("completion",completion);
				}
			}else{
				jsonObject.put("wwDay", "0");
				jsonObject.put("weekWorkId","无记录");
				jsonObject.put("stateName", "空");
			}
			
			List<DataRow> weekWorkRecords = wmWeekManage.getWeekWorkRecord(userId,currentWeekId);
			JSONArray weekWorkArray = new JSONArray();
			if(weekWorkRecords.size() != 0){
				for(int i=0;i<weekWorkRecords.size();i++){
					DataRow row = weekWorkRecords.get(i);
					JSONObject weekWorkJson = new JSONObject();
					weekWorkJson.put("describe", row.stringValue("ENTRY_DESCRIBE"));
					weekWorkJson.put("planday", row.stringValue("ENTRY_PLAN"));
					weekWorkArray.put(weekWorkJson);
				}
			}else{
				JSONObject weekWorkJson = new JSONObject();
				weekWorkJson.put("describe", "无记录");
				weekWorkJson.put("planday", "0");
				weekWorkArray.put(weekWorkJson);
			}
			
			List<DataRow> prepareWeekRecords = wmWeekManage.getWeekPrepareRecord(userId,currentWeekId);
			JSONArray prepareWeekArray = new JSONArray();
			if(prepareWeekRecords.size() != 0){
				for(int i=0;i<prepareWeekRecords.size();i++){
					DataRow row = prepareWeekRecords.get(i);
					JSONObject prepareWeekJson = new JSONObject();
					prepareWeekJson.put("predesc", row.stringValue("PRE_DESCRIBE"));
					prepareWeekJson.put("preplanday", row.stringValue("PRE_LOAD"));
					prepareWeekArray.put(prepareWeekJson);
				}
			}else{
				JSONObject prepareWeekJson = new JSONObject();
				prepareWeekJson.put("predesc", "无记录");
				prepareWeekJson.put("preplanday", "0");
				prepareWeekArray.put(prepareWeekJson);
			}
			jsonObject.put("plan", weekWorkArray);
			jsonObject.put("follow", prepareWeekArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}

	@Override
	public String findProWeekWorkListInfos(String weekTimeId,String grpId,String userId) {
		String responseText = BaseHandler.FAIL;
		try {
			User user = (User)this.getUser();
			if(UNDEFINED.equals(userId)){
				userId = user.getUserId();
			}
			String beforeWeekId = "";
			String startTime = "";
			String endTime = "";
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			
			String grpName = "";
			if(!UNDEFINED.equals(grpId)){
				DataRow grpRow = wmWeekManage.queryGroupRecord(grpId);
				grpName = grpRow.stringValue("GRP_NAME");
			}
			
			DataRow row1 = wmWeekManage.getBeforeWeekRow(weekTimeId);
			if(row1 != null && row1.size()>0){
				beforeWeekId = row1.stringValue("WT_ID");
				startTime = row1.stringValue("WT_BEGIN");
				endTime = row1.stringValue("WT_END");
				
				DataRow weekManageRow = wmWeekManage.getMasterWeekWorkRecord(userId,beforeWeekId);
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("startTime", startTime);
				jsonObject.put("endTime", endTime);
				jsonObject.put("time", startTime);
				
				jsonObject.put("grpName", grpName);
				jsonObject.put("weekTimeId", beforeWeekId);
				if(weekManageRow != null){
					jsonObject.put("wwDay", weekManageRow.stringValue("WW_DAY"));
					jsonObject.put("weekWorkId", weekManageRow.stringValue("WW_ID"));
					jsonObject.put("state", weekManageRow.stringValue("WW_STATE"));
					jsonObject.put("stateName", weekManageRow.stringValue("CODE_NAME"));
					String completion =  weekManageRow.stringValue("WW_COMPLETION");
					if("0".equals(completion)){
						jsonObject.put("completion","");
					}else{
						jsonObject.put("completion",completion);
					}
				}else{
					jsonObject.put("wwDay", "0");
					jsonObject.put("weekWorkId","无记录");
					jsonObject.put("stateName", "空");
				}
				List<DataRow> beforeWeekRecords = wmWeekManage.getWeekWorkRecord(userId,beforeWeekId);
				JSONArray jsonArray11 = new JSONArray();
				if(beforeWeekRecords.size() != 0){
					for(int i=0;i<beforeWeekRecords.size();i++){
						DataRow row = beforeWeekRecords.get(i);
						JSONObject jsonObject11 = new JSONObject();
						jsonObject11.put("describe", row.stringValue("ENTRY_DESCRIBE"));
						jsonObject11.put("planday", row.stringValue("ENTRY_PLAN"));
						jsonArray11.put(jsonObject11);
					}
				}else{
					JSONObject jsonObject11 = new JSONObject();
					jsonObject11.put("describe", "无记录");
					jsonObject11.put("planday", "0");
					jsonArray11.put(jsonObject11);
				}
				
				List<DataRow> prepareWeekRecords = wmWeekManage.getWeekPrepareRecord(userId,beforeWeekId);
				JSONArray jsonArray12 = new JSONArray();
				if(prepareWeekRecords.size() != 0){
					for(int i=0;i<prepareWeekRecords.size();i++){
						DataRow row = prepareWeekRecords.get(i);
						JSONObject jsonObject11 = new JSONObject();
						jsonObject11.put("predesc", row.stringValue("PRE_DESCRIBE"));
						jsonObject11.put("preplanday", row.stringValue("PRE_LOAD"));
						jsonArray12.put(jsonObject11);
					}
				}else{
					JSONObject jsonObject11 = new JSONObject();
					jsonObject11.put("predesc", "无记录");
					jsonObject11.put("preplanday","0");
					jsonArray12.put(jsonObject11);
				}
				jsonObject.put("plan", jsonArray11);
				jsonObject.put("follow", jsonArray12);
				responseText = jsonObject.toString();
			}else{
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("startTime", ERROR);
				jsonObject.put("weekTimeId", weekTimeId);
				responseText = jsonObject.toString();
			}
			
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}

	@Override
	public String findLastWeekWorkListInfos(String weekTimeId, String grpId,
			String userId) {
		String responseText = BaseHandler.FAIL;
		try {
			User user = (User)this.getUser();
			if(UNDEFINED.equals(userId)){
				userId = user.getUserId();
			}
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			
			String grpName = "";
			if(!UNDEFINED.equals(grpId)){
				DataRow grpRow = wmWeekManage.queryGroupRecord(grpId);
				grpName = grpRow.stringValue("GRP_NAME");
			}
			
			DataRow nextWeekRow = wmWeekManage.getNextWeekRow(weekTimeId);
			String followWeekId = "";
			String startTime = "";
			String endTime = "";
			if(nextWeekRow != null && nextWeekRow.size() > 0){
				followWeekId = nextWeekRow.stringValue("WT_ID");
				startTime = nextWeekRow.stringValue("WT_BEGIN");
				endTime = nextWeekRow.stringValue("WT_END");
				
				DataRow weekManageRow = wmWeekManage.getMasterWeekWorkRecord(userId,followWeekId);
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("startTime", startTime);
				jsonObject.put("time", startTime);
				jsonObject.put("endTime", endTime);
				jsonObject.put("weekTimeId", followWeekId);
				
				jsonObject.put("grpName", grpName);
				if(weekManageRow != null){
					jsonObject.put("wwDay", weekManageRow.stringValue("WW_DAY"));
					jsonObject.put("weekWorkId", weekManageRow.stringValue("WW_ID"));
					jsonObject.put("state", weekManageRow.stringValue("WW_STATE"));
					jsonObject.put("stateName", weekManageRow.stringValue("CODE_NAME"));
					String completion =  weekManageRow.stringValue("WW_COMPLETION");
					if("0".equals(completion)){
						jsonObject.put("completion","");
					}else{
						jsonObject.put("completion",completion);
					}
				}else{
					jsonObject.put("wwDay", "0");
					jsonObject.put("weekWorkId","无记录");
					jsonObject.put("stateName", "空");
				}
				
				List<DataRow> followWeekRecords = wmWeekManage.getWeekWorkRecord(userId,followWeekId);
				JSONArray followWeekArray = new JSONArray();
				if(followWeekRecords.size() != 0){
					for(int i=0;i<followWeekRecords.size();i++){
						DataRow row = followWeekRecords.get(i);
						JSONObject followWeekJson = new JSONObject();
						followWeekJson.put("describe", row.stringValue("ENTRY_DESCRIBE"));
						followWeekJson.put("planday", row.stringValue("ENTRY_PLAN"));
						followWeekArray.put(followWeekJson);
					}
				}else{
					JSONObject followWeekJson = new JSONObject();
					followWeekJson.put("describe", "无记录");
					followWeekJson.put("planday", "0");
					followWeekArray.put(followWeekJson);
				}
				
				List<DataRow> prepareWeekRecords = wmWeekManage.getWeekPrepareRecord(userId,followWeekId);
				JSONArray prepareWeekArray = new JSONArray();
				if(prepareWeekRecords.size() != 0){
					for(int i=0;i<prepareWeekRecords.size();i++){
						DataRow row = prepareWeekRecords.get(i);
						JSONObject prepareWeekJson = new JSONObject();
						prepareWeekJson.put("predesc", row.stringValue("PRE_DESCRIBE"));
						prepareWeekJson.put("preplanday", row.stringValue("PRE_LOAD"));
						prepareWeekArray.put(prepareWeekJson);
					}
				}else{
					JSONObject prepareWeekJson = new JSONObject();
					prepareWeekJson.put("predesc", "无记录");
					prepareWeekJson.put("preplanday", "0");
					prepareWeekArray.put(prepareWeekJson);
				}
				jsonObject.put("plan", followWeekArray);
				jsonObject.put("follow", prepareWeekArray);
				responseText = jsonObject.toString();
			}else{
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("startTime", ERROR);
				jsonObject.put("weekTimeId", weekTimeId);
				responseText = jsonObject.toString();
			}
			
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}

}
