package com.agileai.wm.module.workmanage.handler;

import java.util.Date;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.SimpleHandler;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;
import com.agileai.wm.cxmodule.WmWeekManage;

public class MobileWeekManageProviderHandler extends SimpleHandler{
	private static String ERROR = "error";
	private static String RoleCode = "Leader";
/*	private static String State = "CONFIRMED";
	private static String ENTRYSTATE = "1";*/
	
	public MobileWeekManageProviderHandler(){
		super();
	}
	
	public ViewRenderer prepareDisplay(DataParam param){
		String responseText = FAIL;
		return new AjaxRenderer(responseText);
	}

	
	@PageAction
	public ViewRenderer findLastWeekWorkListInfo(DataParam param){
		String responseText = FAIL;
		
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findThisWeekWorkListInfo(DataParam param){
		String responseText = FAIL;
		
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findFllowWeekWorkListInfo(DataParam param){
		String responseText = FAIL;
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findStatisWorkCompleteCardInfos(DataParam param){
		String responseText = FAIL;
		try {
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			String currentDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,new Date());
			String sdate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(new Date(), DateUtil.DAY, -21));
			DataRow row2 = wmWeekManage.getCurrentWeek(currentDate);
			String currentWeekId = "";
			if(row2 != null && row2.size() > 0){
				currentWeekId = row2.stringValue("WT_ID");
			}
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray1 = new JSONArray();
			JSONArray jsonArray2 = new JSONArray();
			JSONArray jsonArray3 = new JSONArray();
			JSONArray jsonArray4 = new JSONArray();
			List<DataRow> userRecords = wmWeekManage.findStaticUserRecords(RoleCode);
			for(int i=0;i<userRecords.size();i++){
				DataRow row = userRecords.get(i);
				jsonArray1.put(row.stringValue("USER_NAME"));
				String userId = row.getString("USER_ID");
				List<DataRow> records = wmWeekManage.findweekCompRecords(sdate,currentDate,userId);
				int weekComp = 0;
				int weekCompAvg = 0;
				for(int j=0;j<records.size();j++){
					DataRow comRow = records.get(j);
					weekComp = weekComp + Integer.valueOf(comRow.stringValue("WW_COMPLETION"));
					int count = records.size();
					if(count == 0){
						count = 1;
					}
					weekCompAvg = weekComp/count;
				}
				jsonArray2.put(weekCompAvg);
				DataRow weekWorkRow = wmWeekManage.getMasterWeekWorkRecord(userId,currentWeekId);
				if(weekWorkRow != null){
					jsonArray3.put(Integer.valueOf(weekWorkRow.stringValue("WW_COMPLETION")));
				}else{
					jsonArray3.put(0);
				}
			}
			jsonArray4.put(jsonArray2);
			jsonArray4.put(jsonArray3);
			jsonObject.put("labels", jsonArray1);
			jsonObject.put("data", jsonArray4);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	
	@PageAction
	public ViewRenderer findStatisCurrentWorkCompleteDetailInfos(DataParam param){
		String responseText = FAIL;
		try {
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			String currentDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,new Date());
			DataRow row2 = wmWeekManage.getCurrentWeek(currentDate);
			String currentWeekId = "";
			String startTime = "";
			String endTime = "";
			String standDay = "";
			if(row2 != null && row2.size() > 0){
				currentWeekId = row2.stringValue("WT_ID");
				startTime = row2.stringValue("WT_BEGIN");
				endTime = row2.stringValue("WT_END");
				standDay = row2.stringValue("WT_STAND_DAY");
			}else{
				row2 = wmWeekManage.getCurrentWeekTimeId(currentDate);
				currentWeekId = row2.stringValue("WT_ID");
				startTime = row2.stringValue("WT_BEGIN");
				endTime = row2.stringValue("WT_END");
				standDay = row2.stringValue("WT_STAND_DAY");
			}
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray1 = new JSONArray();
			List<DataRow> userRecords = wmWeekManage.findStaticUserRecords(RoleCode);;
			for(int i=0;i<userRecords.size();i++){
				DataRow row = userRecords.get(i);
				JSONObject jsonObject1 = new JSONObject();
				String userId = row.getString("USER_ID");
				jsonObject1.put("userName", row.stringValue("USER_NAME"));
				DataRow weekWorkRow = wmWeekManage.getMasterWeekWorkRecord(userId,currentWeekId);
				if(weekWorkRow != null){
					jsonObject1.put("comp",weekWorkRow.stringValue("WW_COMPLETION"));
				}else{
					jsonObject1.put("comp","0");
				}
				DataRow numRow = wmWeekManage.getEntryWorkNumRecord(userId,currentWeekId);
				if(numRow != null){
					jsonObject1.put("count",numRow.stringValue("ENTRY_ID_NUM"));
				}else{
					jsonObject1.put("count","0");
				}
				jsonArray1.put(jsonObject1);
			}
			jsonObject.put("startTime", startTime);
			jsonObject.put("endTime", endTime);
			jsonObject.put("standDay", standDay);
			jsonObject.put("weekTimeId", currentWeekId);
			jsonObject.put("workComp", jsonArray1);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findStatisBeforeWorkCompleteDetailInfos(DataParam param){
		String responseText = FAIL;
		try {
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			String weekTimeId = param.get("weekTimeId");
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray1 = new JSONArray();
			String beforeWeekId = "";
			String startTime = "";
			String endTime = "";
			String standDay = "";
			DataRow row1 = wmWeekManage.getBeforeWeekRow(weekTimeId);
			if(row1 != null && row1.size()>0){
				beforeWeekId = row1.stringValue("WT_ID");
				startTime = row1.stringValue("WT_BEGIN");
				endTime = row1.stringValue("WT_END");
				standDay = row1.stringValue("WT_STAND_DAY");
			}else{
				jsonObject.put("week",ERROR);
			}
			List<DataRow> userRecords = wmWeekManage.findStaticUserRecords(RoleCode);
			for(int i=0;i<userRecords.size();i++){
				DataRow row = userRecords.get(i);
				JSONObject jsonObject1 = new JSONObject();
				String userId = row.getString("USER_ID");
				jsonObject1.put("userName", row.stringValue("USER_NAME"));
				DataRow weekWorkRow = wmWeekManage.getMasterWeekWorkRecord(userId,beforeWeekId);
				if(weekWorkRow != null){
					jsonObject1.put("comp",weekWorkRow.stringValue("WW_COMPLETION"));
				}else{
					jsonObject1.put("comp","0");
				}
				DataRow numRow = wmWeekManage.getEntryWorkNumRecord(userId,beforeWeekId);
				if(numRow != null){
					jsonObject1.put("count",numRow.stringValue("ENTRY_ID_NUM"));
				}else{
					jsonObject1.put("count","0");
				}
				jsonArray1.put(jsonObject1);
			}
			jsonObject.put("startTime", startTime);
			jsonObject.put("endTime", endTime);
			jsonObject.put("standDay", standDay);
			jsonObject.put("weekTimeId", beforeWeekId);
			jsonObject.put("workComp", jsonArray1);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	
	@PageAction
	public ViewRenderer findStatisFollowWorkCompleteDetailInfos(DataParam param){
		String responseText = FAIL;
		try {
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			String weekTimeId = param.get("weekTimeId");
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray1 = new JSONArray();
			String nextWeekId = "";
			String startTime = "";
			String endTime = "";
			String standDay = "";
			DataRow row1 = wmWeekManage.getNextWeekRow(weekTimeId);
			if(row1 != null && row1.size() > 0){
				nextWeekId = row1.stringValue("WT_ID");
				startTime = row1.stringValue("WT_BEGIN");
				endTime = row1.stringValue("WT_END");
				standDay = row1.stringValue("WT_STAND_DAY");
			}else{
				jsonObject.put("week",ERROR);
			}
			List<DataRow> userRecords = wmWeekManage.findStaticUserRecords(RoleCode);;
			for(int i=0;i<userRecords.size();i++){
				DataRow row = userRecords.get(i);
				JSONObject jsonObject1 = new JSONObject();
				String userId = row.getString("USER_ID");
				jsonObject1.put("userName", row.stringValue("USER_NAME"));
				DataRow weekWorkRow = wmWeekManage.getMasterWeekWorkRecord(userId,nextWeekId);
				if(weekWorkRow != null){
					jsonObject1.put("comp",weekWorkRow.stringValue("WW_COMPLETION"));
				}else{
					jsonObject1.put("comp","0");
				}
				DataRow numRow = wmWeekManage.getEntryWorkNumRecord(userId,nextWeekId);
				if(numRow != null){
					jsonObject1.put("count",numRow.stringValue("ENTRY_ID_NUM"));
				}else{
					jsonObject1.put("count","0");
				}
				jsonArray1.put(jsonObject1);
			}
			jsonObject.put("startTime", startTime);
			jsonObject.put("endTime", endTime);
			jsonObject.put("standDay", standDay);
			jsonObject.put("workComp", jsonArray1);
			jsonObject.put("weekTimeId", nextWeekId);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findUserInfos(DataParam param){
		String responseText = FAIL;
		try {
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			List<DataRow> userRecords = wmWeekManage.findUserRecords();
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray1 = new JSONArray();
			for(int i=0;i<userRecords.size();i++){
				DataRow row = userRecords.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("userId", row.stringValue("USER_ID"));
				jsonObject1.put("userName",row.stringValue("USER_NAME"));
				jsonArray1.put(jsonObject1);
			}
			jsonObject.put("userInfos", jsonArray1);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findStatisWeekWorkCardInfos(DataParam param){
		String responseText = FAIL;
		try {
			User user = (User)this.getUser();
			String userId = user.getUserId();
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			JSONArray jsonArray1 = new JSONArray();
			
			String edate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,new Date());
			String beginDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(new Date(), DateUtil.DAY, -60));
			List<DataRow> records = wmWeekManage.findweekCompRecords(beginDate,edate,userId);
			for(int i=0;i<records.size();i++){
				DataRow row = records.get(i);
				String weekComp = row.stringValue("WW_COMPLETION");
				jsonArray1.put(weekComp);
			}
			if(records.size() < 8){
				int size = 8 - records.size();
				for(int i=0;i<size;i++){
					jsonArray1.put("0");
				}
			}
			
			jsonArray.put(jsonArray1);
			jsonObject.put("data", jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findStatisWeekWorkDetailInfos(DataParam param){
		String responseText = FAIL;
		try {
			User user = (User)this.getUser();
			String userId = user.getUserId();
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			String sdate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(new Date(), DateUtil.DAY, -60));
			String edate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,new Date());
			List<DataRow> records = wmWeekManage.findweekCompRecords(sdate,edate,userId);
			for(int i=0;i<records.size();i++){
				JSONObject jsonObject1 = new JSONObject();
				DataRow row = records.get(i);
				String weekComp = row.stringValue("WW_COMPLETION");
				String beginDate = row.stringValue("WT_BEGIN").substring(5, 10).replaceAll("-", "/");
				String endDate = row.stringValue("WT_END").substring(5, 10).replaceAll("-", "/");
				jsonObject1.put("beginDate", beginDate);
				jsonObject1.put("endDate", endDate);
				if(!weekComp.isEmpty()){
					jsonObject1.put("comp", weekComp);
				}else{
					jsonObject1.put("comp", "0");
				}
				
				jsonArray.put(jsonObject1);
			}
			jsonObject.put("weekWork", jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer initWeekExaminationUserInfos(DataParam param){
		String responseText = FAIL;
		
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer initWeekExamGroupInfos(DataParam param){
		String responseText = FAIL;

		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findActiveUserId(DataParam param){
		String responseText = FAIL;
		try {
			String userCode = param.get("userCode");
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			DataRow userInfo = wmWeekManage.findActiveUserId(userCode);
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("userId", userInfo.stringValue("USER_ID"));
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findCurrentGroupUserCodes(DataParam param){
		String responseText = FAIL;
		try {
			String grpId = param.get("grpId");
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray1 = new JSONArray();
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			List<DataRow> records =   wmWeekManage.findCurrentGroupUserCodes(grpId);
			if(records.size() >0){
				for(int i=0;i<records.size();i++){
					DataRow row1 = records.get(i);
					jsonArray1.put(row1.stringValue("USER_CODE"));
				}
			}else{
				jsonArray1.put("无记录");
			}
			
			jsonObject.put("curUserCodes", jsonArray1);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer doSubmitWeekWork(DataParam param){
		String responseText = FAIL;

		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer initCurrentSelectWeekInfo(DataParam param){
		String responseText = FAIL;
		
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer initCurSelectTimeWeekInfo(DataParam param){
		String responseText = FAIL;
		
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer showSubmitBtn(DataParam param){
		String responseText = FAIL;
		try {
			String selectUserCode = param.get("userCode");
			String grpId = param.get("grpId");
			User user = (User)this.getUser();
			String userCode = user.getUserCode();
			String userId = user.getUserId();
			
			JSONObject jsonObject = new JSONObject();
			if(!userCode.equals(selectUserCode)){
				WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
				List<DataRow> empJobRecords = wmWeekManage.quertUserEmpJobRecords(userId,grpId);
				for(int i=0;i<empJobRecords.size();i++){
					DataRow row = empJobRecords.get(i);
					String empJob = row.stringValue("EMP_JOB");
					if("Master".equals(empJob)){
						jsonObject.put("showSubmitBtn", true);
					}else if("Auditer".equals(empJob)){
						jsonObject.put("showSubmitBtn", true);
					}else{
						jsonObject.put("showSubmitBtn", false);
					}
				}
			}else{
				jsonObject.put("showSubmitBtn",false);
			}
			
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
}
